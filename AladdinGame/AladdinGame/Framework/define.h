﻿#ifndef __GAME_FRAMEWORK__
#define __GAME_FRAMEWORK__

#define _USE_MATH_DEFINES

#include <d3d9.h>		// d3d9.lib
#include <d3dx9.h>		// d3dx9.lib
#include <dinput.h>		// dinput8.lib, dxguid.lib
#include <dsound.h>		// dsound.lib

#include <windows.h>
#include <exception>
#include <math.h>
#include <string>
#include <map>
#include <vector>
#include <functional>
#include "../../pugixml/src/pugixml.hpp"


using namespace std;

#define WINDOW_WIDTH 640	
#define WINDOW_HEIGHT 448
#define SCALE_FACTOR 2.0f
#define VIEWPORT_VELOCITY 300
#define PI 3.14

#define C_WHITE D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)				// màu trắng
#define COLOR_KEY D3DXCOLOR(1.0f, 0.0f, 1.0f, 1.0f)				// màu khi mà load hình nó bỏ qua > trong suốt mau hong

enum eObjectID
{
	ALADDIN = 0, // main character
	LAND = 1,
	ROPE = 2,
	APPLE = 3, // for throw
	SWORD = 4, // f0r slash
	GUARD = 5,
	HAKIM = 6,
	FALZA = 7,
	NAHBI = 8,
	THROWER = 9, // thằng ném bình
	JUGGER = 10,
	JAR = 11,
	FLAME = 12,
	CAMEL = 13,
	EXPLOSIONPOT = 14,
	DAGGER = 15, //throw by enemy
	ABU = 16,

	MAPBOSS = 17,
	MAPBOSSBACKGROUND = 18,
	MISCITEMS = 19,
	PINK = 20,
	STAR = 23,
	RESILIENCE = 24,
	CUTSCENES = 25,
	EXPLOSIONS = 26,
	ITEM = 27,
	WALL = 28,
	JAFAR = 29, // the boss
	MAP1 = 30,
	MAP1BACKGROUND = 31,
	CIVILIAN = 32, // civilian sprite object
	
};


enum eStatus
{
	//Aladdin status
	NORMAL = 0,					// 00000 = 0	
	LEFTFACING = (1 << 0),				// 00001 = 2^0
	RIGHTFACING = (1 << 1),				// 00010 = 2^1
	JUMPING = (1 << 2),				// 00100 = 2^2
	LAYING_DOWN = (1 << 3),				// 01000 = 2^3
	RUNNING = (1 << 4),				// 10000 = 2^4
	LOOKING_UP = (1 << 5),				// 2^5
	THROW = (1 << 6),
	FALLING = (1 << 7),
	MOVING_JUMPING = (1 << 8),
	SLASH = (1 << 9), SLASH1 = ( 1<< 9),
	CLIMB_VERTICAL = (1 << 10),
	CLIMB_HORIZON = (1 << 11),
	SLASH2 = (1 << 12),
	BORING1 = (1 << 14),
	TAUGHT = (1 << 14),
	BORING2 = (1 << 15),
	BORING3 = (1 << 16),
	LANDING = (1 << 17),
	BRAKING = (1 << 18),
	BEATEN = (1 << 19),
	PUSH = (1 << 20),
	BURNED = (1 << 21),
	DYING = (1 << 22),
	CARE = (1 << 23),
	REVIVE = (1 << 24),
	JEALOUS = (1 << 25),
	DIE = (1 << 27),
	WIN = (1 << 28),
	PULLED = (1 << 29),

	//Enemy status

	//share status
	DESTROY = (1 << 26),
};




enum eDirection
{
	NONE = 0,
	TOP = 1,
	BOTTOM = 2,
	LEFT = 4,
	RIGHT = 8,
	ALL = (TOP | BOTTOM | LEFT | RIGHT),
};

enum eRopeType
{
	rVERTICAL = 0,
	rHORIZONTAL = 1
};


enum eCommand
{
	cmdMOVELEFT,
	cmdMOVERIGHT,
	cmdJUMP,
	cmdCLIMB
};

enum eLandType
{
	lNORMAL = 0,
	lFLAME = 1,
	lFALLING = 2,
	lPLATFORM = 3	
};

enum eSoundId
{
	// Nhạc nền map 1
	BACKGROUND_STAGE1,
	// Nhac chem
	sALADDIN_SLASH,
	sALADDIN_HURT,
	sOBJECT_THROW,
	sAPPLE_SPLAT,
	sABU_WAVING,
	sALADDIN_PUSH,
	sBOXING_BELL,
	sSTART_GUN,
	sLEVEL_COMPLETE,
	sENDING,
	sMENU_CHANGE,
	sNE_NAW,
	sNAHBI_TAUGHT,
	sGUARD_HIT,     // nahbi, hakim la len khi bi danh
	sSWORD_CHING,
	sSWORD_SPINNING,
	sFALZA_PANTS,    // falza rot quan
	sCLAY_POT,       // cái bình vỡ
	sWOW,  // khi ăn cái mặt hề xanh
	sCAMELSPIT,  // lạc đà phun 
	sCLOUD_POOF, // lúc ăn cây đèn nổ đám mây
	sCOLLECT_ITEM, // nhặt item: táo, hồng ngọc...
	sLOW_SWORD, // tiếng kiếm của th quăng tùm lum 
	sFLAGPOLE,  // tiếng chạm cây bật đàn hồi 
	sCONTINUE_POINT, // đụng cái bình mặt hè xanh, chết ssống lại tai đó 

	sFIRE_FROM_COAL,  // tiếng lửa trên đống than
	sGUARD_DIE,    // khi mấy con enemy chết
	sGAZEEM_HIT_1,
	sGAZEEM_HIT_2,
	sTIP_TOE,    // con trong lu khi di chuyển
	sCASH_REGISTER,    // khi mua đồ ở cửa hàng 
	sALADDIN_DIE,  // cảnh aladdin chết
	sMAP_INTRO,       // cảnh giới thiệu tên map
	sMAP_BOSS,    // nhạc nền mapboss

	// boss jafar
	sJAFAR_LAUGH,  // hình người, cười khi đánh trúng aladdin
	sJAFAR_SNAKE,  // con rắn lúd bị đánh trúng
	sJAFAR_TRACTOR  // tiếng phát ra khi hút aladdin
};

enum eItem
{
	// 1 - 9 preserve for life display
	iEXTRA_HEART = 10,
	iAPPLE = 11,
	iMONEY = 12,
	iRESTART_POINT = 13,
	i1UP = 14,
	iGENIE_BONUS = 15,
	iABU_BONUS = 16
};

enum eExplostion
{
	EX1 = 0,
	EX2 = 1,
	EX3 = 2,
	EX4 = 3,
	EX5 = 4
};

enum eCutscene
{
	cLEVEL_COMPLETE = 0,
	cMENU = 1,
	cSWORD = 2,
	cAGRABAH_MARKET = 3,
	cJAFAR_PALACE = 4
};

enum eZindex
{
	Z0 = 0,
	Z1 = (1 << 0), // 1
	Z2 = (1 << 1) // 2


};
typedef D3DXVECTOR3 GVector3;
typedef D3DXVECTOR2 GVector2;
#define VECTOR2ZERO GVector2(0.0f, 0.0f)
#define VECTOR2ONE  GVector2(1.0f, 1.0f)


#define DEFINE_FRAMEWORK_START		namespace Framework {

#define DEFINE_FRAMEWORK_END	}

#define LINK_FRAMEWORK		using namespace Framework;

#ifndef SAFE_DELETE

#define SAFE_DELETE(p) \
if(p) \
{\
	delete (p); \
	p = nullptr; \
} \

#endif // !SAFE_DELETE



#endif // !__GAME_FRAMEWORK__

