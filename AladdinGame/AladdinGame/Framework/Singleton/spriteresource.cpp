﻿#include "spriteresource.h"
#include "../../debug.h"
LINK_FRAMEWORK

SpriteResource* SpriteResource::_instance = nullptr;
SpriteResource::SpriteResource(void)
{
	// do nothing.
}

void SpriteResource::drawBounding(LPD3DXSPRITE spriteHandle, RECT rect, Viewport * _viewport)
{
	RECT frameR;
	frameR.left = frameR.top = 0;
	frameR.right = rect.right - rect.left;
	frameR.bottom = rect.top - rect.bottom;
	_listSprite[eObjectID::PINK]->setPosition(GVector2(rect.left, rect.bottom));
	_listSprite[eObjectID::PINK]->setFrameRect(frameR);
	_listSprite[eObjectID::PINK]->setOrigin(GVector2(0.0f, 0.0f));
	_listSprite[eObjectID::PINK]->render(spriteHandle, _viewport);


}

SpriteResource::~SpriteResource(void)
{
	for (auto spr = _listSprite.begin(); spr != _listSprite.end(); ++spr)
	{
		spr->second->release();		// release image
		delete spr->second;			// delete sprite
	}
	if (_listSprite.empty() == false)
		_listSprite.clear();		// remove all from MAP
}

void SpriteResource::loadResource(LPD3DXSPRITE spriteHandle)
{
	Sprite* sp = NULL;


	Sprite* aladdin = new Sprite(spriteHandle, L"Resources//Images//aladdin.png");
	this->_listSprite[eObjectID::ALADDIN] = aladdin;
	this->loadSpriteInfo(eObjectID::ALADDIN, "Resources//Images//aladdin.txt");

	Sprite* map = new Sprite(spriteHandle, L"Resources//Images//map1.png");
	this->_listSprite[eObjectID::MAP1] = map;
	this->loadSpriteInfo(eObjectID::MAP1, "Resources//Images//map1.txt");

	Sprite* guard = new Sprite(spriteHandle, L"Resources//Images//guard.png", D3DXCOLOR(120.f / 255, 193.f / 255, 152.f / 255, 1.f));
	this->_listSprite[eObjectID::GUARD] = guard;
	this->loadSpriteInfo(eObjectID::GUARD, "Resources//Images//guard.txt");

	Sprite* flame = new Sprite(spriteHandle, L"Resources//Images//jafar.png", D3DXCOLOR(186.f / 255, 254.f / 255, 202.f / 255, 1.f));
	this->_listSprite[eObjectID::JAFAR] = flame;
	this->loadSpriteInfo(eObjectID::JAFAR, "Resources//Images//jafar.txt");

	Sprite* map1background = new Sprite(spriteHandle, L"Resources//Images//map1_background.png");
	this->_listSprite[eObjectID::MAP1BACKGROUND] = map1background;
	this->loadSpriteInfo(eObjectID::MAP1BACKGROUND, "Resources//Images//map1_background.txt");

	Sprite* civilian = new Sprite(spriteHandle, L"Resources//Images//civilian_enemies.png", D3DXCOLOR(120.f / 255, 193.f / 255, 152.f / 255, 1.f));
	this->_listSprite[eObjectID::CIVILIAN] = civilian;
	this->loadSpriteInfo(eObjectID::CIVILIAN, "Resources//Images//civilian_enemies.txt");

	Sprite* item = new Sprite(spriteHandle, L"Resources//Images//items_old.png", COLOR_KEY /*D3DXCOLOR(248.f / 255, 0, 248.f / 255, 1.f)*/);
	this->_listSprite[eObjectID::ITEM] = item;
	this->loadSpriteInfo(eObjectID::ITEM, "Resources//Images//items.txt");

	Sprite* miscsitem = new Sprite(spriteHandle, L"Resources//Images//miscitems.png", COLOR_KEY);
	this->_listSprite[eObjectID::MISCITEMS] = miscsitem;
	this->loadSpriteInfo(eObjectID::MISCITEMS, "Resources//Images//miscitems.txt");

	Sprite* camel = new Sprite(spriteHandle, L"Resources//Images//camel.png", D3DXCOLOR(248.f / 255, 0, 248.f / 255, 1.f));
	this->_listSprite[eObjectID::CAMEL] = camel;
	this->loadSpriteInfo(eObjectID::CAMEL, "Resources//Images//camel.txt");

	Sprite* explosions = new Sprite(spriteHandle, L"Resources//Images//explosions_remake.png", COLOR_KEY);
	this->_listSprite[eObjectID::EXPLOSIONS] = explosions;
	this->loadSpriteInfo(eObjectID::EXPLOSIONS, "Resources//Images//explosions_remake.txt");

	Sprite* abu = new Sprite(spriteHandle, L"Resources//Images//abu.png", COLOR_KEY);
	this->_listSprite[eObjectID::ABU] = abu;
	this->loadSpriteInfo(eObjectID::ABU, "Resources//Images//abu.txt");

	Sprite* cutscene = new Sprite(spriteHandle, L"Resources//Images//Cutscenes_remake.png", COLOR_KEY);
	this->_listSprite[eObjectID::CUTSCENES] = cutscene;
	this->loadSpriteInfo(eObjectID::CUTSCENES, "Resources//Images//Cutscenes_remake.txt");

	Sprite* resilience = new Sprite(spriteHandle, L"Resources//Images//resilience.png");
	this->_listSprite[eObjectID::RESILIENCE] = resilience;
	this->loadSpriteInfo(eObjectID::RESILIENCE, "Resources//Images//resilience.txt");

	Sprite* mapboss = new Sprite(spriteHandle, L"Resources//Images//mapboss.png");
	this->_listSprite[eObjectID::MAPBOSS] = mapboss;
	////this->loadSpriteInfo(eObjectID::MAPBOSS, "Resources//Images//mapboss.txt");

	Sprite* mapbossbackground = new Sprite(spriteHandle, L"Resources//Images//mapboss_bg.png");
	this->_listSprite[eObjectID::MAPBOSSBACKGROUND] = mapbossbackground;
	////this->loadSpriteInfo(eObjectID::MAPBOSSBACKGROUND, "Resources//Images//mapboss_background.txt");

	Sprite* pink = new Sprite(spriteHandle, L"Resources//Images//pink.png");
	this->_listSprite[eObjectID::PINK] = pink;
}

Sprite* SpriteResource::getSprite(eObjectID id)
{
	Sprite *it = this->_listSprite.find(id)->second;
	return new Sprite(*it);			// get the copy version of Sprite
}

RECT SpriteResource::getSourceRect(eObjectID id, string name)
{
	return _sourceRectList[id][name];
}

GVector2 SpriteResource::getSourceTransition(eObjectID id, string name)
{
	return _sourceTransitionList[id][name];
}

void SpriteResource::loadSpriteInfo(eObjectID id, const char* fileInfoPath)
{
	FILE* file;
	file = fopen(fileInfoPath, "r");

	if (file)
	{
		while (!feof(file))
		{
			RECT rect;
			GVector2 transition;
			char name[100];
			fgets(name, 100, file);

			fscanf(file, "%s %d %d %d %d %f %f", &name, &rect.left, &rect.top, &rect.right, &rect.bottom, &transition.x, &transition.y);

			_sourceRectList[id][string(name)] = rect;
			_sourceTransitionList[id][string(name)] = transition;
		}
	}

	fclose(file);
}

void SpriteResource::releaseSprite(eObjectID id)
{
	Sprite *it = this->_listSprite.find(id)->second;
	delete it;							// delete the sprite only, dont relase image
	this->_listSprite.erase(id);		// erase funciotn only remove the pointer from MAP, dont delete it.
}
void SpriteResource::releaseTexture(eObjectID id)
{
	Sprite *spr = this->_listSprite.find(id)->second;
	spr->release();						// release image
	delete spr;
	this->_listSprite.erase(id);		// erase funciotn only remove the pointer from MAP, dont delete it.
}
SpriteResource* SpriteResource::getInstance()
{
	if (_instance == nullptr)
		_instance = new SpriteResource();
	return _instance;
}

void SpriteResource::release()
{
	delete _instance;			// _instance is static attribute, only static function can delete it.
	_instance = nullptr;
}
