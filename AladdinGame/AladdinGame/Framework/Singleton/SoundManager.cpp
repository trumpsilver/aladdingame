﻿

#include "SoundManager.h"

SoundManager* SoundManager::_instance;

SoundManager* SoundManager::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new SoundManager();
	}
	return _instance;
}

void SoundManager::loadSound(HWND hWnd)
{
	// Khởi tạo CSoundManager.
	DirectSound_Init(hWnd);

	CSound* sound = nullptr;

	sound = LoadSound("Resources//Sounds//map1.wav");
	_listSound[eSoundId::BACKGROUND_STAGE1] = sound;

	sound = LoadSound("Resources//Sounds//HighSword.wav");
	_listSound[eSoundId::sALADDIN_SLASH] = sound;

	sound = LoadSound("Resources//Sounds//AladdinHurt.wav");
	_listSound[eSoundId::sALADDIN_HURT] = sound;

	sound = LoadSound("Resources//Sounds//ObjectThrow.wav");
	_listSound[eSoundId::sOBJECT_THROW] = sound;

	sound = LoadSound("Resources//Sounds//AbuWaving.wav");
	_listSound[eSoundId::sABU_WAVING] = sound;

	sound = LoadSound("Resources//Sounds//AladdinPush.wav");
	_listSound[eSoundId::sALADDIN_PUSH] = sound;

	sound = LoadSound("Resources//Sounds//BoxingBell.wav");
	_listSound[eSoundId::sBOXING_BELL] = sound;

	sound = LoadSound("Resources//Sounds//StartGun.wav");
	_listSound[eSoundId::sSTART_GUN] = sound;

	sound = LoadSound("Resources//Sounds//LevelComplete.wav");
	_listSound[eSoundId::sLEVEL_COMPLETE] = sound;

	sound = LoadSound("Resources//Sounds//Ending.wav");
	_listSound[eSoundId::sENDING] = sound;

	sound = LoadSound("Resources//Sounds//MenuChange.wav");
	_listSound[eSoundId::sMENU_CHANGE] = sound;


	sound = LoadSound("Resources//Sounds//NeNaw.wav");
	_listSound[eSoundId::sNE_NAW] = sound;

	//-------------
	sound = LoadSound("Resources//Sounds//NahbiTaught.wav");
	_listSound[eSoundId::sNAHBI_TAUGHT] = sound;

	sound = LoadSound("Resources//Sounds//GuardHit.wav");
	_listSound[eSoundId::sGUARD_HIT] = sound;

	sound = LoadSound("Resources//Sounds//FalzaPants.wav");
	_listSound[eSoundId::sFALZA_PANTS] = sound;

	sound = LoadSound("Resources//Sounds//ClayPot.wav");
	_listSound[eSoundId::sCLAY_POT] = sound;

	sound = LoadSound("Resources//Sounds//Wow.wav");
	_listSound[eSoundId::sWOW] = sound;

	sound = LoadSound("Resources//Sounds//CamelSpit.wav");
	_listSound[eSoundId::sCAMELSPIT] = sound;

	sound = LoadSound("Resources//Sounds//CloudPoof.wav");
	_listSound[eSoundId::sCLOUD_POOF] = sound;

	sound = LoadSound("Resources//Sounds//CollectItem.wav");
	_listSound[eSoundId::sCOLLECT_ITEM] = sound;

	sound = LoadSound("Resources//Sounds//LowSword.wav");
	_listSound[eSoundId::sLOW_SWORD] = sound;

	sound = LoadSound("Resources//Sounds//Flagpole.wav");
	_listSound[eSoundId::sFLAGPOLE] = sound;

	sound = LoadSound("Resources//Sounds//ContinuePoint.wav");
	_listSound[eSoundId::sCONTINUE_POINT] = sound;

	sound = LoadSound("Resources//Sounds//FireFromCoal.wav");
	_listSound[eSoundId::sFIRE_FROM_COAL] = sound;

	sound = LoadSound("Resources//Sounds//GuardDie.wav");
	_listSound[eSoundId::sGUARD_DIE] = sound;

	sound = LoadSound("Resources//Sounds//TipToe.wav");
	_listSound[eSoundId::sTIP_TOE] = sound;

	sound = LoadSound("Resources//Sounds//CashRegister.wav");
	_listSound[eSoundId::sCASH_REGISTER] = sound;

	sound = LoadSound("Resources//Sounds//AladdinDie.wav");
	_listSound[eSoundId::sALADDIN_DIE] = sound;

	sound = LoadSound("Resources//Sounds//MapIntro.wav");
	_listSound[eSoundId::sMAP_INTRO] = sound;

	// Boss Jafar
	sound = LoadSound("Resources//Sounds//JafarLaugh.wav");
	_listSound[eSoundId::sJAFAR_LAUGH] = sound;

	sound = LoadSound("Resources//Sounds//JafarSnake.wav");
	_listSound[eSoundId::sJAFAR_SNAKE] = sound;

	sound = LoadSound("Resources//Sounds//JafarTractor.wav");
	_listSound[eSoundId::sJAFAR_TRACTOR] = sound;

	sound = LoadSound("Resources//Sounds//MapBoss.wav");
	_listSound[eSoundId::sMAP_BOSS] = sound;

	sound = LoadSound("Resources//Sounds//SwordChing.wav");
	_listSound[eSoundId::sSWORD_CHING] = sound;

	sound = LoadSound("Resources//Sounds//SwordSpinning.wav");
	_listSound[eSoundId::sSWORD_SPINNING] = sound;

	sound = LoadSound("Resources//Sounds//GazeemHit1.wav");
	_listSound[eSoundId::sGAZEEM_HIT_1] = sound;

	sound = LoadSound("Resources//Sounds//GazeemHit2.wav");
	_listSound[eSoundId::sGAZEEM_HIT_2] = sound;

	sound = LoadSound("Resources//Sounds//AppleSplat.wav");
	_listSound[eSoundId::sAPPLE_SPLAT] = sound;
}

bool SoundManager::IsPlaying(eSoundId soundid)
{
	auto sound = _listSound[soundid];
	if (sound != nullptr)
	{
		return sound->IsSoundPlaying();
	}
	return false;
}

void SoundManager::Play(eSoundId soundid)
{
	auto sound = _listSound[soundid];
	if (sound != nullptr)
	{
		PlaySound(sound);
		this->currentSound = sound;
	}
}
void SoundManager::PlayLoop(eSoundId soundid)
{
	auto sound = _listSound[soundid];
	if (sound != nullptr)
	{
		sound->Play(0, DSBPLAY_LOOPING);
	}
}
void SoundManager::Stop(eSoundId soundid)
{
	auto sound = _listSound[soundid];
	if (sound != nullptr)
	{
		StopSound(sound);
	}
}

void SoundManager::Stop()
{
	for (auto sound : _listSound)
	{
		sound.second->Stop();
	}
}

SoundManager::SoundManager()
{
	currentSound = nullptr;
}

SoundManager::~SoundManager()
{
}