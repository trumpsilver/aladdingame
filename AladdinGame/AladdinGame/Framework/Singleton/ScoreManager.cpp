﻿#include "ScoreManager.h"

ScoreManager::~ScoreManager()
{
}

ScoreManager::ScoreManager()
{
	_score = 0;
}

void ScoreManager::init()
{
	_score = 0;
}

ScoreManager * ScoreManager::_instance;

ScoreManager * ScoreManager::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new ScoreManager();
	}
	return _instance;
}

int ScoreManager::getScore()
{
	return _score;
}

void ScoreManager::setScore(int score)
{
	_score = score;
}

void ScoreManager::addScore(int score)
{
	_score += score;
}
