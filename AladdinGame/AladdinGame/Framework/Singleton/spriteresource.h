﻿#pragma once
#ifndef __SPRITERESOURCE_H__
#define __SPRITERESOURCE_H__


#include <fstream>

#include "../define.h"
#include "../sprite.h"

DEFINE_FRAMEWORK_START

class SpriteResource
{
public:
	static SpriteResource* getInstance();
	static void release();

	void loadResource(LPD3DXSPRITE spritehandle);
	Sprite* getSprite(eObjectID  id);

	RECT getSourceRect(eObjectID id, string name);
	GVector2 getSourceTransition(eObjectID id, string name);
	void loadSpriteInfo(eObjectID id, const char* fileInfoPath);

	// release Sprite, dont release Texture, keep Texture to use in another Sprite.
	void releaseSprite(eObjectID id);

	void releaseTexture(eObjectID id);	// dù dễ bị lỗi nhưng hàm này vẫn cần thiết để giải phóng texture khi không còn đối tượng nào nữa.

	void drawBounding(LPD3DXSPRITE spriteHandle, RECT rect, Viewport * _viewport);
	~SpriteResource(void);
private:
	SpriteResource(void);
	static SpriteResource* _instance;
	map<eObjectID, Sprite*> _listSprite;
	map<eObjectID, map<string, RECT> > _sourceRectList;
	map<eObjectID, map<string, GVector2> > _sourceTransitionList;

};

DEFINE_FRAMEWORK_END
#endif // !__SpriteResource_H__
