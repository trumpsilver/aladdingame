
#ifndef __SCORE_MANAGER__
#define __SCORE_MANAGER__
#include "../define.h"


using namespace std;
class ScoreManager
{
public:
	~ScoreManager();
	void init();
	static ScoreManager* getInstance();
	int getScore();
	void setScore(int score);
	void addScore(int score);
private:
	static ScoreManager* _instance;
	ScoreManager();
	int _score;
};

#endif // !__SCORE_MANAGER__
