﻿#include "BehaviorComponent.h"
#include "AnimationComponent.h"
#include "PhysicsComponent.h"
#include "../GameObject.h"
#include "../Singleton/gamecontroller.h"


//static phải khởi tạo ở file c++
sigcxx::Signal<GameObject * > BehaviorComponent::addToScene;

BehaviorComponent::BehaviorComponent()
{
}

BehaviorComponent::BehaviorComponent(GameObject* gameObject)
{
	_obj = gameObject;
}


BehaviorComponent::~BehaviorComponent()
{
}

void BehaviorComponent::update(float deltatime)
{
}

int BehaviorComponent::getStatus()
{
	return _status;
}

void BehaviorComponent::setStatus(int status)
{
	if (_status != status)
	{
		_preStatus = _status;
		_status = status;
	}
}

int BehaviorComponent::getFacingDirection()
{
	return _facingDirection;
}

void BehaviorComponent::setFacingDirection(eDirection status)
{
	_facingDirection = status;
}

int BehaviorComponent::getWeapon()
{
	return _weapon;
}

void BehaviorComponent::setWeapon(int weapon)
{
	_weapon = weapon;
}

void BehaviorComponent::checkWeaponAnimation()
{
}



void BehaviorComponent::executeCommand(eCommand command)
{
}

void BehaviorComponent::updateAnimation()
{
}

void BehaviorComponent::setGameController(GameController * input)
{
	_input = input;
}

wstring BehaviorComponent::getStatusString(eStatus status)
{
	switch (status)
	{
	case NORMAL:
		return L"normal";
		break;
	case JUMPING:
		return L"jumping";
		break;
	case LAYING_DOWN:
		return L"laying_down";
		break;
	case RUNNING:
		return L"running";
		break;
	case LOOKING_UP:
		return L"look_up";
		break;
	case THROW:
		return L"throw";
		break;
	case FALLING:
		return L"falling";
		break;
	case MOVING_JUMPING:
		return L"movingjumping";
		break;
	case SLASH:
		return L"slash";
		break;
	case CLIMB_VERTICAL:
		return L"climb vertical";
		break;
	case CLIMB_HORIZON:
		return L"climb horizon";
		break;
	case BORING1:
		return L"boring1";
		break;
	case BORING2:
		return L"boring2";
		break;
	case BORING3:
		return L"boring3";
		break;
	case LANDING:
		return L"landing";
		break;
	case BRAKING:
		return L"braking";
		break;
	case PUSH:
		return L"push";
		break;
	case BURNED:
		return L"burned";
		break;
	case DYING:
		return L"dying";
		break;
	case CARE:
		return L"care";
		break;
	case REVIVE:
		return L"revive";
		break;
	case JEALOUS:
		return L"jealous";
		break;
	default:
		return L"unknown";
		break;
	}
}

void BehaviorComponent::setGameObject(GameObject * gameObject)
{
	_obj = gameObject;
}

CollisionComponent * BehaviorComponent::getCollisionComponent()
{
	return _collisionComponent;
}

void BehaviorComponent::switchOrigin(GVector2 origin)
{
	GVector2 oldOrigin = _obj->getAnimationComponent()->getOrigin();
	RECT bound = _obj->getAnimationComponent()->getBounding();
	
	GVector2 difirent;
	difirent.x =  - origin.x * (bound.right - bound.left);
	difirent.y =  - origin.y * (bound.top - bound.bottom);

	_obj->getPhysicsComponent()->setPosition(_obj->getPhysicsComponent()->getPosition() + difirent);

	_obj->getAnimationComponent()->setOrigin(origin);
}

void BehaviorComponent::setRespawnPosition(GVector2 position)
{
	_respawnPosition = position;
}

void BehaviorComponent::reset()
{

}

void BehaviorComponent::respawn()
{
	_obj->getPhysicsComponent()->setPosition(_respawnPosition);
}

void BehaviorComponent::init()
{
}

void BehaviorComponent::release()
{
}

