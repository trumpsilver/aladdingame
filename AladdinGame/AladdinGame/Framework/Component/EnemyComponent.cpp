﻿
#include "EnemyComponent.h"
#include "../GameObject.h"

void EnemyBehaviorComponent::init()
{
}

int EnemyBehaviorComponent::getHitpoint()
{
	return _hitpoint;
}

void EnemyBehaviorComponent::setHitpoint(int hitpoint)
{
	_hitpoint = hitpoint;
}

int EnemyBehaviorComponent::getScore()
{
	return _score;
}

void EnemyBehaviorComponent::setScore(int score)
{
	_score = score;
}

void EnemyBehaviorComponent::dropHitpoint()
{
	_hitpoint--;
	if (_hitpoint <= 0)
		gainScore();
}

void EnemyBehaviorComponent::dropHitpoint(int damage)
{
	_hitpoint -= damage;
	OutputDebugStringW(L"Enemy hit point : ");
	__debugoutput(_hitpoint);
	if (_hitpoint <= 0)
		gainScore();
}

void EnemyBehaviorComponent::setStatus(int status)
{
	if (status == eStatus::DESTROY)
	{
		//SoundManager::getInstance()->Play(eSoundId::DESTROY_ENEMY);
	}
	BehaviorComponent::setStatus(status);
}

void EnemyBehaviorComponent::setRange(float rangeXStart, float rangeXEnd)
{
	if (rangeXStart <= rangeXEnd)
	{
		_rangeXStart = rangeXStart;
		_rangeXEnd = rangeXEnd;
	}
	else
	{
		_rangeXStart = rangeXEnd;
		_rangeXEnd = rangeXStart;
	}
}

void EnemyBehaviorComponent::gainScore()
{
	//add score
}


void EnemyBehaviorComponent::faceLeft()
{
	if (_obj->getAnimationComponent()->getScale().x > 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + _width);
	}
	setFacingDirection(eDirection::LEFT);
}

void EnemyBehaviorComponent::faceRight()
{
	if (_obj->getAnimationComponent()->getScale().x < 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - _width);
	}
	setFacingDirection(eDirection::RIGHT);
}

void EnemyBehaviorComponent::standing()
{
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, 0));

	auto gravity = (Gravity*)_obj->getPhysicsComponent()->getComponent("Gravity");
	gravity->setStatus(eGravityStatus::LANDED);
}

void EnemyBehaviorComponent::moveLeft()
{
	faceLeft();
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(-_obj->getPhysicsComponent()->getMovingSpeed(), move->getVelocity().y));
	setFacingDirection(eDirection::LEFT);
}

void EnemyBehaviorComponent::moveRight()
{
	faceRight();
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(_obj->getPhysicsComponent()->getMovingSpeed(), move->getVelocity().y));
	setFacingDirection(eDirection::RIGHT);
}

void EnemyBehaviorComponent::falling()
{
	auto g = (Gravity*)this->_obj->getPhysicsComponent()->getComponent("Gravity");
	g->setStatus(eGravityStatus::FALLING__DOWN);
}
