#pragma once
#ifndef __BEHAVIORCOMPONENT_H__
#define __BEHAVIORCOMPONENT_H__
#include "../define.h"
#include "Component.h"
#include "../Singleton/gamecontroller.h"
#include "../../../pugixml/src/pugixml.hpp"
#include "../../../sigcxx/include/sigcxx/sigcxx.hpp"
using namespace pugi;
//forward declarations
class CollisionComponent;
class GameObject;

LINK_FRAMEWORK

class BehaviorComponent: public Component
{
public:
	BehaviorComponent();
	BehaviorComponent(GameObject* gameObject);
	~BehaviorComponent();
	
	virtual void update(float deltatime);

	virtual int getStatus();
	virtual void setStatus(int status);

	virtual int getFacingDirection();
	virtual void setFacingDirection(eDirection status);

	virtual int getWeapon();
	virtual void setWeapon(int weapon);
	virtual void checkWeaponAnimation();

	virtual void executeCommand(eCommand command);
	virtual void updateAnimation();

	virtual void setGameController(GameController * input);

	wstring getStatusString(eStatus status);

	virtual void setGameObject(GameObject* gameObject);
	virtual CollisionComponent* getCollisionComponent();

	virtual void switchOrigin(GVector2 origin);
	virtual void setRespawnPosition(GVector2 position);
	virtual void reset();
	virtual void respawn();

	static sigcxx::Signal<GameObject * > addToScene;
	virtual void init() override;
	virtual void release() override;
protected:
	int _status;
	int _preStatus;
	int _facingDirection;
	int _weapon;

	GameController * _input;
	GameObject * _obj;
	GameObject * _colliseObject;
	CollisionComponent* _collisionComponent;
	GVector2 _respawnPosition;


};

#endif //__BEHAVIORCOMPONENT_H__
