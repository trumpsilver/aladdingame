﻿#include "Hakim.h"
#include "../../Framework/Singleton/ScoreManager.h"

void HakimPhysicsComponent::init()
{
	_movingSpeed = 100;
	auto movement = new Movement(GVector2(10, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -HAKIM_GRAVITY), movement);
}

GVector2 HakimPhysicsComponent::getVelocity()
{
	auto move = (Movement*)getComponent("Movement");
	return move->getVelocity();
}


RECT HakimPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void HakimAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::GUARD);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "nakim_running_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::GUARD, "nakim_beaten_1", NULL);;

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.1f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::GUARD,"nakim_running_1", "nakim_running_2", "nakim_running_3", "nakim_running_4", "nakim_running_5", "nakim_running_6", "nakim_running_7", "nakim_running_8", NULL);

	_animations[eStatus::SLASH] = new Animation(_sprite, 0.1f);
	_animations[eStatus::SLASH]->addFrameRect(eObjectID::GUARD, "nakim_slash_1", "nakim_slash_2", "nakim_slash_3", "nakim_slash_4", "nakim_slash_5", "nakim_slash_6", NULL);

	_animations[eStatus::BEATEN] = new Animation(_sprite, 0.1f);
	_animations[eStatus::BEATEN]->addFrameRect(eObjectID::GUARD, "nakim_beaten_1", "nakim_beaten_2", "nakim_beaten_3", "nakim_beaten_4", "nakim_beaten_5", "nakim_beaten_6", "nakim_beaten_7", "nakim_beaten_8", NULL);

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void HakimBehaviorComponent::init()
{
	reset();
	auto rect = SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "nakim_beaten_1");
	_width = rect.right - rect.left;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void HakimBehaviorComponent::update(float detatime)
{
	//Khai báo 1 số thứ cần thiết
	Land * landObject;
	Movement * move;

	checkCollision(detatime);
	if (_hitpoint <= 0)
	{
		ScoreManager::getInstance()->addScore(100);
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
		setStatus(eStatus::DESTROY);
		//add explosion
		GVector2 pos = _obj->getPhysicsComponent()->getPosition();
		RECT enemyBound = _obj->getPhysicsComponent()->getBounding();
		pos.x = ((enemyBound.right + enemyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX1);
		addToScene.Emit(explosion);
		return;
	}

	//not collison with land falling
	landObject = (Land *)_collisionComponent->isColliding(eObjectID::LAND);
	if (landObject == nullptr)
	{
		falling();
		return;
	}

	//calculator aladdin position
	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	RECT aladdinBound = aladdin->getAnimationComponent()->getBounding();
	float aladdinPosX;
	if (_facingDirection == eDirection::LEFT)
	{
		aladdinPosX = aladdinBound.right;
	}
	else
	{
		aladdinPosX = aladdinBound.left;
	}
	//diffirent between nahbi and aladdin pos
	float diffirent = _obj->getPhysicsComponent()->getPosition().x - aladdinPosX;
	//calculator max slash distance
	float maxSlashDistance = HAKIM_SWORD_WIDTH + HAKIM_SWORD_OFFSET;
	float posX = _obj->getPhysicsComponent()->getPositionX();
	switch (_status)
	{
	case NORMAL:
		//Kiểm tra aladdin có trong tầm thì chém
		if (diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance)
		{
			if (diffirent > 0)
			{
				faceLeft();
			}
			else
			{
				faceRight();
			}
			standing();
			slash();
			break;
		}
		//Có 3 tầm hoạt động 
		//1.posX < _rangeXStart nhân vật đi quá đà, chỉ có thể qua phải
		//2._rangeXStart  <= posX <= _rangeXEnd nhân vật có thể qua trái lẫn phải
		//3._rangeXEnd > _posXEnd nhân vật đi quá đà, chỉ có thể qua trái
		if (posX >= _rangeXStart && posX <= _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
			else
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
		}
		else if (posX < _rangeXStart)
		{
			if (diffirent < 0)
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
		}
		else if (posX > _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
		}
		break;
	case RUNNING:
		if (diffirent > maxSlashDistance && posX >= _rangeXStart) // aladdin ở bến trái
		{
			moveLeft();
			break;
		}
		if (diffirent < -maxSlashDistance && posX <= _rangeXEnd) // aladdin ở bên phải
		{
			moveRight();
			break;
		}
		//Trong phạm vi thì đứng lại hoặc quá phạm vi thì đứng lại
		if ((diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance) || posX <= _rangeXStart || posX >= _rangeXEnd)
		{
			standing();
			setStatus(eStatus::NORMAL);
		}
		break;
	case SLASH: //state chỉ chờ để chém xong
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	case BEATEN:
		standing();
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	default:
		break;
	}

}

void HakimBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	updateAnimation();
}

void HakimBehaviorComponent::dropHitpoint(int damage)
{
	EnemyBehaviorComponent::dropHitpoint(damage);
	if (_hitpoint > 0)
	{
		SoundManager::getInstance()->Play(eSoundId::sGUARD_HIT);
	}
	else
	{
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
	}
	setStatus(eStatus::BEATEN);
	_standTime = STAND_TIME;
}

void HakimBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
	faceLeft();
	_hitpoint = 100;
	_standTime = 0;
}

void HakimBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

void HakimBehaviorComponent::checkCollision(float deltatime)
{
	//declare
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case LAND:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				colliSide = _collisionComponent->getCollidingDirection(obj);
				if (colliSide == eDirection::TOP && _collisionComponent->isColliding(obj, moveX, moveY, deltatime) && _obj->getPhysicsComponent()->getPositionY() > obj->getPhysicsComponent()->getBounding().bottom)
				{
					standing();
					eDirection direction = _collisionComponent->getSide(obj);
					_collisionComponent->updateTargetPosition(obj, colliSide, GVector2(moveX, moveY));;
				}
			}
			break;
		
		default:
			break;
		}
	}
}

void HakimBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case NORMAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	case BEATEN:
		_obj->getAnimationComponent()->setAnimation(eStatus::BEATEN);
		break;
	case SLASH:
		_obj->getAnimationComponent()->setAnimation(eStatus::SLASH);
		break;
	default:
		break;
	}
}


void HakimBehaviorComponent::slash(float offset, float width)
{
	setStatus(eStatus::SLASH);
	setWeapon(eStatus::SLASH);
	//Tính toạn tọa độ kích thước của kiếm
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += _obj->getAnimationComponent()->getSprite()->getFrameHeight();
	float height = _obj->getAnimationComponent()->getSprite()->getFrameHeight();

	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(offset, 0);
	}
	else
	{
		pos -= GVector2(offset + width, 0);
	}

	auto sword = ObjectFactory::getSword(pos, width, height, false);
	addToScene.Emit(sword);

}
