﻿
#ifndef __JAFAR_H__
#define __JAFAR_H__
#include"../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define JAFAR_HIT_POINT 1000
#define JAFAR_LAUGHT_TIME 1000
#define JAFAR_PULL_TIME 3000
#define JAFAR_REST_TIME 3000
#define JAFAR_FLAME_TIME 1500
#define JAFAR_STAR_OFFSETX 71 * SCALE_FACTOR
#define JAFAR_STAR_OFFSETY 47 * SCALE_FACTOR
#define JAFAR_FRAME_OFFSETX 44 * SCALE_FACTOR
#define JAFAR_FRAME_OFFSETY 29 * SCALE_FACTOR
#define JAFAR_STAR_FRAME 3
#define JAFAR_FLAME_FRAME 7
#define JAFAR_NORMAL_WIDTH 24 * SCALE_FACTOR
#define JAFAR_NORMAL_HEIGHT 71 * SCALE_FACTOR
#define JAFAR_SNAKE_WIDTH 29 * SCALE_FACTOR
#define JAFAR_SNAKE_HEIGHT 84 * SCALE_FACTOR

//forward declator
class StarCreator;

enum eJafarStatus
{
	jNORMAL = 0,
	jRAMPAGE = 1,
	jLAUGHT = 2,
	jFLAME = 3,
	jPULL = 4
};

class JafarPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	RECT getBounding() override;
protected:
};

class JafarAnimationComponent : public AnimationComponent
{
public:
	void init();
	RECT getBounding() override;
private:

};

class JafarBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void dropHitpoint(int damage) override;
	void reset() override;
	void respawn() override;
	static sigcxx::Signal<> rampageMode;
private:
	void faceLeft();
	void faceRight();
	void checkCollision(float deltatime);
	void updateAnimation();
	void dropFrame();
	float _timer;
	StarCreator * _starCreator;
	float _width;
};


#endif


