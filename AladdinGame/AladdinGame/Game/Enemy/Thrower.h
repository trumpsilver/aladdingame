﻿
#ifndef __THROWER_H__
#define __THROWER_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define THROW_INTERVAL 100
#define THROW_VELOCITY GVector2(100, 0)
#define THROW_OFFSETX 27 * SCALE_FACTOR
#define THROW_OFFSETY -15 * SCALE_FACTOR

class ThrowerPhysicsComponent : public NullPhysicsComponent
{
public:
	void init();
protected:
};

class ThrowerAnimationComponent : public AnimationComponent
{
public:
	void init();
private:

};

//he is invisible so no HP xD
class ThrowerBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
protected:
	bool _toThrow;
	float _timer;
	void throwPot();
};


#endif

