﻿#ifndef __JUGGER_H__
#define __JUGGER_H__

#include"../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"
  

#define JUGGER_MAX_DISTANT_TO_FAST_THROW 350
#define JUGGER_DAGGER_GRAVITY 200
#define JUGGER_DAGGER_OFFSEYX 37 * SCALE_FACTOR
#define JUGGER_DAGGER_OFFSEYY 28 * SCALE_FACTOR

class JuggerPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	RECT getBounding() override;
protected:
};

class JuggerAnimationComponent : public AnimationComponent
{
public:
	void init();
protected:
};

class JuggerBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void reset() override;
	void respawn() override;
private:
	
	void throwDagger(GVector2 velocity = GVector2(-800, -300), GVector2 gravity = GVector2(0, -800));

	float _throwTime;


};


#endif
