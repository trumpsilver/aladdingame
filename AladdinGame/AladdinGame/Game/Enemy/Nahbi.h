﻿
#ifndef __NAHBI_H__
#define __NAHBI_H__
#include"../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define STAND_TIME 500
#define NAHBI_GRAVITY 800
#define NAHBI_SWORD_OFFSET 0 * SCALE_FACTOR
#define NAHBI_SWORD_WIDTH 114 * SCALE_FACTOR
#define NAHBI_DONOT_CHECK_TIME 500


class NahbiPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	GVector2 getVelocity();
	RECT getBounding() override;
protected:
};

class NahbiAnimationComponent : public AnimationComponent
{
public:
	void init();
	RECT getBounding();
protected:
	RECT _bound;

};

class NahbiBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void dropHitpoint(int damage) override;
	void reset() override;
	void respawn() override;
private:
	void checkCollision(float deltatime);
	void updateAnimation();
	void slash(float offset = NAHBI_SWORD_OFFSET, float height = NAHBI_SWORD_WIDTH);

	float _standTime; // sau khi bị đánh phải đứng lại một khoảng thời gian
	float _timer;


};


#endif


