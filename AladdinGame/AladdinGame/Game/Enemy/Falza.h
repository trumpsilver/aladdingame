﻿#ifndef __FALZA_H__
#define __FALZA_H__

#include"../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"
  

#define STAND_TIME 500
#define FALZA_GRAVITY 800
#define FALZA_THROW_INTERVAL 3000
#define FALZA_DAGGER_OFFSETX 42 * SCALE_FACTOR
#define FALZA_DAGGER_OFFSETY 23 * SCALE_FACTOR
#define FALZA_MAX_THROW_DISTANT 200

class FalzaPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	GVector2 getVelocity();
	RECT getBounding() override;
protected:
};

class FalzaAnimationComponent : public AnimationComponent
{
public:
	void init();
private:

};

class FalzaBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void dropHitpoint(int damage) override;
	void reset() override;
	void respawn() override;
private:
	void updateAnimation();
	void checkCollision(float deltatime);
	void throwApple();

	float _standTime; // sau khi bị đánh phải đứng lại một khoảng thời gian
	float _throwTime;


};


#endif
