﻿
#ifndef __JAR_H__
#define __JAR_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"

#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define JAR_MOVING_TIME 1000 // Jar chỉ di chuyển trong vòng 1 s rồi dừng lại rồi mới đi tiếp
#define JAR_STAND_TIME 2000 // Thời gian đứng im trước khi ngó lên rồi di chuyển
#define JAR_MIN_DISTANCE 25 * SCALE_FACTOR
#define JAR_GRAVITY 800
#define JAR_PUSH_OFFSETX 15 * SCALE_FACTOR
#define JAR_PUSH_WIDTH 44 * SCALE_FACTOR


class JarPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	GVector2 getVelocity();
	RECT getBounding() override;
protected:
};

class JarAnimationComponent : public AnimationComponent
{
public:
	void init();
private:

};

class JarBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void dropHitpoint(int damage) override;
	void reset() override;
	void respawn() override;
private:
	void checkCollision(float deltatime);
	void updateAnimation();
	void slash(float offset = JAR_PUSH_OFFSETX, float height = JAR_PUSH_WIDTH);

	float _standTime; // sau khi bị đánh phải đứng lại một khoảng thời gian
	float _timer; // đồng hồ canh thời gian đứng yên di chuyển


};


#endif

