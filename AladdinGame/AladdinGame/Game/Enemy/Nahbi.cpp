﻿#include "Nahbi.h"
#include <ctime>
#include "../../Framework/Singleton/ScoreManager.h"

void NahbiPhysicsComponent::init()
{
	_movingSpeed = 100;
	auto movement = new Movement(GVector2(10, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -NAHBI_GRAVITY), movement);
}

GVector2 NahbiPhysicsComponent::getVelocity()
{
	auto move = (Movement*)getComponent("Movement");
	return move->getVelocity();
}


RECT NahbiPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void NahbiAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::GUARD);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "nahbi_running_8"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	//_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	//_animations[eStatus::NORMAL]->addFrameRect(eObjectID::GUARD, "nahbi_running_8", NULL);

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.1f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::GUARD, "nahbi_running_1", "nahbi_running_2", "nahbi_running_3", "nahbi_running_4", "nahbi_running_5", "nahbi_running_6", "nahbi_running_7", "nahbi_running_8", NULL);

	_animations[eStatus::TAUGHT] = new Animation(_sprite, 0.1f);
	_animations[eStatus::TAUGHT]->addFrameRect(eObjectID::GUARD, "nahbi_taught_1", "nahbi_taught_2", "nahbi_taught_3", "nahbi_taught_4", "nahbi_taught_5", "nahbi_taught_6", NULL);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f); 
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::GUARD, "nahbi_slash1_1", "nahbi_slash1_2", "nahbi_slash1_3", "nahbi_slash1_4", "nahbi_slash1_5", "nahbi_slash1_6", NULL);

	_animations[eStatus::SLASH] = new Animation(_sprite, 0.1f);
	_animations[eStatus::SLASH]->addFrameRect(eObjectID::GUARD, "nahbi_slash2_1", "nahbi_slash2_2", "nahbi_slash2_3", "nahbi_slash2_4", "nahbi_slash2_5", NULL);

	_animations[eStatus::BEATEN] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BEATEN]->addFrameRect(eObjectID::GUARD, "nahbi_beaten_1", "nahbi_beaten_2", "nahbi_beaten_3", "nahbi_beaten_4", "nahbi_beaten_5", "nahbi_beaten_6", NULL);

	_animations[eStatus::BURNED] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BURNED]->addFrameRect(eObjectID::GUARD, "nahbi_burned_1", "nahbi_burned_2", "nahbi_burned_3", "nahbi_burned_4", "nahbi_burned_5", "nahbi_burned_6", "nahbi_burned_7", "nahbi_burned_8", "nahbi_burned_9", NULL);

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}

	_bound = SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "nahbi_running_1");
}

RECT NahbiAnimationComponent::getBounding()
{
	_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());

	RECT oldRect = _sprite->getFrameRect();
	_sprite->setFrameRect(_bound);
	RECT bound = _sprite->getBounding();
	_sprite->setFrameRect(oldRect);

	return bound;
}

void NahbiBehaviorComponent::init()
{
	reset();
	auto rect = SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "nahbi_slash1_1");
	_width = rect.right - rect.left;
	srand(time(0));//Khởi tạo random để random khả năng chém
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);

}


void NahbiBehaviorComponent::update(float detatime)
{
	//Khai báo 1 số thứ cần thiết
	Land * landObject;
	Movement * move;

	checkCollision(detatime);
	if (_hitpoint <= 0)
	{
		ScoreManager::getInstance()->addScore(100);
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
		setStatus(eStatus::DESTROY);
		//add explosion
		GVector2 pos = _obj->getPhysicsComponent()->getPosition();
		RECT enemyBound = _obj->getPhysicsComponent()->getBounding();
		pos.x = ((enemyBound.right + enemyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX1);
		addToScene.Emit(explosion);
		return;
	}

	//not collison with land falling
	landObject = (Land *)_collisionComponent->isColliding(eObjectID::LAND);
	if (landObject == nullptr)
	{
		falling();
		return;
	}

	//calculator aladdin position
	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	RECT aladdinBound = aladdin->getAnimationComponent()->getBounding();
	float aladdinPosX;
	if (_facingDirection == eDirection::LEFT)
	{
		aladdinPosX = aladdinBound.right;
	}
	else
	{
		aladdinPosX = aladdinBound.left;
	}
	//diffirent between nahbi and aladdin pos
	float diffirent = _obj->getPhysicsComponent()->getPosition().x - aladdinPosX;
	//calculator max slash distance
	float maxSlashDistance = NAHBI_SWORD_WIDTH + NAHBI_SWORD_OFFSET;
	float posX = _obj->getPhysicsComponent()->getPositionX();
	switch (_status)
	{
	case NORMAL:
		//Chạm lửa thì bỏng
		if (landObject != nullptr && landObject->getLandType() == eLandType::lFLAME)
		{
			setStatus(eStatus::BURNED);
			break;
		}
		//Kiểm tra aladdin có trong tầm thì chém
		if (diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance)
		{
			if (diffirent > 0)
			{
				faceLeft();
			}
			else
			{
				faceRight();
			}
			standing();
			int random = rand() % 5; // random có thể bằng 0, 1, 2, 3, 4. 80% là chém, vậy random nhỏ hơn bằng 3 sẽ chém
			if (random <= 3)
			{
				slash();
			}
			else
			{
				setStatus(eStatus::SLASH2);
			}

			break;
		}
		//Có 3 tầm hoạt động 
		//1.posX < _rangeXStart nhân vật đi quá đà, chỉ có thể qua phải
		//2._rangeXStart  <= posX <= _rangeXEnd nhân vật có thể qua trái lẫn phải
		//3._rangeXEnd > _posXEnd nhân vật đi quá đà, chỉ có thể qua trái
		if (posX >= _rangeXStart && posX <= _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
			else
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
		}
		else if (posX < _rangeXStart)
		{
			if (diffirent < 0)
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
			else
			{
				setStatus(eStatus::TAUGHT);
				
			}
		}
		else if (posX > _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
			else
			{
				setStatus(eStatus::TAUGHT);
			}
		}
		break;
	case RUNNING:
		//Chạm lửa thì bỏng
		if (landObject != nullptr && landObject->getLandType() == eLandType::lFLAME)
		{
			setStatus(eStatus::BURNED);
			break;
		}
		if (diffirent > maxSlashDistance && posX >= _rangeXStart) // aladdin ở bến trái
		{
			moveLeft();
			break;
		}
		if (diffirent < -maxSlashDistance && posX <= _rangeXEnd) // aladdin ở bên phải
		{
			moveRight();
			break;
		}
		//Trong phạm vi thì đứng lại hoặc quá phạm vi thì đứng lại
		if ((diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance) || posX <= _rangeXStart || posX >= _rangeXEnd)
		{
			standing();
			setStatus(eStatus::NORMAL);
		}
		break;
	case BURNED:
		//Đang bị cháy thì chạy cho đến khi hết cháy
		//Không chạm thì hết bỏng
		//Đuổi theo aladdin nhưng phải ra khỏi flame land mới chém được
		if (diffirent > 0) //aladdin bên trái
		{
			int random = rand() % 2;
			if (random == 0 && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
			{
				SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_1);
			}
			else
			{
				if (!SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
					SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_2);
			}
			moveLeft();
		}
		else //aladdin bên phải
		{
			int random = rand() % 2;
			if (random == 0 && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
			{
				SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_1);
			}
			else
			{
				if (!SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
					SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_2);
			}
			moveRight();
		}
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getTotalTimeAnimation() <= NAHBI_DONOT_CHECK_TIME / 1000.f)
			break;
		if (landObject == nullptr || ((landObject != nullptr) && landObject->getLandType() != eLandType::lFLAME))
		{
			standing();
			setStatus(eStatus::NORMAL);
		}
		break;
	case SLASH: case SLASH2: //slash2 chẳng qua là normal khác, set thành trạng thái để chờ nó chạy animation xong
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	case TAUGHT:
		if (diffirent > 0)
		{
			faceLeft();
		}
		else
		{
			faceRight();
		}
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			int random = rand() % 100; // random có thể bằng 0, 1, 2, 3, 4. 80% là chém, vậy random nhỏ hơn bằng 3 sẽ chém
			if (random <= 98)
			{
				slash();
			}
			else
			{
				setStatus(eStatus::SLASH2);
			}
		}
		//Taught xong thì chuyển về normal, lưu ý có lưu trái phải
		break;
	case BEATEN:
		standing();
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	default:
		break;
	}
}


void NahbiBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	updateAnimation();
	if (_status == eStatus::TAUGHT && !SoundManager::getInstance()->IsPlaying(eSoundId::sNAHBI_TAUGHT))
	{
		SoundManager::getInstance()->Play(eSoundId::sNAHBI_TAUGHT);
	}
}

void NahbiBehaviorComponent::dropHitpoint(int damage)
{
	EnemyBehaviorComponent::dropHitpoint(damage);
	if (_hitpoint > 0)
	{
		SoundManager::getInstance()->Play(eSoundId::sGUARD_HIT);
	}
	else
	{
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
	}
	setStatus(eStatus::BEATEN);
	_standTime = STAND_TIME;
}

void NahbiBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
	faceLeft();
	_hitpoint = 100;
	_standTime = 0;
	_timer = 0;
}

void NahbiBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

void NahbiBehaviorComponent::checkCollision(float deltatime)
{
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case LAND:
			if (_collisionComponent->checkCollision(obj, deltatime, true))
			{
				colliSide = _collisionComponent->getCollidingDirection(obj);
				if (colliSide == eDirection::TOP && _collisionComponent->isColliding(obj, moveX, moveY, deltatime) && _obj->getPhysicsComponent()->getPositionY() > obj->getPhysicsComponent()->getBounding().bottom)
				{
					standing();
					_collisionComponent->updateTargetPosition(obj, colliSide, GVector2(moveX, moveY));;
				}
			}
			break;
		}
	}

}

void NahbiBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case TAUGHT:
		_obj->getAnimationComponent()->setAnimation(eStatus::TAUGHT);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	case NORMAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case BURNED:
	{
		int random = rand() % 2;
		if (random == 0 && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
		{
			SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_1);
		}
		else
		{
			if (!SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_1) && !SoundManager::getInstance()->IsPlaying(eSoundId::sGAZEEM_HIT_2))
				SoundManager::getInstance()->Play(eSoundId::sGAZEEM_HIT_2);
		}
		_obj->getAnimationComponent()->setAnimation(eStatus::BURNED);
	}
		break;
	case SLASH:
		_obj->getAnimationComponent()->setAnimation(eStatus::SLASH);
		break;
	case SLASH2:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case BEATEN:
		_obj->getAnimationComponent()->setAnimation(eStatus::BEATEN);
		break;
	default:
		break;
	}
}

void NahbiBehaviorComponent::slash(float offset, float width)
{
	setStatus(eStatus::SLASH);
	setWeapon(eStatus::SLASH);
	//Tính toạn tọa độ kích thước của kiếm
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += _obj->getAnimationComponent()->getSprite()->getFrameHeight();
	float height = _obj->getAnimationComponent()->getSprite()->getFrameHeight();

	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(offset, 0);
	}
	else
	{
		pos -= GVector2(offset + width, 0);
	}

	auto sword = ObjectFactory::getSword(pos, width, height, false);
	addToScene.Emit(sword);
	SoundManager::getInstance()->Play(eSoundId::sLOW_SWORD);

}
