﻿#include "Jugger.h"
#include "../../Framework/Singleton/ScoreManager.h"

void JuggerPhysicsComponent::init()
{
	
}



RECT JuggerPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void JuggerAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CIVILIAN);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CIVILIAN, "juggler_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::CIVILIAN, "juggler_1", "juggler_2", "juggler_3", "juggler_4", "juggler_5", "juggler_6", "juggler_7", "juggler_8", "juggler_9", "juggler_10", "juggler_11", NULL);

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}


void JuggerBehaviorComponent::init()
{
	reset();
	auto rect = SpriteResource::getInstance()->getSourceRect(eObjectID::CIVILIAN, "juggler_1");
	_width = rect.right - rect.left;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
	faceLeft();
}

void JuggerBehaviorComponent::update(float detatime)
{
	_throwTime += detatime;
	if (_hitpoint <= 0)
	{
		ScoreManager::getInstance()->addScore(100);
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
		setStatus(eStatus::DESTROY);
		GVector2 pos = _obj->getPhysicsComponent()->getPosition();
		RECT enemyBound = _obj->getPhysicsComponent()->getBounding();
		pos.x = ((enemyBound.right + enemyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX1);
		addToScene.Emit(explosion);
		return;
	}


	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	auto aladdinPos = aladdin->getPhysicsComponent()->getPosition();

	float diffirent = _obj->getPhysicsComponent()->getPosition().x - aladdinPos.x;

	if (diffirent >= 0)
	{
		faceLeft();
	}
	else
	{
		faceRight();
	}
	if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() == 9) // Ném ở khung hình thứ 10 (index = 9)
	{
		if (_weapon != eStatus::THROW)
		{
			if (_facingDirection == eDirection::LEFT && diffirent >= 0)
			{
				if (diffirent <= JUGGER_MAX_DISTANT_TO_FAST_THROW)
				{
					SoundManager::getInstance()->Play(eSoundId::sSWORD_SPINNING);
					throwDagger(GVector2(-800, -300));
				}
				else
				{
					SoundManager::getInstance()->Play(eSoundId::sSWORD_SPINNING);
					throwDagger(GVector2(-300, 600), GVector2(0, -1200));
				}
			}

			if (_facingDirection == eDirection::RIGHT && diffirent <= 0)
			{
				if (diffirent >= -JUGGER_MAX_DISTANT_TO_FAST_THROW)
				{
					SoundManager::getInstance()->Play(eSoundId::sSWORD_SPINNING);
					throwDagger(GVector2(-800, -300));
				}
				else
				{
					SoundManager::getInstance()->Play(eSoundId::sSWORD_SPINNING);
					throwDagger(GVector2(-300, 600), GVector2(0, -1200));
				}
			}
		}
	}
	else// Khung hình còn lại thì reset lại weapon
	{
		setWeapon(eStatus::NORMAL);
	}

}

void JuggerBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	updateAnimation();
}

void JuggerBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
	_hitpoint = 100;
	_throwTime = THROW_INTERVAL;
}

void JuggerBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}




void JuggerBehaviorComponent::throwDagger(GVector2 velocity , GVector2 gravity)
{
	setWeapon(eStatus::THROW);
	SoundManager::getInstance()->Play(eSoundId::sLOW_SWORD);
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += JUGGER_DAGGER_OFFSEYY;
	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(JUGGER_DAGGER_OFFSEYX, 0);
		velocity.x = -velocity.x;
	}
	else
	{
		pos -= GVector2(JUGGER_DAGGER_OFFSEYX, 0);
	}

	auto dagger = ObjectFactory::getDagger(pos, velocity, gravity);
	addToScene.Emit(dagger);
}
