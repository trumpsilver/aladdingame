﻿#include "Jar.h"
#include "../../Framework/Singleton/ScoreManager.h"

void JarPhysicsComponent::init()
{
	_movingSpeed = 100;
	auto movement = new Movement(GVector2(10, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -JAR_GRAVITY), movement);
}

GVector2 JarPhysicsComponent::getVelocity()
{
	auto move = (Movement*)getComponent("Movement");
	return move->getVelocity();
}


RECT JarPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void JarAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CIVILIAN);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CIVILIAN, "jar_peep_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::BORING1] = new Animation(_sprite, 0.1f);
	_animations[eStatus::BORING1]->addFrameRect(eObjectID::CIVILIAN, "jar_peep_1", NULL);;
	_animations[eStatus::BORING1]->canAnimate(false);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::CIVILIAN, "jar_peep_1", "jar_peep_2", "jar_peep_3", "jar_peep_4", "jar_peep_5", "jar_peep_6", NULL);;

	_animations[eStatus::SLASH] = new Animation(_sprite, 0.1f);
	_animations[eStatus::SLASH]->addFrameRect(eObjectID::CIVILIAN,"jar_punch_1", "jar_punch_2" , "jar_punch_3", "jar_punch_4", NULL);

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.1f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::CIVILIAN, "jar_tiptoe_1", "jar_tiptoe_2", "jar_tiptoe_3", "jar_tiptoe_4", "jar_tiptoe_5", "jar_tiptoe_6", "jar_tiptoe_7", "jar_tiptoe_8", "jar_tiptoe_9", "jar_tiptoe_10", "jar_tiptoe_11", "jar_tiptoe_12", NULL);

}

void JarBehaviorComponent::init()
{
	reset();
	auto rect = SpriteResource::getInstance()->getSourceRect(eObjectID::CIVILIAN, "jar_peep_1");
	_width = rect.right - rect.left;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void JarBehaviorComponent::update(float detatime)
{
	//Khai báo 1 số thứ cần thiết
	Land * landObject;
	Movement * move;

	checkCollision(detatime);
	if (_hitpoint <= 0)
	{
		ScoreManager::getInstance()->addScore(100);
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
		setStatus(eStatus::DESTROY);
		//add explosion
		GVector2 pos = _obj->getPhysicsComponent()->getPosition();
		RECT enemyBound = _obj->getPhysicsComponent()->getBounding();
		pos.x = ((enemyBound.right + enemyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX1);
		addToScene.Emit(explosion);
		return;
	}

	_standTime -= detatime;
	if (_standTime > 0)
	{
		setStatus(eStatus::NORMAL);
		standing();
		return;
	}
	//not collison with land falling
	landObject = (Land *)_collisionComponent->isColliding(eObjectID::LAND);
	if (landObject == nullptr)
	{
		falling();
		return;
	}

	//calculator aladdin position
	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	RECT aladdinBound = aladdin->getAnimationComponent()->getBounding();
	float aladdinPosX;
	if (_facingDirection == eDirection::LEFT)
	{
		aladdinPosX = aladdinBound.right;
	}
	else
	{
		aladdinPosX = aladdinBound.left;
	}
	//diffirent between nahbi and aladdin pos
	float diffirent = _obj->getPhysicsComponent()->getPosition().x - aladdinPosX;
	//calculator max slash distance
	float maxSlashDistance = JAR_MIN_DISTANCE;
	float posX = _obj->getPhysicsComponent()->getPositionX();
	switch (_status)
	{

	case NORMAL:
		_timer += detatime;
		if (_timer >= JAR_STAND_TIME)
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
		}
		else
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
		}
		//Chờ animation ngó lên
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			//Kiểm tra aladdin có trong tầm thì chém
			if (diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance && posX >= _rangeXStart && posX <= _rangeXEnd)
			{
				if (diffirent > 0)
				{
					faceLeft();
				}
				else
				{
					faceRight();
				}
				standing();
				slash();
				break;
			}
			//Có 3 tầm hoạt động 
			//1.posX < _rangeXStart nhân vật đi quá đà, chỉ có thể qua phải
			//2._rangeXStart  <= posX <= _rangeXEnd nhân vật có thể qua trái lẫn phải
			//3._rangeXEnd > _posXEnd nhân vật đi quá đà, chỉ có thể qua trái
			if (posX >= _rangeXStart && posX <= _rangeXEnd)
			{
				if (diffirent > 0)
				{
					moveLeft();
					setStatus(eStatus::RUNNING);
					break;
				}
				else
				{
					moveRight();
					setStatus(eStatus::RUNNING);
					break;
				}
			}
			else if (posX < _rangeXStart)
			{
				if (diffirent < 0)
				{
					moveRight();
					setStatus(eStatus::RUNNING);
					break;
				}
				else
				{
					_timer = 0;
				}
			}
			else if (posX > _rangeXEnd)
			{
				if (diffirent > 0)
				{
					moveLeft();
					setStatus(eStatus::RUNNING);
					break;
				}
				else
				{
					_timer = 0;
				}
			}
		}
		
		break;
	case RUNNING:
		if(!SoundManager::getInstance()->IsPlaying(eSoundId::sTIP_TOE))
		{
			SoundManager::getInstance()->Play(eSoundId::sTIP_TOE);
		}
		_timer += detatime;
		if (_timer >= JAR_MOVING_TIME)
		{
			setStatus(eStatus::NORMAL);
			break;
		}
		if (diffirent > maxSlashDistance && posX >= _rangeXStart) // aladdin ở bến trái
		{
			moveLeft();
			break;
		}
		if (diffirent < -maxSlashDistance && posX <= _rangeXEnd) // aladdin ở bên phải
		{
			moveRight();
			break;
		}
		//Trong phạm vi thì đứng lại hoặc quá phạm vi thì đứng lại
		if ((diffirent <= maxSlashDistance && diffirent >= -maxSlashDistance) || posX <= _rangeXStart || posX >= _rangeXEnd)
		{
			standing();
			_timer = 0;
			setStatus(eStatus::NORMAL);
		}
		break;
	case SLASH: //state chỉ chờ để chém xong
		if (_obj->getAnimationComponent()->isTempAnimationEmpty() == true)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	default:
		break;
	}

}

void JarBehaviorComponent::setStatus(int status)
{

	BehaviorComponent::setStatus(status);
	if (_status == eStatus::NORMAL || _status == eStatus::RUNNING)
	{
		
		_timer = 0;
	}
	updateAnimation();
}

void JarBehaviorComponent::dropHitpoint(int damage)
{

	if (_status == eStatus::NORMAL) // dang ở trong lọ thì không mất máu đc
		return;

	EnemyBehaviorComponent::dropHitpoint(damage);

}

void JarBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
	faceLeft();
	_hitpoint = 50;
	_standTime = 0;
	_timer = 0;
}

void JarBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

void JarBehaviorComponent::checkCollision(float deltatime)
{
	//declare
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case LAND:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				colliSide = _collisionComponent->getCollidingDirection(obj);
				if (colliSide == eDirection::TOP && _collisionComponent->isColliding(obj, moveX, moveY, deltatime) && _obj->getPhysicsComponent()->getPositionY() > obj->getPhysicsComponent()->getBounding().bottom)
				{
					standing();
					eDirection direction = _collisionComponent->getSide(obj);
					_collisionComponent->updateTargetPosition(obj, colliSide, GVector2(moveX, moveY));;
				}
			}
			break;

		default:
			break;
		}
	}
}

void JarBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case BORING1:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case NORMAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	case SLASH:
		_obj->getAnimationComponent()->setAnimation(eStatus::SLASH);
		break;
	default:
		break;
	}
}



void JarBehaviorComponent::slash(float offset, float width)
{
	setStatus(eStatus::SLASH);
	setWeapon(eStatus::SLASH);
	_obj->getAnimationComponent()->setTempAnimation(eStatus::SLASH, 1);
	//Tính toạn tọa độ kích thước của kiếm
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += _obj->getAnimationComponent()->getSprite()->getFrameHeight();
	float height = _obj->getAnimationComponent()->getSprite()->getFrameHeight();

	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(offset, 0);
	}
	else
	{
		pos -= GVector2(offset + width, 0);
	}

	auto sword = ObjectFactory::getSword(pos, width, height, false);
	addToScene.Emit(sword);

}
