﻿#include "Falza.h"
#include "../../Framework/Singleton/ScoreManager.h"

void FalzaPhysicsComponent::init()
{
	_movingSpeed = 100;
	auto movement = new Movement(GVector2(10, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -FALZA_GRAVITY), movement);
}

GVector2 FalzaPhysicsComponent::getVelocity()
{
	auto move = (Movement*)getComponent("Movement");
	return move->getVelocity();
}


RECT FalzaPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void FalzaAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::GUARD);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "falza_boring_7"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::GUARD, "falza_normal_1", NULL);

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.1f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::GUARD, "falza_running_1", "falza_running_2", "falza_running_3", "falza_running_4", "falza_running_5", "falza_running_6", "falza_running_7", "falza_running_8", NULL);

	_animations[eStatus::THROW] = new Animation(_sprite, 0.1f);
	_animations[eStatus::THROW]->addFrameRect(eObjectID::GUARD, "falza_throw_1", "falza_throw_2", "falza_throw_3", "falza_throw_4", "falza_throw_5", NULL);
	vector<float> customTime(5, 0.1f);
	customTime[0] = 0.5f;
	_animations[eStatus::THROW]->setCustomTime(customTime);

	_animations[eStatus::BEATEN] = new Animation(_sprite, 0.1f);
	_animations[eStatus::BEATEN]->addFrameRect(eObjectID::GUARD, "falza_beaten_1", "falza_beaten_2", "falza_beaten_3", "falza_beaten_4", "falza_beaten_5", "falza_beaten_6", "falza_beaten_7", NULL);
	vector<float> customTime2(7, 0.1f);
	customTime2[6] = 0.5f;
	_animations[eStatus::BEATEN]->setCustomTime(customTime2);

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void FalzaBehaviorComponent::init()
{
	reset();
	auto rect = SpriteResource::getInstance()->getSourceRect(eObjectID::GUARD, "falza_normal_1");
	_width = rect.right - rect.left;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void FalzaBehaviorComponent::update(float detatime)
{
	_throwTime += detatime;
	//Khai báo 1 số thứ cần thiết
	Land * landObject;
	Movement * move;

	checkCollision(detatime);
	if (_hitpoint <= 0)
	{
		ScoreManager::getInstance()->addScore(100);
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
		setStatus(eStatus::DESTROY);
		//add explosion
		GVector2 pos = _obj->getPhysicsComponent()->getPosition();
		RECT enemyBound = _obj->getPhysicsComponent()->getBounding();
		pos.x = ((enemyBound.right + enemyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX1);
		addToScene.Emit(explosion);
		return;
	}


	//not collison with land falling
	landObject = (Land *)_collisionComponent->isColliding(eObjectID::LAND);
	if (landObject == nullptr)
	{
		falling();
		return;
	}

	//calculator aladdin position
	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	RECT aladdinBound = aladdin->getAnimationComponent()->getBounding();
	float aladdinPosX;
	if (_facingDirection == eDirection::LEFT)
	{
		aladdinPosX = aladdinBound.right;
	}
	else
	{
		aladdinPosX = aladdinBound.left;
	}
	//diffirent between nahbi and aladdin pos
	float diffirent = _obj->getPhysicsComponent()->getPosition().x - aladdinPosX;
	//calculator max slash distance
	float maxThrowDistance = FALZA_MAX_THROW_DISTANT;
	float posX = _obj->getPhysicsComponent()->getPositionX();
	switch (_status)
	{
	case NORMAL:
		//Kiểm tra aladdin có trong tầm thì chém
		if (diffirent <= maxThrowDistance && diffirent >= -maxThrowDistance)
		{
			if (diffirent > 0)
			{
				faceLeft();
			}
			else
			{
				faceRight();
			}
			standing();
			if (_throwTime >= FALZA_THROW_INTERVAL)
			{
				_throwTime = 0;
				setStatus(eStatus::THROW);
			}		
			break;
		}
		//Có 3 tầm hoạt động 
		//1.posX < _rangeXStart nhân vật đi quá đà, chỉ có thể qua phải
		//2._rangeXStart  <= posX <= _rangeXEnd nhân vật có thể qua trái lẫn phải
		//3._rangeXEnd > _posXEnd nhân vật đi quá đà, chỉ có thể qua trái
		if (posX >= _rangeXStart && posX <= _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
			else
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
		}
		else if (posX < _rangeXStart)
		{
			if (diffirent < 0)
			{
				moveRight();
				setStatus(eStatus::RUNNING);
			}
		}
		else if (posX > _rangeXEnd)
		{
			if (diffirent > 0)
			{
				moveLeft();
				setStatus(eStatus::RUNNING);
			}
		}
		break;
	case RUNNING:
		if (diffirent > maxThrowDistance && posX >= _rangeXStart) // aladdin ở bến trái
		{
			moveLeft();
			break;
		}
		if (diffirent < -maxThrowDistance && posX <= _rangeXEnd) // aladdin ở bên phải
		{
			moveRight();
			break;
		}
		//Trong phạm vi thì đứng lại hoặc quá phạm vi thì đứng lại
		if ((diffirent <= maxThrowDistance && diffirent >= -maxThrowDistance) || posX <= _rangeXStart || posX >= _rangeXEnd)
		{
			standing();
			setStatus(eStatus::NORMAL);
		}
		break;
	case THROW: //state chỉ chờ để chém xong
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() == 2)
		{
			if(_weapon != eStatus::THROW)
				throwApple();
		}
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	case BEATEN:
		standing();
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
		{
			setStatus(eStatus::NORMAL);
			setWeapon(eStatus::NORMAL);
		}
		break;
	default:
		break;
	}

}

void FalzaBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	updateAnimation();
}

void FalzaBehaviorComponent::dropHitpoint(int damage)
{
	EnemyBehaviorComponent::dropHitpoint(damage);
	if (_hitpoint > 0)
	{
		SoundManager::getInstance()->Play(eSoundId::sFALZA_PANTS);
	}
	else
	{
		SoundManager::getInstance()->Play(eSoundId::sGUARD_DIE);
	}
	setStatus(eStatus::BEATEN);
	_standTime = STAND_TIME;
}

void FalzaBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
	_hitpoint = 100;
	_throwTime = THROW_INTERVAL;
	_standTime = 0;
	faceLeft();
}

void FalzaBehaviorComponent::respawn()
{
	reset();
	_obj->getPhysicsComponent()->setPosition(_respawnPosition);
}

void FalzaBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case NORMAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	case THROW:
		_obj->getAnimationComponent()->setAnimation(eStatus::THROW);
		break;
	case BEATEN:
		_obj->getAnimationComponent()->setAnimation(eStatus::BEATEN);
		break;
	default:
		break;
	}
}



void FalzaBehaviorComponent::checkCollision(float deltatime)
{
	//declare
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case LAND:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				colliSide = _collisionComponent->getCollidingDirection(obj);
				if (colliSide == eDirection::TOP && _collisionComponent->isColliding(obj, moveX, moveY, deltatime) && _obj->getPhysicsComponent()->getPositionY() > obj->getPhysicsComponent()->getBounding().bottom)
				{
					standing();
					eDirection direction = _collisionComponent->getSide(obj);
					_collisionComponent->updateTargetPosition(obj, colliSide, GVector2(moveX, moveY));;
				}
			}
			break;

		default:
			break;
		}
	}
}

void FalzaBehaviorComponent::throwApple()
{
	setWeapon(eStatus::THROW);
	setStatus(eStatus::THROW);
	SoundManager::getInstance()->Play(eSoundId::sSWORD_SPINNING);
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += FALZA_DAGGER_OFFSETY;
	GVector2 velocity(-800, 0);

	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(FALZA_DAGGER_OFFSETX, 0);
		velocity.x = -velocity.x;
	}
	else
	{
		pos -= GVector2(FALZA_DAGGER_OFFSETX, 0);
	}

	auto dagger = ObjectFactory::getDagger(pos, velocity);
	addToScene.Emit(dagger);
	pos.x += 10;
	pos.y += 10;
	auto dagger2 = ObjectFactory::getDagger(pos, velocity);
	addToScene.Emit(dagger2);

}