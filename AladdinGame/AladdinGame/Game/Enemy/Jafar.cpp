﻿#include "Jafar.h"
#include "../Scene/SceneManager.h"
#include "../Object/StarCreator.h"


void JafarPhysicsComponent::init()
{
}


RECT JafarPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void JafarAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::JAFAR);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "jafar_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eJafarStatus::jLAUGHT] = new Animation(_sprite, 0.1f);
	_animations[eJafarStatus::jLAUGHT]->addFrameRect(eObjectID::JAFAR, "jafar_1", "jafar_2", NULL);

	_animations[eJafarStatus::jNORMAL] = new Animation(_sprite, 0.07f);
	_animations[eJafarStatus::jNORMAL]->addFrameRect(eObjectID::JAFAR, "jafar_3", "jafar_4", "jafar_5", "jafar_6", "jafar_7", "jafar_8", NULL);
	vector<float> customTime2(6, 0.1f);
	customTime2[3] = JAFAR_PULL_TIME / 1000.f;
	_animations[eJafarStatus::jNORMAL]->setCustomTime(customTime2);

	_animations[eJafarStatus::jRAMPAGE] = new Animation(_sprite, 0.12f);
	_animations[eJafarStatus::jRAMPAGE]->addFrameRect(eObjectID::JAFAR, "snake_1", "snake_2", "snake_3", "snake_4", "snake_5", "snake_6", "snake_7", "snake_8", "snake_9", "snake_10", "snake_11",  NULL);

}

RECT JafarAnimationComponent::getBounding()
{
	_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());
	if (_obj->getBehaviorComponent()->getStatus() == jNORMAL)
	{
		RECT newRect = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "jafar_1");
		RECT oldRect = _sprite->getFrameRect();
		_sprite->setFrameRect(newRect);
		RECT bound = _sprite->getBounding();
		_sprite->setFrameRect(oldRect);
		return bound;
	}
	return AnimationComponent::getBounding();

}



sigcxx::Signal<> JafarBehaviorComponent::rampageMode;


void JafarBehaviorComponent::init()
{
	_width = 65;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
	_starCreator = new StarCreator();
	_starCreator->setTrackerObj(SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN));
	_starCreator->setPostion(GVector2(600, 300));
	reset();
}

void JafarBehaviorComponent::update(float detatime)
{
	//Bám theo hướng aladdin
	GVector2 aladdinPos = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN)->getPhysicsComponent()->getPosition();
	if (aladdinPos.x > _obj->getPhysicsComponent()->getPositionX())
	{
		faceRight();
	}
	else
	{
		faceLeft();
	}
	checkCollision(detatime);
	switch (_status)
	{
	case jLAUGHT:
		if (!SoundManager::getInstance()->IsPlaying(eSoundId::sJAFAR_LAUGH))
		{
			SoundManager::getInstance()->Play(eSoundId::sJAFAR_LAUGH);
		}
		_timer += detatime;
		if (_timer >= JAFAR_LAUGHT_TIME)
			setStatus(jNORMAL);
		break;
	case jNORMAL:
		if (_hitpoint <= JAFAR_HIT_POINT / 2)
		{
			setStatus(jRAMPAGE);
			break;
		}
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() == JAFAR_STAR_FRAME)
		{
			if (!SoundManager::getInstance()->IsPlaying(eSoundId::sJAFAR_TRACTOR))
			{
				SoundManager::getInstance()->Play(eSoundId::sJAFAR_TRACTOR);
			}
			_starCreator->update(detatime);
			break;
		}
		break;
	case jRAMPAGE:
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() == JAFAR_FLAME_FRAME)
		{
			dropFrame();
		}
		break;
	case jFLAME:
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() != JAFAR_FLAME_FRAME)
		{
			setStatus(jRAMPAGE);
		}
		break;
	default:
		break;
	}
}

void JafarBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	_timer = 0;
	if (_status == jRAMPAGE)
	{
		rampageMode.Emit();
	}
	updateAnimation();
}

void JafarBehaviorComponent::dropHitpoint(int damage)
{
	EnemyBehaviorComponent::dropHitpoint(damage);
	if (_status == jRAMPAGE)
	{
		SoundManager::getInstance()->Play(eSoundId::sJAFAR_SNAKE);
	}
}

void JafarBehaviorComponent::reset()
{
	_hitpoint = JAFAR_HIT_POINT;
	faceRight();
	setStatus(jNORMAL);
}

void JafarBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

void JafarBehaviorComponent::faceLeft()
{
	if (_obj->getAnimationComponent()->getScale().x > 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + _width);
		//đặt vị trí starcreator
		RECT bound = _obj->getAnimationComponent()->getBounding();
		GVector2 myPos;
		myPos.x = bound.right;
		myPos.y = bound.bottom;
		_starCreator->setPullDest(myPos);
		myPos.x -= JAFAR_STAR_OFFSETX;
		myPos.y += JAFAR_STAR_OFFSETY;
		_starCreator->setPostion(myPos);
	}
	setFacingDirection(eDirection::LEFT);
}

void JafarBehaviorComponent::faceRight()
{
	if (_obj->getAnimationComponent()->getScale().x < 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - _width);

		RECT bound = _obj->getAnimationComponent()->getBounding();
		GVector2 myPos;
		myPos.x = bound.left;
		myPos.y = bound.bottom;
		_starCreator->setPullDest(myPos);
		myPos.x += JAFAR_STAR_OFFSETX;
		myPos.y += JAFAR_STAR_OFFSETY;
		_starCreator->setPostion(myPos);
	}
	setFacingDirection(eDirection::RIGHT);
}

void JafarBehaviorComponent::checkCollision(float deltatime)
{
	//declare
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case ALADDIN:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				PlayerBehaviorComponent * encom = (PlayerBehaviorComponent *)obj->getBehaviorComponent();
				encom->dropHitpoint(100);
				if(_status == jNORMAL)
					setStatus(jLAUGHT);
			}
			break;

		default:
			break;
		}
	}
}

void JafarBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case jNORMAL:
		_obj->getAnimationComponent()->setAnimation(jNORMAL);
		break;
	case jLAUGHT:
		_obj->getAnimationComponent()->setAnimation(jLAUGHT);
		break;
	case jRAMPAGE:
		_obj->getAnimationComponent()->setAnimation(jRAMPAGE);
		break;
	default:
		break;
	}
}

void JafarBehaviorComponent::dropFrame()
{
	setStatus(jFLAME);
	setWeapon(eStatus::SLASH);

	RECT bound = _obj->getAnimationComponent()->getBounding();
	GVector2 pos;
	pos.y = bound.bottom + JAFAR_FRAME_OFFSETY;

	if (_facingDirection == eDirection::RIGHT)
	{
		pos.x = bound.left + JAFAR_FRAME_OFFSETX;
	}
	else
	{
		pos.x = bound.right - JAFAR_FRAME_OFFSETX;
	}

	auto sword = ObjectFactory::getSnakeFlame(pos, (eDirection)_facingDirection);
	addToScene.Emit(sword);
}
