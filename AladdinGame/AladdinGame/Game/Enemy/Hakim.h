﻿
#ifndef __HAKIM_H__
#define __HAKIM_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../Share/ShareBehavior.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define STAND_TIME 560
#define HAKIM_GRAVITY 800
#define HAKIM_SWORD_OFFSET 57 * SCALE_FACTOR
#define HAKIM_SWORD_WIDTH 32 * SCALE_FACTOR

class HakimPhysicsComponent : public PhysicsComponent
{
public:
	void init();

	GVector2 getVelocity();
	RECT getBounding() override;
protected:
};

class HakimAnimationComponent : public AnimationComponent
{
public:
	void init();
private:

};

class HakimBehaviorComponent : public EnemyBehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setStatus(int status) override;
	void dropHitpoint(int damage) override;
	void reset() override;
	void respawn() override;
private:
	void checkCollision(float deltatime);
	void updateAnimation();
	void slash(float offset = HAKIM_SWORD_OFFSET, float height = HAKIM_SWORD_WIDTH);

	float _standTime; // sau khi bị đánh phải đứng lại một khoảng thời gian


};


#endif

