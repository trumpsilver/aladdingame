#include "StringDraw.h"

StringDraw* StringDraw::_instance;

StringDraw::StringDraw(void)
{
}
StringDraw::~StringDraw(void)
{
}

void StringDraw::release()
{
}

StringDraw * StringDraw::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new StringDraw();
	}
	return _instance;
}

void StringDraw::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::MISCITEMS);
	_sprite->setScale(SCALE_FACTOR);
	_sprite->setOrigin(GVector2(.0f, .0f));
	_sprite->setColor(D3DXCOLOR(1.f, 1.f, 1.f, 1.f));

	_charRect['a'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_a");
	_charRect['b'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_b");
	_charRect['c'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_c");
	_charRect['d'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_d");
	_charRect['e'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_e");
	_charRect['f'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_f");
	_charRect['g'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_g");
	_charRect['h'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_h");
	_charRect['j'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_j");
	_charRect['k'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_k");
	_charRect['l'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_l");
	_charRect['m'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_m");
	_charRect['n'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_n");
	_charRect['o'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_o");
	_charRect['p'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_p");
	_charRect['q'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_q");
	_charRect['r'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_r");
	_charRect['s'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_s");
	_charRect['t'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_t");
	_charRect['u'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_u");
	_charRect['v'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_v");
	_charRect['w'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_w");
	_charRect['x'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_x");
	_charRect['y'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_y");
	_charRect['z'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_z");
	_charRect['('] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_(");
	_charRect[')'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_)");
	_charRect['0'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_0");
	_charRect['1'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_1");
	_charRect['2'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_2");
	_charRect['3'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_3");
	_charRect['4'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_4");
	_charRect['5'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_5");
	_charRect['6'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_6");
	_charRect['7'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_7");
	_charRect['8'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_8");
	_charRect['9'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_9");
	_charRect['.'] = SpriteResource::getInstance()->getSourceRect(eObjectID::MISCITEMS, "character_.");

	_charwidth = (_charRect['a'].right - _charRect['a'].left) * SCALE_FACTOR;
}

void StringDraw::draw(LPD3DXSPRITE spriteHandle, string text, GVector2 pos, float size)
{
	_sprite->setScale(SCALE_FACTOR * size);
	float width = _charwidth * size;
	for (auto c : text)
	{
		_sprite->setPosition(pos);
		_sprite->setFrameRect(_charRect[c]);
		_sprite->render(spriteHandle);
		pos.x += width;
	}
}
