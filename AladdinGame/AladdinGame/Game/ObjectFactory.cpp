﻿#include "ObjectFactory.h"

ObjectFactory::ObjectFactory()
{
}

ObjectFactory::~ObjectFactory()
{
}


map<string, GameObject*>* ObjectFactory::getMapObjectFromFile(const string path)
{
	pugi::xml_document doc;
	map<string, GameObject*>* listobject = new map<string, GameObject*>();

	// Mở file và đọc
	xml_parse_result result = doc.load_file(path.data(), pugi::parse_default | pugi::parse_pi);
	if (result == false)
	{
		return listobject;
	}

	xml_node objects = doc.child("Objects");
	auto list = objects.children();

	// Lấy id từ file xml. so sánh với eID, tuỳ theo eID nào mà gọi đến đúng hàm load cho riêng object đó.
	for (auto item : list)
	{
		int id = item.attribute("Id").as_int();
		string name = item.attribute("Name").as_string();
		eObjectID enumID;
		try {
			enumID = (eObjectID)id;
		}
		catch (exception e) {
			continue;
		}
		GameObject* obj = getObjectById(item, enumID);
		if (obj != NULL)
			(*listobject)[name] = obj;
	}
	return listobject;
}

map<string, std::function<GameObject*()>>* ObjectFactory::getMapObjectFunctionFromFile(const string path)
{
	pugi::xml_document doc;
	map<string, std::function<GameObject*()>>* listobject = new map<string, std::function<GameObject*()>>();

	// Mở file và đọc
	xml_parse_result result = doc.load_file(path.data(), pugi::parse_default | pugi::parse_pi);
	if (result == false)
	{
		return listobject;
	}

	xml_node objects = doc.child("Objects");
	auto list = objects.children();

	// Lấy id từ file xml. so sánh với eID, tuỳ theo eID nào mà gọi đến đúng hàm load cho riêng object đó.
	for (auto item : list)
	{
		int id = item.attribute("Id").as_int();
		string name = item.attribute("Name").as_string();
		eObjectID enumID;
		try {
			enumID = (eObjectID)id;
		}
		catch (exception e) {
			continue;
		}
		auto obj = getFunctionById(item, enumID);
		if (obj != NULL)
			(*listobject)[name] = obj;
	}
	return listobject;
}

GameObject* ObjectFactory::getObjectById(xml_node node, eObjectID id)
{
	switch (id)
	{
	case LAND:
		return getLand(node);
	case ROPE:
		return getRope(node);
	case THROWER:
		return getThrower(node);
	case HAKIM:
		return getHakim(node);
	case FALZA:
		return getFalza(node);
	case NAHBI:
		return getNahbi(node);
	case WALL:
		return getWall(node);
	case CAMEL:
		return getCamel(node);
	case JAR:
		return getJar(node);
	case JUGGER:
		return getJugger(node);
	case ITEM:
		return getItem(node);
	case RESILIENCE:
		return getResilience(node);
	default:
		break;
	}
}

std::function<GameObject*() > ObjectFactory::getFunctionById(xml_node node, eObjectID id)
{
	switch (id)
	{
	case LAND:
		return bind(getLand, node);
		break;
	case ROPE:
		return bind(getRope, node);
		break;
	default:
		break;
	}
}

GameObject * ObjectFactory::getAladdin()
{
	auto aladdin = new GameObject(eObjectID::ALADDIN);

	auto physicsComponent = new AladdinPhysicsComponent();
	physicsComponent->setGameObject(aladdin);

	auto animationComponent = new AladdinAnimationComponent();
	animationComponent->setGameObject(aladdin);

	auto behaviorComponent = new AladdinBehaviorComponent();
	behaviorComponent->setGameObject(aladdin);
	behaviorComponent->setGameController(GameController::getInstance());

	aladdin->setPhysicsComponent(physicsComponent);
	aladdin->setAnimationComponent(animationComponent);
	aladdin->setBehaviorComponent(behaviorComponent);

	aladdin->init();

	return aladdin;
}

GameObject * ObjectFactory::getApple(GVector2 pos, GVector2 velocity)
{
	auto Apple = new GameObject(eObjectID::APPLE);

	auto physicsComponent = new ApplePhysicsComponent();
	physicsComponent->setGameObject(Apple);
	physicsComponent->setPosition(pos);

	auto animationComponent = new AppleAnimationComponent();
	animationComponent->setGameObject(Apple);

	auto behaviorComponent = new AppleBehaviorComponent();
	behaviorComponent->setGameObject(Apple);
	behaviorComponent->setGameController(GameController::getInstance());

	Apple->setPhysicsComponent(physicsComponent);
	Apple->setAnimationComponent(animationComponent);
	Apple->setBehaviorComponent(behaviorComponent);

	Apple->init();
	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);

	return Apple;
}

GameObject * ObjectFactory::getApple(GVector2 pos, GVector2 velocity, GVector2 gravity)
{
	auto Apple = new GameObject(eObjectID::APPLE);

	auto physicsComponent = new ApplePhysicsComponent();
	physicsComponent->setGameObject(Apple);
	physicsComponent->setPosition(pos);

	auto animationComponent = new AppleAnimationComponent();
	animationComponent->setGameObject(Apple);

	auto behaviorComponent = new AppleBehaviorComponent();
	behaviorComponent->setGameObject(Apple);
	behaviorComponent->setGameController(GameController::getInstance());

	Apple->setPhysicsComponent(physicsComponent);
	Apple->setAnimationComponent(animationComponent);
	Apple->setBehaviorComponent(behaviorComponent);

	Apple->init();
	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);
	auto gravityc = (Gravity*)physicsComponent->getComponent("Gravity");
	gravityc->setGravity(gravity);

	return Apple;
}

GameObject * ObjectFactory::getApple(GVector2 start, GVector2 end, GVector2 gravity, float time)
{
	auto Apple = new GameObject(eObjectID::APPLE);

	auto physicsComponent = new ApplePhysicsComponent();
	physicsComponent->setGameObject(Apple);
	physicsComponent->setPosition(start);

	auto animationComponent = new AppleAnimationComponent();
	animationComponent->setGameObject(Apple);

	auto behaviorComponent = new AppleBehaviorComponent();
	behaviorComponent->setGameObject(Apple);
	behaviorComponent->setGameController(GameController::getInstance());

	Apple->setPhysicsComponent(physicsComponent);
	Apple->setAnimationComponent(animationComponent);
	Apple->setBehaviorComponent(behaviorComponent);

	Apple->init();
	auto gravityc = (Gravity*)physicsComponent->getComponent("Gravity");
	gravityc->setGravity(gravity);


	GVector2 velocity;
	float sx = end.x - start.x;
	float sy = end.y - start.y;
	velocity.x = gravity.x * time / 2 - sx / time;
	velocity.y = gravity.y * time / 2 - sy / time;

	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);



	return Apple;
}

GameObject * ObjectFactory::getDagger(GVector2 pos, GVector2 velocity)
{
	auto Dagger = new GameObject(eObjectID::DAGGER);

	auto physicsComponent = new DaggerPhysicsComponent();
	physicsComponent->setGameObject(Dagger);
	physicsComponent->setPosition(pos);

	auto animationComponent = new DaggerAnimationComponent();
	animationComponent->setGameObject(Dagger);

	auto behaviorComponent = new DaggerBehaviorComponent();
	behaviorComponent->setGameObject(Dagger);
	behaviorComponent->setGameController(GameController::getInstance());

	Dagger->setPhysicsComponent(physicsComponent);
	Dagger->setAnimationComponent(animationComponent);
	Dagger->setBehaviorComponent(behaviorComponent);

	Dagger->init();
	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);

	return Dagger;
}

GameObject * ObjectFactory::getDagger(GVector2 pos, GVector2 velocity, GVector2 gravity)
{
	auto Dagger = new GameObject(eObjectID::DAGGER);

	auto physicsComponent = new DaggerPhysicsComponent();
	physicsComponent->setGameObject(Dagger);
	physicsComponent->setPosition(pos);

	auto animationComponent = new DaggerAnimationComponent();
	animationComponent->setGameObject(Dagger);

	auto behaviorComponent = new DaggerBehaviorComponent();
	behaviorComponent->setGameObject(Dagger);
	behaviorComponent->setGameController(GameController::getInstance());

	Dagger->setPhysicsComponent(physicsComponent);
	Dagger->setAnimationComponent(animationComponent);
	Dagger->setBehaviorComponent(behaviorComponent);

	Dagger->init();
	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);
	auto gravityc = (Gravity*)physicsComponent->getComponent("Gravity");
	gravityc->setGravity(gravity);

	return Dagger;
}

GameObject * ObjectFactory::getDagger(GVector2 start, GVector2 end, GVector2 gravity, float time)
{
	auto Dagger = new GameObject(eObjectID::DAGGER);

	auto physicsComponent = new DaggerPhysicsComponent();
	physicsComponent->setGameObject(Dagger);
	physicsComponent->setPosition(start);

	auto animationComponent = new DaggerAnimationComponent();
	animationComponent->setGameObject(Dagger);

	auto behaviorComponent = new DaggerBehaviorComponent();
	behaviorComponent->setGameObject(Dagger);
	behaviorComponent->setGameController(GameController::getInstance());

	Dagger->setPhysicsComponent(physicsComponent);
	Dagger->setAnimationComponent(animationComponent);
	Dagger->setBehaviorComponent(behaviorComponent);

	Dagger->init();
	auto gravityc = (Gravity*)physicsComponent->getComponent("Gravity");
	gravityc->setGravity(gravity);


	GVector2 velocity;
	float sx = (end.x - start.x);
	float sy = (end.y - start.y);
	velocity.x = sx / time - gravity.x * time / 2;
	velocity.y = sy / time - gravity.y * time / 2;
	OutputDebugStringW(L"Velocity : ");
	__debugoutput(velocity.x);
	__debugoutput(velocity.y);

	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);



	return Dagger;
}

GameObject * ObjectFactory::getSword(GVector2 pos, float width, float height, bool canSlashEnemy)
{
	auto sword = new Sword();
	sword->setID(eObjectID::SWORD);

	auto physicsComponent = new SwordPhysicsComponent();
	physicsComponent->setGameObject(sword);

	auto behaviorComponent = new SwordBehaviorComponent();
	behaviorComponent->setGameObject(sword);

	sword->setPhysicsComponent(physicsComponent);
	sword->setBehaviorComponent(behaviorComponent);

	sword->setAnimationComponent(nullptr);
	sword->init(pos.x, pos.y, width, height, eDirection::ALL, canSlashEnemy);

	return sword;
}

GameObject * ObjectFactory::getLand(xml_node node)
{
	auto properties = getObjectProperties(node);
	if (properties.size() == 0)
		return nullptr;

	int x, y, width, height;
	int zIndex;
	eDirection dir;
	eLandType type;

	x = stoi(properties["X"]) * SCALE_FACTOR;
	y = stoi(properties["Y"]) * SCALE_FACTOR;
	width = stoi(properties["Width"]) * SCALE_FACTOR;
	height = stoi(properties["Height"]) * SCALE_FACTOR;

	if (properties.find("type") != properties.end())
	{
		type = (eLandType)(stoi(properties.find("type")->second));
	}
	else
	{
		type = eLandType::lNORMAL;
	}

	if (properties.find("physicBodyDirection") != properties.end())
	{
		dir = (eDirection)(stoi(properties.find("physicBodyDirection")->second));
	}
	else
	{
		dir = eDirection::TOP;
	}

	if (properties.find("zIndex") != properties.end())
	{
		zIndex = (int)(stoi(properties.find("zIndex")->second));
	}
	else
	{
		zIndex = 0;
	}
	auto land = new Land();

	auto behaviorComponent = new LandBehaviorComponent();
	PhysicsComponent * physicsComponent;
	AnimationComponent * animationComponent = nullptr;
	if (type == eLandType::lFALLING)
	{
		physicsComponent = new FallingLandPhysiscsComponent();
		animationComponent = new FallingLandAnimationComponent();
		animationComponent->setGameObject(land);
		physicsComponent->setGameObject(land);
		physicsComponent->setPosition(GVector2(x, y));
		behaviorComponent->setRespawnPosition(GVector2(x, y));
	}
	else
	{
		physicsComponent = new LandPhysiscsComponent();
	}
	behaviorComponent->setGameObject(land);

	
	land->setPhysicsComponent(physicsComponent);
	land->setBehaviorComponent(behaviorComponent);
	land->setAnimationComponent(animationComponent);
	land->init(x, y, width, height, dir, type);
	physicsComponent->setZIndex(zIndex);

	return land;
}

GameObject * ObjectFactory::getRope(xml_node node)
{
	auto properties = getObjectProperties(node);
	if (properties.size() == 0)
		return nullptr;

	int x, y, width, height;
	eDirection dir;
	eRopeType type;

	x = stoi(properties["X"]) * SCALE_FACTOR;
	y = stoi(properties["Y"]) * SCALE_FACTOR;
	width = stoi(properties["Width"]) * SCALE_FACTOR;
	height = stoi(properties["Height"]) * SCALE_FACTOR;

	if (properties.find("type") != properties.end())
	{
		type = (eRopeType)(stoi(properties.find("type")->second));
	}
	else
	{
		type = eRopeType::rVERTICAL;
	}

	if (properties.find("physicBodyDirection") != properties.end())
	{
		dir = (eDirection)(stoi(properties.find("physicBodyDirection")->second));
	}
	else
	{
		dir = eDirection::ALL;
	}

	auto physicsComponent = new RopePhysiscsComponent();

	auto rope = new Rope();
	rope->setPhysicsComponent(physicsComponent);
	rope->init(x, y, width, height, dir, type);

	return rope;
}

GameObject * ObjectFactory::getHakim(GVector2 pos, float rangeXStart, float rangeXEnd)
{
	auto Hakim = new GameObject(eObjectID::HAKIM);

	auto physicsComponent = new HakimPhysicsComponent();
	physicsComponent->setGameObject(Hakim);
	physicsComponent->setPosition(pos);

	auto animationComponent = new HakimAnimationComponent();
	animationComponent->setGameObject(Hakim);

	EnemyBehaviorComponent* behaviorComponent = new HakimBehaviorComponent();
	behaviorComponent->setGameObject(Hakim);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRange(rangeXStart, rangeXEnd);
	behaviorComponent->setRespawnPosition(pos);

	Hakim->setPhysicsComponent(physicsComponent);
	Hakim->setAnimationComponent(animationComponent);
	Hakim->setBehaviorComponent(behaviorComponent);

	Hakim->init();

	return Hakim;
}

GameObject * ObjectFactory::getHakim(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	float width = node.attribute("Width").as_float() * SCALE_FACTOR;
	auto bound = node.child("Bound");
	float rangeXStart = bound.attribute("Left").as_float() * SCALE_FACTOR + width;
	float rangeXEnd = bound.attribute("Right").as_float() * SCALE_FACTOR;
	return getHakim(pos, rangeXStart, rangeXEnd);
}

GameObject * ObjectFactory::getFalza(GVector2 pos, float rangeXStart, float rangeXEnd)
{
	auto Falza = new GameObject(eObjectID::FALZA);

	auto physicsComponent = new FalzaPhysicsComponent();
	physicsComponent->setGameObject(Falza);
	physicsComponent->setPosition(pos);

	auto animationComponent = new FalzaAnimationComponent();
	animationComponent->setGameObject(Falza);

	EnemyBehaviorComponent* behaviorComponent = new FalzaBehaviorComponent();
	behaviorComponent->setGameObject(Falza);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRange(rangeXStart, rangeXEnd);
	behaviorComponent->setRespawnPosition(pos);

	Falza->setPhysicsComponent(physicsComponent);
	Falza->setAnimationComponent(animationComponent);
	Falza->setBehaviorComponent(behaviorComponent);

	Falza->init();

	return Falza;
}

GameObject * ObjectFactory::getFalza(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	float width = node.attribute("Width").as_float() * SCALE_FACTOR;
	auto bound = node.child("Bound");
	float rangeXStart = bound.attribute("Left").as_float() * SCALE_FACTOR + width;
	float rangeXEnd = bound.attribute("Right").as_float() * SCALE_FACTOR;
	return getFalza(pos, rangeXStart, rangeXEnd);
}

GameObject * ObjectFactory::getNahbi(GVector2 pos, float rangeXStart, float rangeXEnd)
{
	auto Nahbi = new GameObject(eObjectID::NAHBI);

	auto physicsComponent = new NahbiPhysicsComponent();
	physicsComponent->setGameObject(Nahbi);
	physicsComponent->setPosition(pos);

	auto animationComponent = new NahbiAnimationComponent();
	animationComponent->setGameObject(Nahbi);

	EnemyBehaviorComponent* behaviorComponent = new NahbiBehaviorComponent();
	behaviorComponent->setGameObject(Nahbi);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRange(rangeXStart, rangeXEnd);
	behaviorComponent->setRespawnPosition(pos);

	Nahbi->setPhysicsComponent(physicsComponent);
	Nahbi->setAnimationComponent(animationComponent);
	Nahbi->setBehaviorComponent(behaviorComponent);

	Nahbi->init();

	return Nahbi;
}

GameObject * ObjectFactory::getNahbi(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	float width = node.attribute("Width").as_float() * SCALE_FACTOR;
	auto bound = node.child("Bound");
	float rangeXStart = bound.attribute("Left").as_float() * SCALE_FACTOR + width;
	float rangeXEnd = bound.attribute("Right").as_float() * SCALE_FACTOR;
	return getNahbi(pos, rangeXStart, rangeXEnd);
}

GameObject * ObjectFactory::getJafar(GVector2 pos)
{
	auto Jafar = new GameObject(eObjectID::JAFAR);

	auto physicsComponent = new JafarPhysicsComponent();
	physicsComponent->setGameObject(Jafar);
	physicsComponent->setPosition(pos);

	auto animationComponent = new JafarAnimationComponent();
	animationComponent->setGameObject(Jafar);

	EnemyBehaviorComponent* behaviorComponent = new JafarBehaviorComponent();
	behaviorComponent->setGameObject(Jafar);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRespawnPosition(pos);

	Jafar->setPhysicsComponent(physicsComponent);
	Jafar->setAnimationComponent(animationComponent);
	Jafar->setBehaviorComponent(behaviorComponent);

	Jafar->init();

	return Jafar;
}

GameObject * ObjectFactory::getJar(GVector2 pos, float rangeXStart, float rangeXEnd)
{
	auto Jar = new GameObject(eObjectID::JAR);

	auto physicsComponent = new JarPhysicsComponent();
	physicsComponent->setGameObject(Jar);
	physicsComponent->setPosition(pos);

	auto animationComponent = new JarAnimationComponent();
	animationComponent->setGameObject(Jar);

	EnemyBehaviorComponent* behaviorComponent = new JarBehaviorComponent();
	behaviorComponent->setGameObject(Jar);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRange(rangeXStart, rangeXEnd);
	behaviorComponent->setRespawnPosition(pos);

	Jar->setPhysicsComponent(physicsComponent);
	Jar->setAnimationComponent(animationComponent);
	Jar->setBehaviorComponent(behaviorComponent);

	Jar->init();

	return Jar;
}

GameObject * ObjectFactory::getJar(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	float width = node.attribute("Width").as_float() * SCALE_FACTOR;
	auto bound = node.child("Bound");
	float rangeXStart = bound.attribute("Left").as_float() * SCALE_FACTOR + width;
	float rangeXEnd = bound.attribute("Right").as_float() * SCALE_FACTOR;
	return getJar(pos, rangeXStart, rangeXEnd);
}

GameObject * ObjectFactory::getJugger(GVector2 pos, eDirection side)
{
	auto Jugger = new GameObject(eObjectID::JUGGER);

	auto physicsComponent = new JuggerPhysicsComponent();
	physicsComponent->setGameObject(Jugger);
	physicsComponent->setPosition(pos);

	auto animationComponent = new JuggerAnimationComponent();
	animationComponent->setGameObject(Jugger);

	JuggerBehaviorComponent* behaviorComponent = new JuggerBehaviorComponent();
	behaviorComponent->setGameObject(Jugger);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setRespawnPosition(pos);

	Jugger->setPhysicsComponent(physicsComponent);
	Jugger->setAnimationComponent(animationComponent);
	Jugger->setBehaviorComponent(behaviorComponent);

	Jugger->init();



	return Jugger;
}

GameObject * ObjectFactory::getJugger(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	eDirection side;
	bool isFaceLeft = node.attribute("FaceLeft").as_bool();
	if (isFaceLeft)
		side = eDirection::LEFT;
	else
		side = eDirection::RIGHT;
	return getJugger(pos, side);
}

GameObject * ObjectFactory::getFlame(GVector2 pos)
{
	auto Flame = new GameObject(eObjectID::FLAME);

	auto physicsComponent = new FlamePhysicsComponent();
	physicsComponent->setGameObject(Flame);
	physicsComponent->setPosition(pos);

	auto animationComponent = new FlameAnimationComponent();
	animationComponent->setGameObject(Flame);

	auto behaviorComponent = new FlameBehaviorComponent();
	behaviorComponent->setGameObject(Flame);
	behaviorComponent->setGameController(GameController::getInstance());

	Flame->setPhysicsComponent(physicsComponent);
	Flame->setAnimationComponent(animationComponent);
	Flame->setBehaviorComponent(behaviorComponent);

	Flame->init();

	return Flame;
}

GameObject * ObjectFactory::getSnakeFlame(GVector2 pos, eDirection side)
{
	auto SnakeFlame = new GameObject(eObjectID::FLAME);

	auto physicsComponent = new SnakeFlamePhysicsComponent();
	physicsComponent->setGameObject(SnakeFlame);
	physicsComponent->setPosition(pos);

	auto animationComponent = new SnakeFlameAnimationComponent();
	animationComponent->setGameObject(SnakeFlame);

	auto behaviorComponent = new SnakeFlameBehaviorComponent();
	behaviorComponent->setGameObject(SnakeFlame);
	behaviorComponent->setGameController(GameController::getInstance());

	SnakeFlame->setPhysicsComponent(physicsComponent);
	SnakeFlame->setAnimationComponent(animationComponent);
	SnakeFlame->setBehaviorComponent(behaviorComponent);

	SnakeFlame->init();
	if(side == eDirection::LEFT)
	{
		behaviorComponent->faceLeft();
		auto move = (Movement*)physicsComponent->getComponent("Movement");
		move->setVelocity(GVector2(-SNAKE_FLAME_VELOCITY, 0));
	}
	else
	{
		behaviorComponent->faceRight();
		auto move = (Movement*)physicsComponent->getComponent("Movement");
		move->setVelocity(GVector2(SNAKE_FLAME_VELOCITY, 0));
	}

	return SnakeFlame;
}

GameObject * ObjectFactory::getCamel(GVector2 pos)
{
	auto Camel = new GameObject(eObjectID::CAMEL);

	auto physicsComponent = new CamelPhysicsComponent();
	physicsComponent->setGameObject(Camel);
	physicsComponent->setPosition(pos);

	auto animationComponent = new CamelAnimationComponent();
	animationComponent->setGameObject(Camel);

	auto behaviorComponent = new CamelBehaviorComponent();
	behaviorComponent->setGameObject(Camel);

	Camel->setPhysicsComponent(physicsComponent);
	Camel->setAnimationComponent(animationComponent);
	Camel->setBehaviorComponent(behaviorComponent);

	Camel->init();

	return Camel;
}

GameObject * ObjectFactory::getCamel(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;
	return getCamel(pos);
}

GameObject * ObjectFactory::getExlplosionPot(GVector2 pos, GVector2 velocity)
{
	auto ExplosionPot = new GameObject(eObjectID::EXPLOSIONPOT);

	auto physicsComponent = new ExplosionPotPhysicsComponent();
	physicsComponent->setGameObject(ExplosionPot);
	physicsComponent->setPosition(pos);

	auto animationComponent = new ExplosionPotAnimationComponent();
	animationComponent->setGameObject(ExplosionPot);

	auto behaviorComponent = new ExplosionPotBehaviorComponent();
	behaviorComponent->setGameObject(ExplosionPot);
	behaviorComponent->setGameController(GameController::getInstance());

	ExplosionPot->setPhysicsComponent(physicsComponent);
	ExplosionPot->setAnimationComponent(animationComponent);
	ExplosionPot->setBehaviorComponent(behaviorComponent);

	ExplosionPot->init();

	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);

	return ExplosionPot;
}


GameObject * ObjectFactory::getThrower(GVector2 pos)
{
	auto Thrower = new GameObject(eObjectID::THROWER);

	auto physicsComponent = new ThrowerPhysicsComponent();
	physicsComponent->setGameObject(Thrower);
	physicsComponent->setPosition(pos);

	auto animationComponent = new ThrowerAnimationComponent();
	animationComponent->setGameObject(Thrower);

	auto behaviorComponent = new ThrowerBehaviorComponent();
	behaviorComponent->setGameObject(Thrower);
	behaviorComponent->setGameController(GameController::getInstance());

	Thrower->setPhysicsComponent(physicsComponent);
	Thrower->setAnimationComponent(animationComponent);
	Thrower->setBehaviorComponent(behaviorComponent);

	Thrower->init();

	return Thrower;
}

GameObject * ObjectFactory::getThrower(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;

	return getThrower(pos);
}

GameObject * ObjectFactory::getWall(xml_node node)
{
	auto properties = getObjectProperties(node);
	if (properties.size() == 0)
		return nullptr;

	int x, y, width, height;
	eDirection dir;

	x = stoi(properties["X"]) * SCALE_FACTOR;
	y = stoi(properties["Y"]) * SCALE_FACTOR;
	width = stoi(properties["Width"]) * SCALE_FACTOR;
	height = stoi(properties["Height"]) * SCALE_FACTOR;


	if (properties.find("physicBodyDirection") != properties.end())
	{
		dir = (eDirection)(stoi(properties.find("physicBodyDirection")->second));
	}
	else
	{
		dir = eDirection::ALL;
	}

	auto physicsComponent = new RopePhysiscsComponent();

	auto wall = new Wall();
	wall->setPhysicsComponent(physicsComponent);
	wall->init(x, y, width, height, dir);

	return wall;
}

GameObject * ObjectFactory::getExplosion(GVector2 pos, eExplostion type)
{
	auto Explosions = new GameObject(eObjectID::EXPLOSIONS);

	auto physicsComponent = new ExplosionsPhysicsComponent();
	physicsComponent->setGameObject(Explosions);

	auto animationComponent = new ExplosionsAnimationComponent();
	animationComponent->setGameObject(Explosions);
	animationComponent->setAnimation(type);

	auto behaviorComponent = new ExplosionsBehaviorComponent();
	behaviorComponent->setGameObject(Explosions);

	Explosions->setPhysicsComponent(physicsComponent);
	Explosions->setAnimationComponent(animationComponent);
	Explosions->setBehaviorComponent(behaviorComponent);

	physicsComponent->setPosition(pos);

	Explosions->init();

	return Explosions;
}

GameObject * ObjectFactory::getItem(GVector2 pos, eItem type)
{
	Item* item = new Item();

	auto physicsComponent = new ItemPhysicsComponent();
	physicsComponent->setGameObject(item);
	physicsComponent->setPosition(pos);

	
	AnimationComponent * animationComponent;
	switch (type)
	{
	case iEXTRA_HEART:
		animationComponent = new HeartItemAnimationComponent();
		break;
	case iAPPLE:
		animationComponent = new AppleItemAnimationComponent();
		break;
	case iMONEY:
		animationComponent = new MoneyItemAnimationComponent();
		break;
	case iRESTART_POINT:
		animationComponent = new CheckpointItemAnimationComponent();
		break;
	case i1UP:
		animationComponent = new LifeupItemAnimation();
		break;
	case iGENIE_BONUS:
		animationComponent = new GenieItemAnimationComponent();
		break;
	case iABU_BONUS:
		animationComponent = new AbuItemAnimationComponent();
		break;
	default:
		animationComponent = new AppleItemAnimationComponent();
		break;
	}
	animationComponent->setGameObject(item);

	ItemBehaviorComponent* behaviorComponent = new ItemBehaviorComponent();
	behaviorComponent->setGameObject(item);
	behaviorComponent->setRespawnPosition(pos);

	item->setPhysicsComponent(physicsComponent);
	item->setAnimationComponent(animationComponent);
	item->setBehaviorComponent(behaviorComponent);


	item->init(type);

	return item;
}

GameObject * ObjectFactory::getItem(xml_node node)
{
	auto properties = getObjectProperties(node);
	if (properties.size() == 0)
		return nullptr;

	int x, y;
	eItem type;

	x = stoi(properties["X"]) * SCALE_FACTOR;
	y = stoi(properties["Y"]) * SCALE_FACTOR;

	if (properties.find("type") != properties.end())
	{
		type = (eItem)(stoi(properties.find("type")->second));
	}
	else
	{
		type = eItem::iAPPLE;
	}
	return getItem(GVector2(x, y), type);

}

GameObject * ObjectFactory::getAbu(GVector2 pos, eStatus type)
{
	auto Abu = new GameObject(eObjectID::ABU);

	auto physicsComponent = new AbuPhysicsComponent();
	physicsComponent->setGameObject(Abu);

	auto animationComponent = new AbuAnimationComponent();
	animationComponent->setGameObject(Abu);
	animationComponent->setAnimation(type);

	auto behaviorComponent = new AbuBehaviorComponent();
	behaviorComponent->setGameObject(Abu);

	Abu->setPhysicsComponent(physicsComponent);
	Abu->setAnimationComponent(animationComponent);
	Abu->setBehaviorComponent(behaviorComponent);

	physicsComponent->setPosition(pos);

	Abu->init();

	return Abu;
}

GameObject * ObjectFactory::getCutscene(GVector2 pos, eCutscene type)
{
	auto Cutscene = new GameObject(eObjectID::CUTSCENES);

	auto physicsComponent = new CutscenePhysicsComponent();
	physicsComponent->setGameObject(Cutscene);

	auto animationComponent = new CutsceneAnimationComponent();
	animationComponent->setGameObject(Cutscene);
	animationComponent->setAnimation(type);

	auto behaviorComponent = new CutsceneBehaviorComponent();
	behaviorComponent->setGameObject(Cutscene);
	behaviorComponent->setStatus((eStatus)type);

	Cutscene->setPhysicsComponent(physicsComponent);
	Cutscene->setAnimationComponent(animationComponent);
	Cutscene->setBehaviorComponent(behaviorComponent);

	physicsComponent->setPosition(pos);

	Cutscene->init();

	return Cutscene;
}
GameObject * ObjectFactory::getResilience(GVector2 pos)
{
	auto Resilience = new GameObject(eObjectID::RESILIENCE);

	auto physicsComponent = new ResiliencePhysicsComponent();
	physicsComponent->setGameObject(Resilience);

	auto animationComponent = new ResilienceAnimationComponent();
	animationComponent->setGameObject(Resilience);

	auto behaviorComponent = new ResilienceBehaviorComponent();
	behaviorComponent->setGameObject(Resilience);

	Resilience->setPhysicsComponent(physicsComponent);
	Resilience->setAnimationComponent(animationComponent);
	Resilience->setBehaviorComponent(behaviorComponent);

	physicsComponent->setPosition(pos);

	Resilience->init();

	return Resilience;
}

GameObject * ObjectFactory::getResilience(xml_node node)
{
	GVector2 pos;
	pos.x = node.attribute("X").as_float() * SCALE_FACTOR;
	pos.y = node.attribute("Y").as_float() * SCALE_FACTOR;

	return getResilience(pos);
}

GameObject * ObjectFactory::getStar(GVector2 pos, GVector2 velocity, eDirection side, GameObject * trackerObject)
{
	auto Star = new GameObject(eObjectID::STAR);

	auto physicsComponent = new StarPhysicsComponent();
	physicsComponent->setGameObject(Star);

	auto animationComponent = new StarAnimationComponent();
	animationComponent->setGameObject(Star);

	auto behaviorComponent = new StarBehaviorComponent();
	behaviorComponent->setGameObject(Star);
	behaviorComponent->setGameController(GameController::getInstance());
	behaviorComponent->setTrackerObj(trackerObject);

	Star->setPhysicsComponent(physicsComponent);
	Star->setAnimationComponent(animationComponent);
	Star->setBehaviorComponent(behaviorComponent);

	Star->init();
	physicsComponent->setPosition(pos);

	auto move = (Movement*)physicsComponent->getComponent("Movement");
	move->setVelocity(velocity);

	behaviorComponent->setPullDirection(side);
	if (side == eDirection::RIGHT)
		behaviorComponent->faceLeft();
	else
		behaviorComponent->faceRight();

	return Star;
}

map<string, string> ObjectFactory::getObjectProperties(xml_node node)
{
	map<string, string> properties;

	// general
	properties["X"] = node.attribute("X").as_string();
	properties["Y"] = node.attribute("Y").as_string();
	properties["Width"] = node.attribute("Width").as_string();
	properties["Height"] = node.attribute("Height").as_string();

	// parameters
	xml_node params = node.child("Params");
	for (auto item : node.children())
	{
		auto key = item.attribute("Key").as_string();
		auto value = item.attribute("Value").as_string();
		properties[key] = value;
	}

	return properties;
}