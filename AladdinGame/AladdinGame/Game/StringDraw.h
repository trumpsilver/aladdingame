#ifndef __STRINGDRAW_H__
#define __STRINGDRAW_H__

#include "../Framework/define.h"
#include "../Framework/viewport.h"
#include "../Framework/GameObject.h"

#include<vector>
#include<string>
LINK_FRAMEWORK
class StringDraw
{
public:
	~StringDraw(void);

	static void release();
	static StringDraw* getInstance();
	void init();
	void draw(LPD3DXSPRITE spriteHandle, string text, GVector2 pos, float size = 1);
				
private:
	//singleton
	StringDraw(void);
	Sprite * _sprite;
	static StringDraw* _instance;
	map<char, RECT> _charRect;
	int _charwidth;

};

#endif // !__SCENEBACKGROUND_H__

