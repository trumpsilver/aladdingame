#include "AgrabahMarketIntroScene.h"
#include "../../Game/Player/Aladdin.h"
#include "TestScene.h"
AgrabahMarketIntroScene::AgrabahMarketIntroScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}

AgrabahMarketIntroScene::~AgrabahMarketIntroScene()
{
	delete _viewport;
	_viewport = nullptr;
}

bool AgrabahMarketIntroScene::init()
{
	GameObject* agrabahMarket = ObjectFactory::getCutscene(GVector2(320, 1), eCutscene::cAGRABAH_MARKET);
	agrabahMarket->getAnimationComponent()->setScale(SCALE_FACTOR);
	_list_object.push_back(agrabahMarket);

	_input = GameController::getInstance();

	SoundManager::getInstance()->PlayLoop(eSoundId::sNE_NAW);

	return true;
}

void AgrabahMarketIntroScene::update(float dt)
{

	//_animations[eCutscene::cLEVEL_COMPLETE]->update(dt);

	for (auto obj : _list_object)
	{
		obj->update(dt);
	}


	if (_input->isKeyPressed(DIK_RETURN)) //done animation
	{
		SoundManager::getInstance()->Stop(eSoundId::sNE_NAW);
		SceneManager::getInstance()->removeScene();
		SceneManager::getInstance()->addScene(new ArgabahMarketScene());
	}
}


void AgrabahMarketIntroScene::draw(LPD3DXSPRITE spriteHandle)
{
	//_sprite->render(spriteHandle);
	for (auto obj : _list_object)
	{
		obj->draw(spriteHandle, _viewport);
	}
}

void AgrabahMarketIntroScene::release()
{
}

GameObject * AgrabahMarketIntroScene::getObject(eObjectID id)
{
	return nullptr;
}

void AgrabahMarketIntroScene::updateInput(float dt)
{
}

void AgrabahMarketIntroScene::restart()
{
	SoundManager::getInstance()->Stop();
	SoundManager::getInstance()->PlayLoop(eSoundId::sNE_NAW);
}

vector<GameObject*> AgrabahMarketIntroScene::getActiveObject()
{
	return vector<GameObject*>();
}