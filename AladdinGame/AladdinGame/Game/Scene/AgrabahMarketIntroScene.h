﻿#ifndef __AGRABAH_MARKET_INTRO_SCENE_H__
#define __AGRABAH_MARKET_INTRO_SCENE_H__

#include<vector>
#include "Scene.h"

#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/sprite.h"
#include "../../Framework/Singleton/spriteresource.h"
#include "../../Framework/Singleton/SoundManager.h"
#include "../../Framework/animation.h"
#include "../../Framework/QuadTree.h"
#include "../Player/PlayerHub.h"
#include "../ObjectFactory.h"
#include "../../debug.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../Object/Cutscene.h"

using namespace std;
LINK_FRAMEWORK


class AgrabahMarketIntroScene : public Scene, public sigcxx::Signal<GameObject * >
{
public:
	AgrabahMarketIntroScene();
	~AgrabahMarketIntroScene();

	bool init() override;
	void update(float dt) override;
	void draw(LPD3DXSPRITE spriteHandle) override;
	void release() override;
	void restart() override;

	virtual vector<GameObject*> getActiveObject() override;
	//	// Trả về một đối tượng theo id.
	//	// id: kiểu enum eID, định danh một đối tượng.
	//	// return: đối tượng cần tìm.
	GameObject* getObject(eObjectID id);
	virtual void updateInput(float dt) override;

protected:
	Sprite * _sprite;
	map<int, Animation*> _animations;
	vector<GameObject*> _list_object;

	GameController * _input;
};

#endif




