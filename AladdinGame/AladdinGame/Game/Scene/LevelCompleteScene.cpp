﻿#include "LevelCompleteScene.h"
#include "../../Game/Player/Aladdin.h"

LevelCompleteScene::LevelCompleteScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}

LevelCompleteScene::~LevelCompleteScene()
{
	delete _viewport;
	_viewport = nullptr;
}

bool LevelCompleteScene::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CUTSCENES);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CUTSCENES, "level_complete"));
	_sprite->setZIndex(0.0f);
	_sprite->setPosition(0, WINDOW_HEIGHT);
	_sprite->setOrigin(GVector2(0.0f, 0.0f));
	_sprite->setScale(SCALE_FACTOR * 1.75);

	GameObject* abu = ObjectFactory::getAbu(GVector2(WINDOW_WIDTH, WINDOW_HEIGHT / 6), eStatus::WIN);
	_list_object.push_back(abu);

	GameObject* aladdin = ObjectFactory::getAladdin();
	aladdin->getAnimationComponent()->setAnimation(eStatus::WIN);
	aladdin->getPhysicsComponent()->setPosition(GVector2(WINDOW_WIDTH + 70, WINDOW_HEIGHT / 6));
	aladdin->getBehaviorComponent()->release();
	delete aladdin->getBehaviorComponent();
	aladdin->setBehaviorComponent(nullptr);
	auto g = (Gravity*)aladdin->getPhysicsComponent()->getComponent("Gravity");
	g->setStatus(eGravityStatus::LANDED);
	_list_object.push_back(aladdin);

	SoundManager::getInstance()->Play(eSoundId::sLEVEL_COMPLETE);

	return true;
}

void LevelCompleteScene::update(float dt)
{
	
	/*_animations[eCutscene::cLEVEL_COMPLETE]->update(dt);*/

	for (auto obj : _list_object)
	{
		posX = obj->getPhysicsComponent()->getPositionX();
		posX -= 1.4 * SCALE_FACTOR;
		obj->getPhysicsComponent()->setPositionX(posX);
		obj->update(dt);
	}

	if (posX < 0) //done animation
	{
		SceneManager::getInstance()->removeScene();
	}
}


void LevelCompleteScene::draw(LPD3DXSPRITE spriteHandle)
{
	_sprite->render(spriteHandle);
	for (auto obj : _list_object)
	{
		obj->draw(spriteHandle, _viewport);
	}
}

void LevelCompleteScene::release()
{
}

GameObject * LevelCompleteScene::getObject(eObjectID id)
{
	return nullptr;
}

void LevelCompleteScene::updateInput(float dt)
{
}

void LevelCompleteScene::restart()
{
	SoundManager::getInstance()->Stop();
	SoundManager::getInstance()->PlayLoop(eSoundId::sNE_NAW);
}

vector<GameObject*> LevelCompleteScene::getActiveObject()
{
	return vector<GameObject*>();
}