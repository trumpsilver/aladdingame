﻿#include "DeathScene.h"
#include "../../Game/Player/Aladdin.h"

DeathScene::DeathScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}

DeathScene::~DeathScene()
{
	delete _viewport;
	_viewport = nullptr;
}

bool DeathScene::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::ALADDIN);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ALADDIN, "die_01"));
	_sprite->setZIndex(0.0f);
	_sprite->setPosition(GVector2(WINDOW_WIDTH / 2.5, WINDOW_HEIGHT / 1.7));
	
	_sprite->setOrigin(GVector2(0.0f, 0.0f));
	_sprite->setScale(SCALE_FACTOR);
	
	_animations[eStatus::DIE] = new Animation(_sprite, 0.15f);
	_animations[eStatus::DIE]->addFrameRect(eObjectID::ALADDIN, "die_01", "die_02", "die_03", "die_04", "die_05", "die_06", "die_07", "die_08", "die_09", "die_10", "die_11", "die_12", "die_13", 
		"care_01", "care_02", "care_03", "care_04", "care_05", "care_06", "care_07", "care_08", "care_09", "care_10", "care_11", "care_12", "care_13", "care_14", "care_15", NULL);

	GameObject* abu = ObjectFactory::getAbu(GVector2(WINDOW_WIDTH / 2.5 + 210, WINDOW_HEIGHT / 1.7 - 110), eStatus::DIE);
	_list_object.push_back(abu);
	
	/*_animations[eStatus::DIE] = new Animation(_sprite, 0.15f);
	_animations[eStatus::DIE]->addFrameRect(eObjectID::ALADDIN, "die_01", "die_02", "die_03", "die_04", "die_05", "die_06", "die_07", "die_08", "die_09", "die_10", "die_11", "die_12", "die_13", NULL);

	_animations[eStatus::CARE] = new Animation(_sprite, 0.15f);
	_animations[eStatus::CARE]->addFrameRect(eObjectID::ALADDIN, "care_01", "care_02", "care_03", "care_04", "care_05", "care_06", "care_07", "care_08", "care_09", "care_10", "care_11", "care_12", "care_13", "care_14", "care_15", NULL);*/
	



	/*
	- Bắt đầu scene là play eSoundId::ALADDIN_PUSH (1 lần) và eSoundId::ABU_WAVING (lặp lại cho đến khi kết thúc, giữa 2 lần lặp cách nhau khoảng 0s40)
	- Khi _animation[eStatus::CARE] bắt đầu thì play eSoundId::START_GUN (1 lần), sau khi eSoundId::START_GUN stop thì play eSoundId::BOXING_BELL (1 lần)
	*/

	SoundManager::getInstance()->Play(eSoundId::sALADDIN_DIE);
	
	return true;
}

void DeathScene::update(float dt)
{

	_animations[eStatus::DIE]->update(dt);
	for (auto obj : _list_object)
	{
		obj->update(dt);
	}
	if (_animations[eStatus::DIE]->getCount() >= 1)//done animation
	{
		SceneManager::getInstance()->removeScene();
	}
}

void DeathScene::draw(LPD3DXSPRITE spriteHandle)
{
	for (auto obj : _list_object)
	{
		obj->draw(spriteHandle, _viewport);
		//obj->getAnimationComponent()->getCurrentAnimation()->draw(spriteHandle);
	}
	
	if (_animations[eStatus::DIE] != nullptr)
	{
		_animations[eStatus::DIE]->draw(spriteHandle);
	}
}

void DeathScene::release()
{
}

GameObject * DeathScene::getObject(eObjectID id)
{
	return nullptr;
}

void DeathScene::updateInput(float dt)
{
}

void DeathScene::restart()
{
}

vector<GameObject*> DeathScene::getActiveObject()
{
	return vector<GameObject*>();
}