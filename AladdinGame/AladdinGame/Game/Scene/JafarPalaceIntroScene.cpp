#include "JafarPalaceIntroScene.h"
#include "../../Game/Player/Aladdin.h"

JafarPalaceIntroScene::JafarPalaceIntroScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}


JafarPalaceIntroScene::~JafarPalaceIntroScene()
{
	delete _viewport;
	_viewport = nullptr;
}

bool JafarPalaceIntroScene::init()
{
	GameObject* jafarPalace = ObjectFactory::getCutscene(GVector2(320, 1), eCutscene::cJAFAR_PALACE);
	jafarPalace->getAnimationComponent()->setScale(SCALE_FACTOR);
	_list_object.push_back(jafarPalace);

	_input = GameController::getInstance();



	return true;
}

void JafarPalaceIntroScene::update(float dt)
{

	//_animations[eCutscene::cLEVEL_COMPLETE]->update(dt);

	for (auto obj : _list_object)
	{
		obj->update(dt);
	}


	if (_input->isKeyPressed(DIK_RETURN)) //done animation
	{
		SoundManager::getInstance()->Stop(eSoundId::sNE_NAW);
		SceneManager::getInstance()->removeScene();
	}
}


void JafarPalaceIntroScene::draw(LPD3DXSPRITE spriteHandle)
{
	//_sprite->render(spriteHandle);
	for (auto obj : _list_object)
	{
		obj->draw(spriteHandle, _viewport);
	}
}

void JafarPalaceIntroScene::release()
{
}

GameObject * JafarPalaceIntroScene::getObject(eObjectID id)
{
	return nullptr;
}

void JafarPalaceIntroScene::updateInput(float dt)
{
}

void JafarPalaceIntroScene::restart()
{
	SoundManager::getInstance()->Stop();
	SoundManager::getInstance()->PlayLoop(eSoundId::sNE_NAW);
}

vector<GameObject*> JafarPalaceIntroScene::getActiveObject()
{
	return vector<GameObject*>();
}