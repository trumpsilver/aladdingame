#include "MenuScene.h"
#include "../../Game/Player/Aladdin.h"
#include "AgrabahMarketIntroScene.h"
#include "TestBossScene.h"

MenuScene::MenuScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}

MenuScene::~MenuScene()
{
	delete _viewport;
	_viewport = nullptr;
}

bool MenuScene::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CUTSCENES);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CUTSCENES, "scene_menu"));
	_sprite->setZIndex(0.0f);
	_sprite->setPosition(0, WINDOW_HEIGHT);
	_sprite->setOrigin(GVector2(0.0f, 0.0f));
	_sprite->setScale(SCALE_FACTOR * 1.1);

	GameObject* sword = ObjectFactory::getCutscene(GVector2(WINDOW_WIDTH / 4 + 10, WINDOW_HEIGHT / 3 + 22), eCutscene::cSWORD);
	sword->getAnimationComponent()->setScale(SCALE_FACTOR * 0.5);
	_list_object.push_back(sword);

	_input = GameController::getInstance();
	_choice = 0;

	SoundManager::getInstance()->PlayLoop(eSoundId::sENDING);

	return true;
}

void MenuScene::update(float dt)
{

	//_animations[eCutscene::cLEVEL_COMPLETE]->update(dt);

	for (auto obj : _list_object)
	{
		pos = obj->getPhysicsComponent()->getPosition();
		if (pos.x > WINDOW_WIDTH / 4 + 35 || pos.x < WINDOW_WIDTH / 4 + 10)
		{
			moveX = -moveX;
		}
		pos.x += moveX;
		
		if (_input->isKeyPressed(DIK_DOWN) || _input->isKeyPressed(BT_UP))
		{
			SoundManager::getInstance()->Play(eSoundId::sMENU_CHANGE);
			pos.x = WINDOW_WIDTH / 4 + 10;
			pos.y -= moveY;
			moveY = -moveY;
			_choice++;
			if (_choice > 1)
				_choice = 0;
		}
		
		obj->getPhysicsComponent()->setPosition(pos);
		obj->update(dt);
	}


	if (_input->isKeyPressed(DIK_RETURN) && pos.y == WINDOW_HEIGHT / 3 + 22 && _choice == 0) //done animation
	{
		SoundManager::getInstance()->Stop(eSoundId::sENDING);
		SceneManager::getInstance()->removeScene();
		SceneManager::getInstance()->addScene(new AgrabahMarketIntroScene());
		return;
	}

	if (_input->isKeyPressed(DIK_W)) //done animation
	{
		SoundManager::getInstance()->Stop(eSoundId::sENDING);
		SceneManager::getInstance()->removeScene();
		SceneManager::getInstance()->addScene(new TestBossScene());
		return;
	}
}


void MenuScene::draw(LPD3DXSPRITE spriteHandle)
{
	_sprite->render(spriteHandle);
	for (auto obj : _list_object)
	{
		obj->draw(spriteHandle, _viewport);
	}
}

void MenuScene::release()
{
}

GameObject * MenuScene::getObject(eObjectID id)
{
	return nullptr;
}

void MenuScene::updateInput(float dt)
{
}

void MenuScene::restart()
{
}

vector<GameObject*> MenuScene::getActiveObject()
{
	return vector<GameObject*>();
}