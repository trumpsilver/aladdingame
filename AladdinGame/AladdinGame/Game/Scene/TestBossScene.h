﻿#ifndef __TEST_BOSS_SCENE_H__
#define __TEST_BOSS_SCENE_H__


#include "../../Framework/define.h"
#include "../../Framework/sprite.h"
#include "../../Framework/Singleton/spriteresource.h"
#include "../../Framework/Singleton/SoundManager.h"
#include "../../Framework/animation.h"
#include "../../Framework/QuadTree.h"
#include "../Player/PlayerHub.h"
#include "../ObjectFactory.h"
#include "../../debug.h"
#include "../Player/Aladdin.h"
#include "LevelCompleteScene.h"
#include "../../Framework/Component/EnemyComponent.h"


using namespace std;

//forward declaretor

class StarCreator;
LINK_FRAMEWORK


class TestBossScene : public Scene, public sigcxx::Trackable
{
public:
	TestBossScene();
	~TestBossScene();

	bool init() override;
	void update(float dt) override;
	void draw(LPD3DXSPRITE spriteHandle) override;
	void release() override;


	void setViewport(Viewport* viewport);
	void moveViewport(float offset, bool moveup, sigcxx::SLOT slot = nullptr);


	//Trả về đối tượng đang hoạt động
	virtual vector<GameObject*> getActiveObject() override;
	// Trả về một đối tượng theo id.
	// id: kiểu enum eID, định danh một đối tượng.
	// return: đối tượng cần tìm.
	GameObject* getObject(eObjectID id);

	// Lấy đối tượng bill.
	GameObject* getAladdin();

	void restart() override;
private:
	void destroyobject();				// kiển tra nếu object hết hạn sử dụng thì phá huỷ đối tượng

										// Danh sách đối tượng dùng để tạo quad tree.
	map <string, GameObject*> _mapobject;

	// Danh sách các đối tượng hoạt động rộng không thể đưa vào quadtree.
	// (Ví dụ main character, táo, dao găm)
	vector<GameObject*> _listobject;
	vector<GameObject*> _throwObject;

	// Danh sách object bị destroy, chỉ có táo trong đây dùng để hồi phục lại
	vector<GameObject*> _destroyedObjectLeft;
	vector<GameObject*> _destroyedObjectRight;


	// Trỏ đến bill, một số đối tượng cần truyền bill vào để xử lý, lấy ở đây.
	GameObject* _Aladdin;
	//Map background
	Sprite* _mapBack;
	Sprite* _mapFront;

	//boss
	GameObject* _jafar;
	//hub
	PlayerHub * _playerHub;


	void updateViewport(GameObject* objTracker, float deltatime);

	// Check if need to update viewport
	bool _updateViewport;

	//Size of screen
	RECT _rootRect;

	void addToScene(GameObject * obj, sigcxx::SLOT slot = nullptr);
	virtual void updateInput(float dt) override;

	// Inherited via Scene
	void checkWinGame();

	bool _isRampageMode;
	void enableRampageMode(sigcxx::SLOT slot);
	void checkAladdinApple();
};

#endif // !__TEST_BOSS_SCENE_H__
