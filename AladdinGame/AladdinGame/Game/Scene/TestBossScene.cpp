﻿#include "TestBossScene.h"
#include "MenuScene.h"
#include "../Object/StarCreator.h"

TestBossScene::TestBossScene()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
}

TestBossScene::~TestBossScene()
{
	delete _viewport;
	_viewport = nullptr;
}

void TestBossScene::setViewport(Viewport * viewport)
{
	if (_viewport != viewport)
		_viewport = viewport;
}

void TestBossScene::moveViewport(float offset, bool moveup, sigcxx::SLOT slot)
{
	GVector2 position = _viewport->getPositionWorld();
	if (moveup == true)
	{
		position.y += offset;
		_updateViewport = false;
	}
	else
	{
		position.y -= offset;
		_updateViewport = true;
	}
	_viewport->setPositionWorld(position);
}


bool TestBossScene::init()
{

	_Aladdin = ObjectFactory::getAladdin();
	_Aladdin->getPhysicsComponent()->setPosition(400, 300);
	auto aladdinBehavior = (AladdinBehaviorComponent*)_Aladdin->getBehaviorComponent();
	aladdinBehavior->setRespawnPosition(GVector2(400, 300));
	aladdinBehavior->moveViewport.Connect(this, &TestBossScene::moveViewport);
	aladdinBehavior->addToScene.Connect(this, &TestBossScene::addToScene);
	_listobject.push_back(_Aladdin);

	JafarBehaviorComponent::rampageMode.Connect(this, &TestBossScene::enableRampageMode);
	_jafar = ObjectFactory::getJafar(GVector2(980, 328));
	_jafar->getBehaviorComponent()->setRespawnPosition(GVector2(980, 328));
	_listobject.push_back(_jafar);



	StarCreator::addToScene.Connect(this, &TestBossScene::addToScene);

	_playerHub = new PlayerHub();
	_playerHub->init(_Aladdin);

	BehaviorComponent::addToScene.Connect(this, &TestBossScene::addToScene);


	_mapBack = SpriteResource::getInstance()->getSprite(eObjectID::MAPBOSSBACKGROUND);
	_mapBack->setPositionX(0);
	_mapBack->setPositionY(WINDOW_HEIGHT);
	_mapBack->setOrigin(GVector2(0.f, 0.f));
	_mapBack->setScale(SCALE_FACTOR);
	_mapBack->setZIndex(0.f);

	_mapFront = SpriteResource::getInstance()->getSprite(eObjectID::MAPBOSS);
	_mapFront->setPositionX(0);
	_mapFront->setPositionY(0);
	_mapFront->setOrigin(GVector2(0.f, 0.f));
	_mapFront->setScale(SCALE_FACTOR);
	_mapFront->setZIndex(0.f);

	_rootRect.left = 0;
	_rootRect.bottom = 0;
	_rootRect.right = _mapFront->getFrameWidth() * SCALE_FACTOR;
	_rootRect.top = _mapFront->getFrameHeight() * SCALE_FACTOR;


	map<string, GameObject*>* maptemp = ObjectFactory::getMapObjectFromFile("Resources//Maps//mapboss.xml");
	_mapobject.insert(maptemp->begin(), maptemp->end());
	for (auto obj : _mapobject)
	{
		_listobject.push_back(obj.second);
	}


	_updateViewport = true;
	_isRampageMode = false;
	return true;
}


void TestBossScene::update(float dt)
{
	updateViewport(_Aladdin, dt);
	checkAladdinApple();
	//update size of screen
	GVector2 viewport_position = _viewport->getPositionWorld();
	RECT viewport_in_transform = _viewport->getBounding();


	this->destroyobject();

	for (GameObject* obj : _listobject)
	{
		obj->update(dt);
	}
	for (GameObject* obj : _throwObject)
	{
		obj->update(dt);
	}
	_playerHub->update(dt);
	checkWinGame();
}

void TestBossScene::draw(LPD3DXSPRITE spriteHandle)
{
	_mapBack->render(spriteHandle);
	_mapFront->render(spriteHandle, _viewport);
	for (GameObject* object : _listobject)
	{
		object->draw(spriteHandle, _viewport);
	}
	for (GameObject* object : _throwObject)
	{
		object->draw(spriteHandle, _viewport);
	}
	_playerHub->draw(spriteHandle);
}

void TestBossScene::release()
{
}


GameObject * TestBossScene::getObject(eObjectID id)
{
	if (id == eObjectID::ALADDIN)
		return getAladdin();
	eObjectID objectID;
	for (GameObject* object : _listobject)
	{
		objectID = object->getID();
		if (objectID == id)
			return object;
	}
	return nullptr;
}

GameObject * TestBossScene::getAladdin()
{
	return _Aladdin;
}

void TestBossScene::destroyobject()
{
	//TODO : Destroy các phần tử ngoài màn hình
	for (auto object : _throwObject)
	{
		if (object->getBehaviorComponent() != nullptr && object->getBehaviorComponent()->getStatus() == eStatus::DESTROY)	// kiểm tra nếu là destroy thì loại khỏi list
		{
			object->release();
			// http://www.cplusplus.com/reference/algorithm/remove/
			auto rs1 = std::remove(_throwObject.begin(), _throwObject.end(), object);
			_throwObject.pop_back();
			delete object;
			break;
		}

		//Check object out of screen, remove them for performance
		if (!isRectangleIntersected(object->getPhysicsComponent()->getBounding(), _rootRect))
		{
			object->release();
			// http://www.cplusplus.com/reference/algorithm/remove/
			auto rs1 = std::remove(_throwObject.begin(), _throwObject.end(), object);
			_throwObject.pop_back();
			delete object;
			break;
		}
	}
	//for (auto object : _listobject)
	//{
	//	if (object->getBehaviorComponent() != nullptr && object->getBehaviorComponent()->getStatus() == eStatus::DESTROY)	// kiểm tra nếu là destroy thì loại khỏi list
	//	{
	//		//object->release();
	//		//// http://www.cplusplus.com/reference/algorithm/remove/
	//		//auto rs1 = std::remove(_listobject.begin(), _listobject.end(), object);
	//		//_listobject.pop_back();
	//		//delete object;
	//		//break;
	//		_destroyedObject.push_back(object);
	//		_listobject.
	//		continue;
	//	}
	//}

	for (auto it = _listobject.begin(); it != _listobject.end(); it++)
	{
		if ((*it)->getBehaviorComponent() != nullptr && (*it)->getBehaviorComponent()->getStatus() == eStatus::DESTROY)	// kiểm tra nếu là destroy thì loại khỏi list
		{
			//object->release();
			//// http://www.cplusplus.com/reference/algorithm/remove/
			//auto rs1 = std::remove(_listobject.begin(), _listobject.end(), object);
			//_listobject.pop_back();
			//delete object;
			//break;
			if ((*it)->getPhysicsComponent()->getPositionX() <= ((_rootRect.right - _rootRect.left) / 2) / SCALE_FACTOR)
			{
				_destroyedObjectLeft.push_back((*it));
				_listobject.erase(it);
			}
			else
			{
				_destroyedObjectRight.push_back((*it));
				_listobject.erase(it);
			}

			break;
		}
	}
}

void TestBossScene::updateViewport(GameObject * objTracker, float deltatime)
{
	GameController * input = GameController::getInstance();
	if (_updateViewport == false)
	{
		return;
	}
	float lerp = 10.0f;
	//Lấy hướng nhìn của aladdin
	int side = _Aladdin->getBehaviorComponent()->getFacingDirection();
	// Vị trí hiện tại của viewport. 
	GVector2 current_position = _viewport->getPositionWorld();
	GVector2 worldsize;
	worldsize.x = _mapFront->getFrameWidth();
	worldsize.y = _mapFront->getFrameHeight();

	// Bám theo object.
	float trackerX;
	if (side == eDirection::RIGHT)
		trackerX = max(objTracker->getPhysicsComponent()->getPositionX() - 150, 0);
	else
		trackerX = max(objTracker->getPhysicsComponent()->getPositionX() - (WINDOW_WIDTH - 150), 0);

	float trackerY = max(objTracker->getPhysicsComponent()->getPositionY() + 300, WINDOW_HEIGHT);

	//Không cho vượt quá bên trái
	if (trackerX + _viewport->getWidth() > worldsize.x)
	{
		trackerX = worldsize.x - _viewport->getWidth();
	}

	//Không vượt quá trên
	if (trackerY > worldsize.y)
	{
		trackerY = worldsize.y;
	}


	current_position.x += (trackerX - current_position.x) * lerp * deltatime / 1000;
	current_position.y += (trackerY - current_position.y) * lerp * deltatime / 1000;


	_viewport->setPositionWorld(current_position);
}


void TestBossScene::addToScene(GameObject * obj, sigcxx::SLOT slot)
{
	_throwObject.push_back(obj);
}

void TestBossScene::updateInput(float dt)
{

}

void TestBossScene::restart()
{
	//reset all dead enemy
	SoundManager::getInstance()->Stop();
	SoundManager::getInstance()->PlayLoop(eSoundId::BACKGROUND_STAGE1);
	AladdinBehaviorComponent * alaBeha = (AladdinBehaviorComponent *)_Aladdin->getBehaviorComponent();
	if (alaBeha->getLife() >= 0)
	{
		alaBeha->respawn();
	}

	for (auto obj : _destroyedObjectLeft)
	{
		obj->getBehaviorComponent()->respawn();
		_listobject.push_back(obj);
	}

	_destroyedObjectLeft.clear();

	for (auto obj : _destroyedObjectRight)
	{
		obj->getBehaviorComponent()->respawn();
		_listobject.push_back(obj);
	}

	_destroyedObjectRight.clear();

	_jafar->getBehaviorComponent()->respawn();

	_isRampageMode = false;

	for (auto obj : _listobject)
	{
		if (obj->getID() == eObjectID::LAND)
		{
			if (((Land*)obj)->getLandType() == eLandType::lFLAME)
			{
				((Land*)obj)->setLandType(eLandType::lNORMAL);
			}
		}
	}

	for (auto object : _throwObject)
	{
		object->release();
	}
	_throwObject.clear();

	
}

void TestBossScene::checkWinGame()
{
	if (((EnemyBehaviorComponent*)_jafar->getBehaviorComponent())->getHitpoint() <= 0)
	{
		SoundManager::getInstance()->Stop();
		SceneManager::getInstance()->removeScene();
		SceneManager::getInstance()->addScene(new MenuScene());
		SceneManager::getInstance()->addScene(new LevelCompleteScene());
	}
}

void TestBossScene::enableRampageMode(sigcxx::SLOT slot)
{
	if (_isRampageMode)
		return;

	//biến mặt đất bình thường thành đất lửa
	for (auto obj : _listobject)
	{
		if (obj->getID() == eObjectID::LAND)
		{
			if (((Land*)obj)->getLandType() == eLandType::lNORMAL)
			{
				((Land*)obj)->setLandType(eLandType::lFLAME);
			}
		}
	}

	_isRampageMode = true;

}

void TestBossScene::checkAladdinApple()
{
	//Nếu aladdin hết táo thì hồi lại
	if (((AladdinBehaviorComponent*)_Aladdin->getBehaviorComponent())->getNumApple() <= 0)
	{
		if (_destroyedObjectLeft.size() == 4 && _destroyedObjectRight.size() == 4)
		{
			if (_jafar->getBehaviorComponent()->getFacingDirection() == eDirection::LEFT)
			{
				for (auto obj : _destroyedObjectRight)
				{
					obj->getBehaviorComponent()->respawn();
					_listobject.push_back(obj);
				}
				_destroyedObjectRight.clear();
				
			}
			else
			{
				for (auto obj : _destroyedObjectLeft)
				{
					obj->getBehaviorComponent()->respawn();
					_listobject.push_back(obj);
				}
				_destroyedObjectLeft.clear();
			}

		}
	}
}



vector<GameObject*> TestBossScene::getActiveObject()
{
	return _listobject;
}
