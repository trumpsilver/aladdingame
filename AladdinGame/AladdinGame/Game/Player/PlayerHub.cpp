#include "PlayerHub.h"
#include "../StringDraw.h"
#include "../Scene/SceneManager.h"
#include "../../Framework/Singleton/ScoreManager.h"
#include "../../Game/Player/Aladdin.h"
PlayerHub::PlayerHub()
{
}

PlayerHub::~PlayerHub()
{
}

bool PlayerHub::init(GameObject * aladdin)
{
	_aladdin = aladdin;
	
	_hubSprite = SpriteResource::getInstance()->getSprite(eObjectID::ITEM);
	_hubSprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ITEM, "apple"));
	_hubSprite->setZIndex(0.0f);
	_hubSprite->setOrigin(GVector2(0.0f, 0.0f));
	_hubSprite->setScale(SCALE_FACTOR);

	_lifeSprite = SpriteResource::getInstance()->getSprite(eObjectID::MISCITEMS);
	_lifeSprite->setZIndex(0.0f);


	_lifeSprite->setOrigin(GVector2(0.0f, 0.0f));
	_lifeSprite->setScale(SCALE_FACTOR);

	_animations[eItem::iAPPLE] = new Animation(_hubSprite, 1000.f);
	_animations[eItem::iAPPLE]->addFrameRect(eObjectID::ITEM, "apple", NULL);
	_animations[eItem::iAPPLE]->canAnimate(false);

	_animations[eItem::iEXTRA_HEART] = new Animation(_hubSprite, 1000.f);
	_animations[eItem::iEXTRA_HEART]->addFrameRect(eObjectID::ITEM, "aladdin_face", NULL);
	_animations[eItem::iEXTRA_HEART]->canAnimate(false);

	_animations[eItem::iMONEY] = new Animation(_hubSprite, 1000.f);
	_animations[eItem::iMONEY]->addFrameRect(eObjectID::ITEM, "money_1", NULL);
	_animations[eItem::iMONEY]->canAnimate(false);

	_animations[9] = new Animation(_lifeSprite, 0.07f);
	_animations[9]->addFrameRect(eObjectID::MISCITEMS, "health_level1_1", "health_level1_2", "health_level1_3", "health_level1_4", NULL);

	_animations[8] = new Animation(_lifeSprite, 0.07f);
	_animations[8]->addFrameRect(eObjectID::MISCITEMS, "health_level2_1", "health_level2_2", "health_level2_3", "health_level2_4", NULL);

	_animations[7] = new Animation(_lifeSprite, 0.07f);
	_animations[7]->addFrameRect(eObjectID::MISCITEMS, "health_level3_1", "health_level3_2", "health_level3_3", "health_level3_4", NULL);

	_animations[6] = new Animation(_lifeSprite, 0.07f);
	_animations[6]->addFrameRect(eObjectID::MISCITEMS, "health_level4_1", "health_level4_2", "health_level4_3", "health_level4_4", NULL);

	_animations[5] = new Animation(_lifeSprite, 0.07f);
	_animations[5]->addFrameRect(eObjectID::MISCITEMS, "health_level5_1", "health_level5_2", "health_level5_3", "health_level5_4", NULL);

	_animations[4] = new Animation(_lifeSprite, 0.07f);
	_animations[4]->addFrameRect(eObjectID::MISCITEMS, "health_level6_1", "health_level6_2", "health_level6_3", "health_level6_4", NULL);

	_animations[3] = new Animation(_lifeSprite, 0.07f);
	_animations[3]->addFrameRect(eObjectID::MISCITEMS, "health_level7_1", "health_level7_2", "health_level7_3", "health_level7_4", NULL);

	_animations[2] = new Animation(_lifeSprite, 0.07f);
	_animations[2]->addFrameRect(eObjectID::MISCITEMS, "health_level8_1", "health_level8_2", "health_level8_3", "health_level8_4", NULL);

	_animations[1] = new Animation(_lifeSprite, 0.07f);
	_animations[1]->addFrameRect(eObjectID::MISCITEMS, "health_level9", NULL);
	_animations[1]->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	_animations[1]->enableFlashes(true);

	_animations[0] = _animations[1];

	return true;
}

void PlayerHub::update(float dt)
{
	auto aladdinBehavior = (AladdinBehaviorComponent *)_aladdin->getBehaviorComponent();

	int max_hp = START_HIT_POINT;
	_hpPercent = floor(10.0f * (aladdinBehavior->getHitpoint() - 1) / max_hp);
	_life = aladdinBehavior->getLife();
	_apple = aladdinBehavior->getNumApple();
	_money = aladdinBehavior->getMoney();


	if(_animations[_hpPercent] != nullptr)
		_animations[_hpPercent]->update(dt);

	if (_hpPercent > 1)
		_hubSprite->setOpacity(1.0f);

}

void PlayerHub::draw(LPD3DXSPRITE spriteHandle)
{
	//draw apple 
	_hubSprite->setPosition(GVector2(WINDOW_WIDTH - 100, WINDOW_HEIGHT - 30));
	_hubSprite->setScale(SCALE_FACTOR * 1.5);
	_animations[eItem::iAPPLE]->draw(spriteHandle);
	//_hubSprite->setScale(SCALE_FACTOR);
	StringDraw::getInstance()->draw(spriteHandle, std::to_string(_apple), GVector2(WINDOW_WIDTH - 50, WINDOW_HEIGHT - 40));

	//draw money is not zerro
	if (_money > 0)
	{
		_hubSprite->setPosition(GVector2(WINDOW_WIDTH - 200, WINDOW_HEIGHT - 30));
		_hubSprite->setScale(SCALE_FACTOR);
		_animations[eItem::iMONEY]->draw(spriteHandle);
		//_hubSprite->setScale(SCALE_FACTOR);
		StringDraw::getInstance()->draw(spriteHandle, std::to_string(_money), GVector2(WINDOW_WIDTH - 150, WINDOW_HEIGHT - 40));
	}

	//draw life
	_hubSprite->setPosition(GVector2(30, WINDOW_HEIGHT - 30));
	_hubSprite->setScale(SCALE_FACTOR);
	_animations[eItem::iEXTRA_HEART]->draw(spriteHandle);
	StringDraw::getInstance()->draw(spriteHandle, std::to_string(_life), GVector2(80, WINDOW_HEIGHT - 40));


	//draw score
	StringDraw::getInstance()->draw(spriteHandle, std::to_string(ScoreManager::getInstance()->getScore()), GVector2(WINDOW_WIDTH * 2 / 3, 40), 2.0f);

	//draw hitpoint
	if (_animations[_hpPercent] != nullptr)
	{
		_lifeSprite->setPosition(GVector2(50, 100));
		_animations[_hpPercent]->draw(spriteHandle);
	}
	

}

void PlayerHub::release()
{
}
