#ifndef __PLAYERHUB_H__
#define __PLAYERHUB_H__

#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include<cmath>
#include<vector>
LINK_FRAMEWORK
class PlayerHub
{
public:
	PlayerHub();
	virtual ~PlayerHub();

	bool virtual init(GameObject * aladdin);
	void virtual update(float dt);
	void virtual draw(LPD3DXSPRITE spriteHandle);
	void virtual release();

protected:
	Sprite * _hubSprite;
	Sprite * _lifeSprite;
	GVector2 _lifeHubPos;
	GVector2 _appleHubPos;
	int _index;
	int _hpPercent;
	int _life;
	int _apple;
	int _money;
	GameObject * _aladdin;
	map<int, Animation*> _animations;
};

#endif // !__PLAYERHUB_H__

