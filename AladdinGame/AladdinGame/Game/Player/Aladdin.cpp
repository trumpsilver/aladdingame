﻿#include "Aladdin.h"
#include "../../Framework/Singleton/gametime.h"
#include <ctime>;
#include "../../Framework/rectutils.h"

//TODO: Add animation queue with count number : replace with temp animation
//		Fix falling animation :not done yet
//		Update animation start animation and start loop is diffirent : ....
void AladdinPhysicsComponent::init()
{
	_movingSpeed = ALADDIN_MOVE_SPEED;
	auto movement = new Movement(GVector2(0, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -GRAVITY), movement);
	_zIndex = 0;
}

void AladdinAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::ALADDIN);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ALADDIN, "normal_01"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::WIN] = new Animation(_sprite, 0.15f);
	_animations[eStatus::WIN]->addFrameRect(eObjectID::ALADDIN, "win_01", "win_02", "win_03", "win_04", "win_05", "win_06", "win_07", "win_08", NULL);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ALADDIN, "normal_01", NULL);

	_animations[eStatus::NORMAL | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "slash_stand_01", "slash_stand_02", "slash_stand_03", "slash_stand_04", "slash_stand_05", NULL);

	_animations[eStatus::NORMAL | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "stand_throw_01", "stand_throw_02", "stand_throw_03", "stand_throw_04", "stand_throw_05", "stand_throw_06", NULL);

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::ALADDIN, "run_01", "run_02", "run_03", "run_04", "run_05", "run_06", "run_07", "run_08", "run_09", "run_10", "run_11", "run_12", "run_13", NULL);

	_animations[eStatus::RUNNING | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::RUNNING | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "run_slash_1", "run_slash_2", "run_slash_3", "run_slash_4", "run_slash_5", "run_slash_6", NULL);

	_animations[eStatus::RUNNING | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::RUNNING | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "run_throw_1", "run_throw_2", "run_throw_3", "run_throw_4", "run_throw_5", "run_throw_6", NULL);

	_animations[eStatus::JUMPING] = new Animation(_sprite, 0.168f);
	_animations[eStatus::JUMPING]->addFrameRect(eObjectID::ALADDIN, "jump_stand_01", "jump_stand_02", "jump_stand_03", "jump_stand_04", "jump_stand_05", "jump_stand_06", "jump_stand_07", "jump_stand_08", "jump_stand_09", "jump_stand_10", NULL);

	_animations[eStatus::FALLING] = new Animation(_sprite, 0.1f);
	_animations[eStatus::FALLING]->addFrameRect(eObjectID::ALADDIN, "jump_stand_06", "jump_stand_07", "jump_stand_08", "jump_stand_09", "jump_stand_10", NULL);
	_animations[eStatus::FALLING]->animateFromTo(3, 4, true);

	_animations[eStatus::JUMPING | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::JUMPING | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "slash_jump_01", "slash_jump_02", "slash_jump_03", "slash_jump_04", "slash_jump_05", "slash_jump_06", NULL);

	_animations[eStatus::JUMPING | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::JUMPING | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "throw_jump_01", "throw_jump_02", "throw_jump_03", "throw_jump_04", "throw_jump_05", NULL);

	_animations[eStatus::FALLING | eStatus::SLASH] = _animations[eStatus::JUMPING | eStatus::SLASH];
	_animations[eStatus::FALLING | eStatus::THROW] = _animations[eStatus::JUMPING | eStatus::THROW];


	_animations[eStatus::MOVING_JUMPING] = new Animation(_sprite, 0.168f);
	_animations[eStatus::MOVING_JUMPING]->addFrameRect(eObjectID::ALADDIN, "jump_moving_01", "jump_moving_02", "jump_moving_03", "jump_moving_04", "jump_moving_05", "jump_moving_06", "jump_moving_07", "jump_moving_08", "jump_moving_09", NULL);

	_animations[eStatus::MOVING_JUMPING | eStatus::SLASH] = _animations[eStatus::JUMPING | eStatus::SLASH];


	_animations[eStatus::LANDING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LANDING]->addFrameRect(eObjectID::ALADDIN, "landing_01", "landing_02", "landing_03", "landing_04", "landing_05", "landing_06", "landing_07", "landing_08", "landing_09", "landing_10", "landing_11", "landing_12", NULL);

	_animations[eStatus::LAYING_DOWN] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LAYING_DOWN]->addFrameRect(eObjectID::ALADDIN, "laydown_01", "laydown_02", "laydown_03", "laydown_04", NULL);
	_animations[eStatus::LAYING_DOWN]->animateFromTo(0, 2, false);

	_animations[eStatus::LAYING_DOWN | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LAYING_DOWN | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "slash_laydown_01", "slash_laydown_02", "slash_laydown_03", "slash_laydown_04", "slash_laydown_05", "slash_laydown_06", "slash_laydown_07", NULL);

	_animations[eStatus::LAYING_DOWN | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LAYING_DOWN | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "throw_laydown_01", "throw_laydown_02", "throw_laydown_03", "throw_laydown_04", "throw_laydown_05", NULL);

	_animations[eStatus::LOOKING_UP] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LOOKING_UP]->addFrameRect(eObjectID::ALADDIN, "lookup_01", "lookup_02", "lookup_03", NULL);
	_animations[eStatus::LOOKING_UP]->animateFromTo(0, 1, false);

	_animations[eStatus::LOOKING_UP | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::LOOKING_UP | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "lookup_slash_01", "lookup_slash_02", "lookup_slash_03", "lookup_slash_04", "lookup_slash_05", "lookup_slash_06", "lookup_slash_07", "lookup_slash_08", "lookup_slash_09", "lookup_slash_10", "lookup_slash_11", "lookup_slash_12", NULL);

	_animations[eStatus::CLIMB_VERTICAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_VERTICAL]->addFrameRect(eObjectID::ALADDIN, "climb_vertical_01", "climb_vertical_02", "climb_vertical_03", "climb_vertical_04", "climb_vertical_05", "climb_vertical_06", "climb_vertical_07", "climb_vertical_08", "climb_vertical_09", "climb_vertical_10", NULL);
	_animations[eStatus::CLIMB_VERTICAL]->animateFromTo(1, 9);

	_animations[eStatus::CLIMB_VERTICAL | eStatus::JUMPING] = new Animation(_sprite, 0.16f);
	_animations[eStatus::CLIMB_VERTICAL | eStatus::JUMPING]->addFrameRect(eObjectID::ALADDIN, "jump_out_01", "jump_out_02", "jump_out_03", "jump_out_04", "jump_out_05", "jump_out_06", "jump_out_07", "jump_out_08", "jump_out_09", NULL);

	_animations[eStatus::CLIMB_HORIZON] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_HORIZON]->addFrameRect(eObjectID::ALADDIN, "climb_horizon_01", "climb_horizon_02", "climb_horizon_03", "climb_horizon_04", "climb_horizon_05", "climb_horizon_06", "climb_horizon_07", "climb_horizon_08", "climb_horizon_09", "climb_horizon_10", NULL);
	_animations[eStatus::CLIMB_HORIZON]->addTransition(GVector2(0, 4 * SCALE_FACTOR));

	_animations[eStatus::CLIMB_HORIZON | eStatus::BORING1] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_HORIZON | eStatus::BORING1]->addFrameRect(eObjectID::ALADDIN, "climb_horizon_boring_01", "climb_horizon_boring_02", "climb_horizon_boring_03", "climb_horizon_boring_04", "climb_horizon_boring_05", NULL);

	_animations[eStatus::CLIMB_HORIZON | eStatus::JUMPING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_HORIZON | eStatus::JUMPING]->addFrameRect(eObjectID::ALADDIN, "tumble_01", "tumble_02", "tumble_03", "tumble_04", "tumble_05", "tumble_06", "tumble_07", "tumble_08", NULL);

	_animations[eStatus::CLIMB_HORIZON | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_HORIZON | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "throw_climb_01", "throw_climb_02", "throw_climb_03", "throw_climb_04", "throw_climb_05", NULL);
	_animations[eStatus::CLIMB_HORIZON | eStatus::THROW]->addTransition(GVector2(0, 8 * SCALE_FACTOR));

	_animations[eStatus::CLIMB_HORIZON | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_HORIZON | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "slash_climb_01", "slash_climb_02", "slash_climb_03", "slash_climb_04", "slash_climb_05", "slash_climb_06", "slash_climb_07", NULL);
	_animations[eStatus::CLIMB_HORIZON | eStatus::SLASH]->addTransition(GVector2(0, 9 * SCALE_FACTOR));

	_animations[eStatus::CLIMB_VERTICAL | eStatus::SLASH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_VERTICAL | eStatus::SLASH]->addFrameRect(eObjectID::ALADDIN, "slash_climb_01", "slash_climb_02", "slash_climb_03", "slash_climb_04", "slash_climb_05", "slash_climb_06", "slash_climb_07", NULL);


	_animations[eStatus::CLIMB_VERTICAL | eStatus::THROW] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CLIMB_VERTICAL | eStatus::THROW]->addFrameRect(eObjectID::ALADDIN, "throw_climb_01", "throw_climb_02", "throw_climb_03", "throw_climb_04", "throw_climb_05", NULL);


	_animations[eStatus::PUSH] = new Animation(_sprite, 0.07f);
	_animations[eStatus::PUSH]->addFrameRect(eObjectID::ALADDIN, "push_01", "push_02", "push_03", "push_04", "push_05", "push_06", "push_07", "push_08", "push_09", NULL);
	_animations[eStatus::PUSH]->animateFromTo(1, 8);

	_animations[eStatus::BEATEN] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BEATEN]->addFrameRect(eObjectID::ALADDIN, "burned_01", "burned_02", "burned_03", "burned_04", "burned_05", "burned_06", NULL);

	_animations[eStatus::DYING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::DYING]->addFrameRect(eObjectID::ALADDIN, "die_01", "die_02", "die_03", "die_04", "die_05", "die_06", "die_07", "die_08", "die_09", "die_10", "die_11", "die_12", "die_13", NULL);

	_animations[eStatus::CARE] = new Animation(_sprite, 0.07f);
	_animations[eStatus::CARE]->addFrameRect(eObjectID::ALADDIN, "care_01", "care_02", "care_03", "care_04", "care_05", "care_06", "care_07", "care_08", "care_09", "care_10", "care_11", "care_12", "care_13", "care_14", "care_15", NULL);

	_animations[eStatus::REVIVE] = new Animation(_sprite, 0.07f);
	_animations[eStatus::REVIVE]->addFrameRect(eObjectID::ALADDIN, "revive_01", "revive_02", "revive_03", "revive_04", "revive_05", "revive_06", "revive_07", "revive_08", "revive_09", "revive_10", "revive_11", "revive_12", "revive_13", "revive_14", NULL);

	_animations[eStatus::JEALOUS] = new Animation(_sprite, 0.07f);
	_animations[eStatus::JEALOUS]->addFrameRect(eObjectID::ALADDIN, "jealous_01", "jealous_02", "jealous_03", "jealous_04", "jealous_05", "jealous_06", "jealous_07", "jealous_08", NULL);

	_animations[eStatus::BRAKING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BRAKING]->addFrameRect(eObjectID::ALADDIN, "braking_01", "braking_02", "braking_03", "braking_04", "braking_05", "braking_06", "braking_07", "braking_08", "braking_09", NULL);

	_animations[eStatus::BORING1] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BORING1]->addFrameRect(eObjectID::ALADDIN, "normal_01", "normal_02", "normal_03", "normal_04", "normal_05", "normal_06", "normal_07", NULL);
	vector<float> customTime(7, 0.07f);
	customTime[3] = 0.5f;
	customTime[6] = 0.5f;
	_animations[eStatus::BORING1]->setCustomTime(customTime);
	_animations[eStatus::BORING1]->setLoop(true);


	_animations[eStatus::BORING2] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BORING2]->addFrameRect(eObjectID::ALADDIN, "boring_01", "boring_02", "boring_03", "boring_04", "boring_05", "boring_06", "boring_07", "boring_08", "boring_09", "boring_10", "boring_11", "boring_12", "boring_13", "boring_14", "boring_15", NULL);
	_animations[eStatus::BORING2]->animateFromTo(5, 14, true);
	vector<float> customTime2(15, 0.07f);
	customTime2[13] = 0.5f;
	_animations[eStatus::BORING2]->setCustomTime(customTime2);
	_animations[eStatus::BORING2]->setLoop(true);

	_animations[eStatus::BORING3] = new Animation(_sprite, 0.07f);
	_animations[eStatus::BORING3]->addFrameRect(eObjectID::ALADDIN, "boring_circus_01", "boring_circus_02", "boring_circus_03", "boring_circus_04", "boring_circus_05", "boring_circus_06", "boring_circus_07", "boring_circus_08", "boring_circus_09", "boring_circus_10", "boring_circus_11", "boring_circus_12", "boring_circus_13", "boring_circus_14", "boring_circus_15", "boring_circus_16", "boring_circus_17", NULL);
	vector<float> customTime3(17, 0.07f);
	customTime3[15] = 0.5f;
	_animations[eStatus::BORING3]->setCustomTime(customTime3);
	_animations[eStatus::BORING3]->setLoop(true);

	//_transition[eStatus::JUMPING][eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	//_transition[eStatus::JUMPING][eStatus::NORMAL]->addFrameRect(eObjectID::ALADDIN, "jump_moving_08","jump_moving_09", NULL);
	//_transition[eStatus::FALLING][eStatus::NORMAL] = _transition[eStatus::JUMPING][eStatus::NORMAL];



	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void AladdinAnimationComponent::setAnimation(int status)
{
	if (_index != status)
	{
		_preindex = _index;
		//Một số trạng thái ko restart lại animation
		if (_animations[_index] != nullptr && _index != eStatus::NORMAL && _index != eStatus::RUNNING)
			_animations[_index]->restart();
		_index = status;
		if (_transition[_preindex][_index] != nullptr)
		{
			_transition[_preindex][_index]->restart();
		}
		_transitionPlayed = false;
	}
}


GVector2 AladdinPhysicsComponent::getVelocity()
{
	auto move = (Movement*)getComponent("Movement");
	return move->getVelocity();
}


RECT AladdinPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}


void AladdinBehaviorComponent::init()
{
	_hitpoint = START_HIT_POINT;
	_isBoring = false;
	_climbingLadder = false;
	_preStatus = eStatus::NORMAL;
	_life = 3;
	_numApple = 15;
	_numMoney = 0;
	_isPulled = false;
	_lambObject = nullptr;
	setWeapon(eStatus::NORMAL);
	setStatus(eStatus::FALLING);
	falling();
	faceRight();
	srand(time(0));
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}
// Khi setstatus mới cho nhân vật thì break ngay đoạn đó để tránh lỗi
void AladdinBehaviorComponent::update(float detatime)
{
	vector<GameObject *> objs;
	GameObject * object;
	Land *	landObject;
	Rope * ropeObject;
	eDirection direction;
	auto movement = (Movement *)_obj->getPhysicsComponent()->getComponent("Movement");
	GVector2 otherPos;
	GVector2 otherPos2;
	GVector2 myPos;
	float diffY;
	updateZindex();
	checkCollision(detatime);
	if (_input->isKeyPressed(BT_BOUND))
	{
		RECT bound = _obj->getAnimationComponent()->getBounding();
		OutputDebugStringW(L"\n\n\n\n\n\n\n\n\n\n\n");
		OutputDebugStringW(L"Position X : ");
		__debugoutput(_obj->getPhysicsComponent()->getPositionX());
		OutputDebugStringW(L"Position Y : ");
		__debugoutput(_obj->getPhysicsComponent()->getPositionY());
		OutputDebugStringW(L"Bound bottom: ");
		__debugoutput(bound.bottom);
		OutputDebugStringW(L"Bound left : ");
		__debugoutput(bound.left);
		OutputDebugStringW(L"Bound right : ");
		__debugoutput(bound.right);
	}

	if (_obj->getPhysicsComponent()->getPositionY() + ALADDIN_HEIGHT < 0)
	{
		die();
		return;
	}

	if (_protectTime > 0)
	{
		_protectTime -= detatime;
		_obj->getAnimationComponent()->getCurrentAnimation()->enableFlashes(true);
	}
	else
	{
		_obj->getAnimationComponent()->getCurrentAnimation()->enableFlashes(false);
	}
	auto f = std::bind(&ObjectHelper::isLandBelow, _obj, _1);
	switch (_status)
	{
	case NORMAL:
		_boringTimer += detatime;
		object = _collisionComponent->isColliding(f);
		if (object == nullptr)
		{
			setStatus(eStatus::FALLING);
			falling();
			break;
		}
		if (_boringTimer >= ALADDIN_BORING_TIME)
		{
			_obj->getAnimationComponent()->setAnimation(eStatus::BORING1);
			setBoringAnimation();
		}
		if (_input->isKeyDown(BT_LEFT))
		{
			setStatus(eStatus::RUNNING);
			moveLeft();
			break;
		}
		if (_input->isKeyDown(BT_RIGHT))
		{
			setStatus(eStatus::RUNNING);
			moveRight();
			break;
		}
		if (_input->isKeyPressed(BT_JUMP))
		{
			setStatus(eStatus::JUMPING);
			jump();
			break;
		}
		if (_input->isKeyDown(BT_UP))
		{
			setStatus(eStatus::LOOKING_UP);
			moveViewport.Emit(VIEWPORT_MOVEUP_OFFSET, true);
			break;
		}
		if (_input->isKeyDown(BT_DOWN))
		{
			setStatus(eStatus::LAYING_DOWN);
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case FALLING:
		//move left
		if (_input->isKeyDown(BT_LEFT))
		{
			moveLeft(0.7);
		}
		//move right
		if (_input->isKeyDown(BT_RIGHT))
		{
			moveRight(0.7);
		}
		//release all the key
		if (!_input->isKeyDown(BT_LEFT) && !_input->isKeyDown(BT_RIGHT))
		{
			removeMovementX();
		}
		//slash
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case JUMPING:
		if (_obj->getPhysicsComponent()->getVelocity().y < 0)
		{
			setStatus(eStatus::FALLING);
			break;
		}
		//move left
		if (_input->isKeyDown(BT_LEFT))
		{
			moveLeft(0.7);
		}
		//move right
		if (_input->isKeyDown(BT_RIGHT))
		{
			moveRight(0.7);
		}
		//release all the key
		if (!_input->isKeyDown(BT_LEFT) && !_input->isKeyDown(BT_RIGHT))
		{
			removeMovementX();
		}
		//slash
		if (_input->isKeyPressed(BT_SLASH))
		{
			_isBoring = false;
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			_isBoring = false;
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case LAYING_DOWN:
		if (_input->isKeyRelease(BT_DOWN))
		{
			setStatus(eStatus::NORMAL);
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case RUNNING:
		object = _collisionComponent->isColliding(f);
		if (object == nullptr)
		{
			setStatus(eStatus::FALLING);
			falling();
		}
		if (_input->isKeyDown(BT_LEFT))
		{
			if (_facingDirection == eDirection::LEFT)
			{
				_brakeTimer += detatime;
			}
			else
			{
				_brakeTimer = 0;
			}
			moveLeft();
		}
		if (_input->isKeyDown(BT_RIGHT))
		{
			if (_facingDirection == eDirection::RIGHT)
			{
				_brakeTimer += detatime;
			}
			else
			{
				_brakeTimer = 0;
			}
			moveRight();
		}
		if (_input->isKeyPressed(BT_JUMP))
		{
			setStatus(eStatus::JUMPING);
			jump();
			break;
		}
		if (!_input->isKeyDown(BT_LEFT) && !_input->isKeyDown(BT_RIGHT))
		{
			if (_brakeTimer >= RUNNING_BRAKE_TIME)
			{
				_brakeTimer = 0;
				standing();
				setStatus(eStatus::BRAKING);
			}
			else
			{
				_brakeTimer = 0;
				standing();
				setStatus(eStatus::NORMAL);
			}
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case LOOKING_UP:
		if (_input->isKeyRelease(BT_UP))
		{
			moveViewport.Emit(VIEWPORT_MOVEUP_OFFSET, false);
			setStatus(eStatus::NORMAL);
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		break;
	case CLIMB_VERTICAL:
		ropeObject = (Rope*)_colliseObject;

		if (_input->isKeyDown(BT_UP))
		{
			if (ropeObject != nullptr)
			{
				if (ropeObject->getPhysicsComponent()->getPositionY() >= _obj->getPhysicsComponent()->getPositionY() + ALADDIN_HEIGHT)
				{
					_obj->getAnimationComponent()->getCurrentAnimation()->setReserve(false);
					_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
					moveUp();
				}
				else
				{
					_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
					standing();
				}
			}
		}
		if (_input->isKeyDown(BT_DOWN))
		{
			if (ropeObject != nullptr)
			{
				if (_obj->getPhysicsComponent()->getBounding().top >= ropeObject->getPhysicsComponent()->getBounding().bottom)
				{
					_obj->getAnimationComponent()->getCurrentAnimation()->setReserve(true);
					_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
					moveDown();
				}
				else
				{
					setStatus(eStatus::FALLING);
					falling();
					break;
				}
			}
			else
			{
				//setStatus(eStatus::FALLING);
				//falling();
				//break;
			}
			
		}
		if (_input->isKeyDown(BT_LEFT))
		{
			faceLeft();
		}
		if (_input->isKeyDown(BT_RIGHT))
		{
			faceRight();
		}
		if (!_input->isKeyDown(BT_UP) && !_input->isKeyDown(BT_DOWN))
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
			standing();
		}
		if (_input->isKeyPressed(BT_JUMP))
		{
			setStatus(eStatus::JUMPING);
			jump();
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			_isBoring = false;
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			_isBoring = false;
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case CLIMB_HORIZON:
		ropeObject = (Rope*)_collisionComponent->isColliding(eObjectID::ROPE);
		if (ropeObject == nullptr)
		{
			setStatus(eStatus::FALLING);
			//_obj->getPhysicsComponent()->setPositionY(_obj->getPhysicsComponent()->getPositionY() - JUMP_OFFSET*3.0);
			standing(); // to remove velocity
			falling();
			break;
		}
		if (_input->isKeyDown(BT_LEFT))
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
			moveLeft();
			faceLeft();
		}
		if (_input->isKeyDown(BT_RIGHT))
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
			moveRight();
			faceRight();
		}
		if (!_input->isKeyDown(BT_LEFT) && !_input->isKeyDown(BT_RIGHT))
		{
			_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
			standing();
		}
		if (_input->isKeyDown(BT_DOWN) && _input->isKeyDown(BT_JUMP))
		{
			setStatus(eStatus::FALLING);
			_obj->getPhysicsComponent()->setPositionY(_obj->getPhysicsComponent()->getPositionY() - JUMP_OFFSET*5.0);
			standing(); // to remove velocity
			falling();
			break;
		}
		if (_input->isKeyPressed(BT_JUMP))
		{
			setStatus(eStatus::JUMPING);
			_obj->getPhysicsComponent()->setPositionY(_obj->getPhysicsComponent()->getPositionY() + ALADDIN_CLIMB_HEIGHT * 2/3);
			jump();
			break;
		}
		if (_input->isKeyPressed(BT_SLASH))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				slash();
				break;
			}
		}
		if (_input->isKeyPressed(BT_THROW))
		{
			if (getWeapon() == eStatus::NORMAL)
			{
				throwApple();
				break;
			}
		}
		break;
	case PUSH:
		if (_facingDirection == eDirection::LEFT)
		{
			if (_input->isKeyPressed(BT_LEFT))
			{
				if (!SoundManager::getInstance()->IsPlaying(eSoundId::sALADDIN_PUSH))
				{
					SoundManager::getInstance()->Play(eSoundId::sALADDIN_PUSH);
				}
			}
			if (_input->isKeyRelease(BT_LEFT))
			{
				_obj->getAnimationComponent()->setOrigin(GVector2(0.f, 0.f));
				RECT bound = _obj->getPhysicsComponent()->getBounding();
				float width = bound.right - bound.left;
				_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + width);
				setStatus(eStatus::NORMAL);
				break;
			}
			if (_input->isKeyPressed(BT_RIGHT))
			{
				_obj->getAnimationComponent()->setOrigin(GVector2(0.f, 0.f));
				RECT bound = _obj->getPhysicsComponent()->getBounding();
				float width = bound.right - bound.left;
				_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + width);
				setStatus(eStatus::NORMAL);
				moveRight();
				break;
			}
		}
		if (_facingDirection == eDirection::RIGHT)
		{
			if (_input->isKeyPressed(BT_RIGHT))
			{
				if (!SoundManager::getInstance()->IsPlaying(eSoundId::sALADDIN_PUSH))
				{
					SoundManager::getInstance()->Play(eSoundId::sALADDIN_PUSH);
				}
			}
			if (_input->isKeyRelease(BT_RIGHT))
			{
				_obj->getAnimationComponent()->setOrigin(GVector2(0.f, 0.f));
				RECT bound = _obj->getPhysicsComponent()->getBounding();
				float width = bound.right - bound.left;
				_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - width);
				setStatus(eStatus::NORMAL);
				break;
			}
			if (_input->isKeyPressed(BT_LEFT))
			{
				_obj->getAnimationComponent()->setOrigin(GVector2(0.f, 0.f));
				RECT bound = _obj->getPhysicsComponent()->getBounding();
				float width = bound.right - bound.left;
				_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - width);
				setStatus(eStatus::NORMAL);
				moveLeft();
				break;
			}
		}
		break;
	case REVIVE:
		if (_lambObject != nullptr)
		{
			if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
			{
				int height = _obj->getAnimationComponent()->getSprite()->getFrameHeight();
				_obj->getPhysicsComponent()->setPositionY(_obj->getPhysicsComponent()->getPositionY() + height);
				_lambObject->getAnimationComponent()->setOpacity(1.0f);
				_lambObject->getAnimationComponent()->getCurrentAnimation()->setIndex(_lambObject->getAnimationComponent()->getCurrentAnimation()->getNumberOfFrames() - 1);
				setStatus(eStatus::FALLING);
				falling();
				break;
			}
			else
			{
				_lambObject->getAnimationComponent()->setOpacity(0.0f);
			}

		}
		else
		{
			setStatus(eStatus::FALLING);
			falling();
		}

		break;
	case BRAKING:
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() < 1)
		{
			if (_facingDirection == eDirection::LEFT)
			{
				moveLeft(0.2);
			}
			
			if (_facingDirection == eDirection::RIGHT)

			{
				moveRight(0.2);
			}
		}
		else
		{
			standing();
			setStatus(eStatus::NORMAL);
		}
		break;
	default:
		break;
	}
	if (_obj->getAnimationComponent()->isTempAnimationEmpty() == true)
	{
		setWeapon(eStatus::NORMAL);
	}
}

void AladdinBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	if (_status == eStatus::NORMAL)
	{
		_boringTimer = 0;
	}
	setWeapon(eStatus::NORMAL);
	updateAnimation();
}


void AladdinBehaviorComponent::updateTimeOut(float deltaTime)
{
}

void AladdinBehaviorComponent::updateAnimation()
{

	switch (_status)
	{
	case NORMAL:
		if (_isBoring == false)
		{
				_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		}
		break;
	case JUMPING:
			switch (_preStatus)
			{
			case RUNNING:
				_obj->getAnimationComponent()->setAnimation(eStatus::MOVING_JUMPING);
				break;
			case CLIMB_VERTICAL:
				_obj->getAnimationComponent()->setAnimation(eStatus::CLIMB_VERTICAL | eStatus::JUMPING);
				break;
			case CLIMB_HORIZON:
				_obj->getAnimationComponent()->setAnimation(eStatus::CLIMB_HORIZON | eStatus::JUMPING);
				break;
			default:
				_obj->getAnimationComponent()->setAnimation(eStatus::JUMPING);
				break;
			}
		break;
	case LAYING_DOWN:
		_obj->getAnimationComponent()->setAnimation(eStatus::LAYING_DOWN);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	case LOOKING_UP:
		_obj->getAnimationComponent()->setAnimation(eStatus::LOOKING_UP);
		break;
	case FALLING:
		if (_preStatus == eStatus::RUNNING && inLadderRange())
			return;
		_obj->getAnimationComponent()->setAnimation(eStatus::FALLING);
		break;
	case CLIMB_VERTICAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::CLIMB_VERTICAL);
		break;
	case CLIMB_HORIZON:
		_obj->getAnimationComponent()->setAnimation(eStatus::CLIMB_HORIZON);
		break;
	case BURNED:
		_obj->getAnimationComponent()->setAnimation(eStatus::BURNED);
		break;
	case BRAKING:
		_obj->getAnimationComponent()->setAnimation(eStatus::BRAKING);
		break;
	case PUSH:
		_obj->getAnimationComponent()->setAnimation(eStatus::PUSH);
		break;
	case REVIVE:
		if (_lambObject == nullptr)
		{
			_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		}
		else
		{
			_obj->getAnimationComponent()->setAnimation(eStatus::REVIVE);

		}
		break;
		
	default:
		break;
	}
}

void AladdinBehaviorComponent::setBoringAnimation()
{
	if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
	{
		int random = rand() % 3;
		switch (random)
		{
		case 0:
			_obj->getAnimationComponent()->setAnimation(eStatus::BORING1);
			break;
		case 1:
			_obj->getAnimationComponent()->setAnimation(eStatus::BORING2);
			break;
		case 2:
			_obj->getAnimationComponent()->setAnimation(eStatus::BORING3);
			break;
		default:
			_obj->getAnimationComponent()->setAnimation(eStatus::BORING1);
			break;
		}
	}
}

void AladdinBehaviorComponent::faceLeft()
{
	if (_obj->getAnimationComponent()->getScale().x > 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		RECT bound = _obj->getPhysicsComponent()->getBounding();
		float width = bound.right - bound.left;
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + width);
	}
	setFacingDirection(eDirection::LEFT);
}

void AladdinBehaviorComponent::faceRight()
{
	if (_obj->getAnimationComponent()->getScale().x < 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		RECT bound = _obj->getPhysicsComponent()->getBounding();
		float width = bound.right - bound.left;
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - width);
	}
	setFacingDirection(eDirection::RIGHT);
}

void AladdinBehaviorComponent::standing()
{
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, 0));

	auto gravity = (Gravity*)_obj->getPhysicsComponent()->getComponent("Gravity");
	gravity->setStatus(eGravityStatus::LANDED);

}

void AladdinBehaviorComponent::moveLeft(float power)
{
	faceLeft();
	auto viewport = SceneManager::getInstance()->getCurrentScene()->getViewport();
	RECT viewport_in_transform = viewport->getBounding();
	if (_obj->getPhysicsComponent()->getPositionX() - _obj->getPhysicsComponent()->getMovingSpeed() * 0.33 <= viewport_in_transform.left) // hard code
	{
		_obj->getPhysicsComponent()->setPositionX(viewport_in_transform.left + _obj->getPhysicsComponent()->getMovingSpeed() * 0.33);
		return;
	}
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(-_obj->getPhysicsComponent()->getMovingSpeed() * power, move->getVelocity().y));
}

void AladdinBehaviorComponent::moveRight(float power)
{
	faceRight();
	auto viewport = SceneManager::getInstance()->getCurrentScene()->getViewport();
	RECT viewport_in_transform = viewport->getBounding();
	if (_obj->getPhysicsComponent()->getPositionX() + _obj->getPhysicsComponent()->getMovingSpeed() * 0.33 >= viewport_in_transform.right) // hard code
	{
		_obj->getPhysicsComponent()->setPositionX(viewport_in_transform.right - _obj->getPhysicsComponent()->getMovingSpeed() * 0.33);
		return;
	}
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(_obj->getPhysicsComponent()->getMovingSpeed() * power, move->getVelocity().y));
}

void AladdinBehaviorComponent::moveUp()
{
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, ALADDIN_CLIMB_SPEED));
}

void AladdinBehaviorComponent::moveDown()
{
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, -ALADDIN_CLIMB_SPEED));
}

void AladdinBehaviorComponent::jump(float jumpVel)
{
	_obj->getPhysicsComponent()->setPositionY(_obj->getPhysicsComponent()->getPositionY() + JUMP_OFFSET);
	auto move = (Movement*)this->_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(move->getVelocity().x, jumpVel));

	auto g = (Gravity*)this->_obj->getPhysicsComponent()->getComponent("Gravity");
	g->setStatus(eGravityStatus::FALLING__DOWN);
}

int AladdinBehaviorComponent::getNumApple()
{
	return _numApple;
}

void AladdinBehaviorComponent::setNumApple(int value)
{
	_numApple = value;
}

int AladdinBehaviorComponent::getMoney()
{
	return _numMoney;
}

void AladdinBehaviorComponent::setMoney(int value)
{
	_numMoney = value;
}

void AladdinBehaviorComponent::slash()
{
	setWeapon(eStatus::SLASH);
	updateWeaponAnimation(_status);
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += _obj->getAnimationComponent()->getSprite()->getFrameHeight();
	float width = _obj->getAnimationComponent()->getSprite()->getFrameWidth() * 4/3;
	float height = _obj->getAnimationComponent()->getSprite()->getFrameHeight();
	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(_obj->getAnimationComponent()->getSprite()->getFrameWidth(), 0);
	}
	else
	{
		pos -= GVector2(_obj->getAnimationComponent()->getSprite()->getFrameWidth() * 2, 0);
	}
	auto sword = ObjectFactory::getSword(pos, width, height, true);
	addToScene.Emit(sword);
	SoundManager::getInstance()->Play(eSoundId::sALADDIN_SLASH);
}

void AladdinBehaviorComponent::throwApple()
{
	if (_numApple > 0)
		_numApple--;
	else
		return;

	setWeapon(eStatus::THROW);
	updateWeaponAnimation(_status);
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.y += _obj->getAnimationComponent()->getSprite()->getFrameHeight() * 2 / 3;
	GVector2 velocity(-600, 300);
	if (_facingDirection == eDirection::RIGHT)
	{
		pos += GVector2(_obj->getAnimationComponent()->getSprite()->getFrameWidth(), 0);
		velocity.x = -velocity.x;
	}
	else
	{
		pos -= GVector2(_obj->getAnimationComponent()->getSprite()->getFrameWidth(), 0);
	}
	auto apple = ObjectFactory::getApple(pos, velocity);
	addToScene.Emit(apple);
	SoundManager::getInstance()->Play(eSoundId::sOBJECT_THROW);

}

void AladdinBehaviorComponent::updateWeaponAnimation(int status)
{
	if (_weapon != eStatus::NORMAL)
	{
		_obj->getAnimationComponent()->setTempAnimation(status | _weapon, 1);
	}
	else
	{
		_obj->getAnimationComponent()->setAnimation(status);
	}

	if (_obj->getAnimationComponent()->isTempAnimationEmpty() == true)
	{
		_weapon = eStatus::NORMAL;
	}
}

void AladdinBehaviorComponent::removeGravity()
{
	auto gravity = (Gravity*)_obj->getPhysicsComponent()->getComponent("Gravity");
	gravity->setStatus(eGravityStatus::LANDED);
}

void AladdinBehaviorComponent::removeMovementX()
{
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, move->getVelocity().y));
}


void AladdinBehaviorComponent::falling()
{
	auto g = (Gravity*)this->_obj->getPhysicsComponent()->getComponent("Gravity");
	g->setStatus(eGravityStatus::FALLING__DOWN);
}

void AladdinBehaviorComponent::climbVertical()
{
	standing();
}

void AladdinBehaviorComponent::climbHorizon()
{
	standing();
}

void AladdinBehaviorComponent::setRespawnPosition(GVector2 respawnPosition)
{
	_respawnPostion = respawnPosition;
}

bool AladdinBehaviorComponent::dropHitpoint(int damage)
{
	//Đang được bảo vệ, không làm gì cả
	if (_protectTime > 0)
		return false;

	PlayerBehaviorComponent::dropHitpoint(damage);
	if (_hitpoint > 0)
	{
		_obj->getAnimationComponent()->setTempAnimation(eStatus::BEATEN, 1);
		_protectTime = ALADDIN_PROTECT_TIME;
		SoundManager::getInstance()->Play(eSoundId::sALADDIN_HURT);
	}
	else
	{
		die();
	}
	return true;
}

void AladdinBehaviorComponent::resetValue()
{
	_hitpoint = START_HIT_POINT;
	_isBoring = false;
	_climbingLadder = false;
	_preStatus = eStatus::NORMAL;
	_brakeTimer = 0;
	setWeapon(eStatus::NORMAL);
	setStatus(eStatus::NORMAL);
	standing();
}

void AladdinBehaviorComponent::die()
{
	SoundManager::getInstance()->Stop();
	_life--;
	SceneManager::getInstance()->addScene(new DeathScene());
}

void AladdinBehaviorComponent::respawn()
{
	resetValue();
	setStatus(eStatus::REVIVE);
	faceRight();
	if (_lambObject == nullptr)
	{
		_obj->getPhysicsComponent()->setPosition(_respawnPostion);
	}
	else
	{
		_obj->getPhysicsComponent()->setPosition(_lambObject->getPhysicsComponent()->getPosition());
	}
}

void AladdinBehaviorComponent::setIsPull(bool isPull)
{
	_isPulled = isPull;
}

void AladdinBehaviorComponent::executeCommand(eCommand command)
{

}

void AladdinBehaviorComponent::checkCollision(float deltatime)
{
	//declare
	eRopeType ropeType;
	RECT modifiedBound = _obj->getPhysicsComponent()->getBounding();
	float middleBound;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case LAND:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
				handleCollisionLand(obj, deltatime);
			break;
		case ROPE:
			ropeType = ((Rope *)obj)->getRopeType();
			switch (ropeType)
			{
			case rVERTICAL:
				middleBound = (modifiedBound.left + modifiedBound.right) / 2;
				modifiedBound.left = middleBound - 5;
				modifiedBound.right = middleBound + 5;
				break;
			case rHORIZONTAL:
				modifiedBound.bottom = modifiedBound.top - 20;
				break;
			default:
				break;
			}

			if (_collisionComponent->checkCollision(obj, modifiedBound, deltatime, false))
				handleCollisionRope(obj, deltatime);
			break;
		case WALL:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				handleCollisionWall(obj, deltatime);
			}
			break;
		case ITEM:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				handleCollisionItem(obj, deltatime);
			}
			break;
		default:
			break;
		}
	}
}

void AladdinBehaviorComponent::handleCollisionLand(GameObject* otherObject, float deltatime)
{
	LandBehaviorComponent * landBehavior = (LandBehaviorComponent *)otherObject->getBehaviorComponent();
	eDirection colliSide = _collisionComponent->getCollidingDirection(otherObject);
	if (colliSide == eDirection::TOP)
	{
		float moveX, moveY;
		switch (_status)
		{
		case FALLING:
			if (fallthroughtLandRange())
			{
				eZindex myZindex = (eZindex)((int)_obj->getPhysicsComponent()->getZIndex());
				eZindex otherZIndex = (eZindex)((int)otherObject->getPhysicsComponent()->getZIndex());

				//ZIndex của đối tượng có trong aladdin
				if ((myZindex & otherZIndex) == otherZIndex || otherZIndex == Z0)
				{
					goto check_collision_fall;
				}
			}
			else
			{
			check_collision_fall:
				// high fall speed 
				
				if (_obj->getPhysicsComponent()->getVelocity().y < -1500)
				{
					setStatus(eStatus::NORMAL);
					standing();
					_obj->getPhysicsComponent()->setPositionY(otherObject->getPhysicsComponent()->getPositionY());
				}
				else if (_collisionComponent->isColliding(otherObject, moveX, moveY, deltatime) && _obj->getPhysicsComponent()->getPositionY() > otherObject->getPhysicsComponent()->getBounding().bottom)
				{
					setStatus(eStatus::NORMAL);
					standing();
					eDirection direction = _collisionComponent->getSide(otherObject);
					_collisionComponent->updateTargetPosition(otherObject, colliSide, GVector2(moveX, moveY));;
				}
			}
			break;
		case RUNNING: 
			if (fallthroughtLandRange())
			{
				eZindex myZindex = (eZindex)((int)_obj->getPhysicsComponent()->getZIndex());
				eZindex otherZIndex = (eZindex)((int)otherObject->getPhysicsComponent()->getZIndex());

				//ZIndex của đối tượng có trong aladdin
				if (((myZindex != 0 && (myZindex & otherZIndex) == myZindex)) || otherZIndex == Z0)
				{
					goto check_collision_running;
				}
			}
			else
			{
			check_collision_running:
				if (_collisionComponent->isColliding(otherObject, moveX, moveY, deltatime))
				{
					eDirection direction = _collisionComponent->getSide(otherObject);
					_collisionComponent->updateTargetPosition(otherObject, colliSide, GVector2(moveX, moveY));
				}
			}
		}
	}
}

void AladdinBehaviorComponent::handleCollisionRope(GameObject* otherObject, float deltatime)
{
	Rope* ropeObject = (Rope*)otherObject;
	switch (_status)
	{
	case FALLING:
		//&& ropeObject->getPhysicsComponent()->getPositionY() >= _obj->getPhysicsComponent()->getPositionY() + ALADDIN_CLIMB_HEIGHT
		if (ropeObject->getRopeType() == eRopeType::rVERTICAL )
		{
			if (ropeObject->getPhysicsComponent()->getPositionY() < _obj->getPhysicsComponent()->getBounding().top)
				return;

			setStatus(eStatus::CLIMB_VERTICAL);
			climbVertical();
			faceRight();

			RECT ropeBound = ropeObject->getPhysicsComponent()->getBounding();
			RECT myBound = _obj->getPhysicsComponent()->getBounding();

			float newPostionX = ropeBound.left + (ropeBound.right - ropeBound.left) * 0.5f;// middle postion of the rope
			newPostionX -= (myBound.right - myBound.left) * 0.5f - 5;
			
			_obj->getPhysicsComponent()->setPositionX(newPostionX);
			_colliseObject = otherObject;

			break;

		}
		if (ropeObject->getRopeType() == eRopeType::rHORIZONTAL)
		{
			RECT ropeBound = ropeObject->getPhysicsComponent()->getBounding();
			float newPostionY = ropeBound.top;
			newPostionY -= ALADDIN_CLIMB_HEIGHT;
			//_obj->getAnimationComponent()->setOrigin(GVector2(_obj->getAnimationComponent()->getOrigin().x, 1.0f);
			_obj->getPhysicsComponent()->setPositionY(newPostionY);
			setStatus(eStatus::CLIMB_HORIZON);
			climbHorizon();
			_colliseObject = otherObject;

			break;
		}
		break;
	}
}

void AladdinBehaviorComponent::handleCollisionWall(GameObject * otherObject, float deltatime)
{
	switch (_status)
	{
	case RUNNING:
		//swith origin to 1.0 
		if (_facingDirection == eDirection::LEFT)
		{
			_obj->getAnimationComponent()->setOrigin(GVector2(1.f, 0.f));
			RECT bound = _obj->getPhysicsComponent()->getBounding();
			float width = bound.right - bound.left;
			_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - width);
		}
		else
		{
			_obj->getAnimationComponent()->setOrigin(GVector2(1.f, 0.f));
			RECT bound = _obj->getPhysicsComponent()->getBounding();
			float width = bound.right - bound.left;
			_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + width);
		}

		setStatus(eStatus::PUSH);
		standing();
		break;
	case PUSH:
		break;
	default:
		_collisionComponent->updatePosition(otherObject);
		break;
	}
}

void AladdinBehaviorComponent::handleCollisionItem(GameObject * otherObject, float deltatime)
{
	if (otherObject->getBehaviorComponent()->getStatus() != eStatus::NORMAL)
		return;

	auto type = ((ItemBehaviorComponent*)otherObject->getBehaviorComponent())->getType();
	switch (type)
	{
	case iGENIE_BONUS:
	{
		SoundManager::getInstance()->Play(eSoundId::sWOW);

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT genieBound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((genieBound.right - genieBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX2);
		addToScene.Emit(explosion);

		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);
	}
		break;
	case iABU_BONUS:
	{
		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT abuBound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((abuBound.right - abuBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX3);
		addToScene.Emit(explosion);
	}
		break;
	case iEXTRA_HEART:
	{
		SoundManager::getInstance()->Play(eSoundId::sCOLLECT_ITEM);
		_hitpoint += 250;
		if (_hitpoint > START_HIT_POINT)
			_hitpoint = START_HIT_POINT;

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT heartBound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((heartBound.right - heartBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX3);
		addToScene.Emit(explosion);

		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);
	}
		break;
	case iAPPLE:
	{
		_numApple++;
		SoundManager::getInstance()->Play(eSoundId::sCOLLECT_ITEM);

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT appleBound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((appleBound.right - appleBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX3);
		addToScene.Emit(explosion);

		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);
	}
	break;
	case iMONEY:
	{
		SoundManager::getInstance()->Play(eSoundId::sCOLLECT_ITEM);
		_numMoney++;

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT moneyBound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((moneyBound.right - moneyBound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX3);
		addToScene.Emit(explosion);

		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);
	}
		break;
	case iRESTART_POINT:
		//Ở frame đầu tiên tức là chưa bị chạm vào
		if (otherObject->getAnimationComponent()->getCurrentAnimation()->getIndex() == 0)
		{
			if (!SoundManager::getInstance()->IsPlaying(eSoundId::sCONTINUE_POINT))
				SoundManager::getInstance()->Play(eSoundId::sCONTINUE_POINT);
			_lambObject = otherObject;
			otherObject->getAnimationComponent()->getCurrentAnimation()->setLoop(false);
			otherObject->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
		}
		break;
	case i1UP:
	{
		_life++;

		GVector2 pos = otherObject->getPhysicsComponent()->getPosition();
		RECT bound = otherObject->getPhysicsComponent()->getBounding();
		pos.x += ((bound.right - bound.left) / 2);
		auto explosion = ObjectFactory::getExplosion(pos, eExplostion::EX3);
		addToScene.Emit(explosion);

		otherObject->getBehaviorComponent()->setStatus(eStatus::DESTROY);
	}
		break;
	default:
		break;
	}
}

bool AladdinBehaviorComponent::inLadderRange()
{
	float posX = _obj->getPhysicsComponent()->getPositionX();
	float posY = _obj->getPhysicsComponent()->getPositionY();
	//Đoạn cầu thang đầu tiên
	if (posX >= 1107 * SCALE_FACTOR && posX <= 1387 * SCALE_FACTOR)
		return true;
	//Đoạn cầu thang thứ 2
	if (posX >= 2243 * SCALE_FACTOR && posX <= 2622 * SCALE_FACTOR && posY <= 200 * SCALE_FACTOR)
		return true;
	//Đoạn cầu thang thứ 3
	if (posX >= 4336 * SCALE_FACTOR && posX <= 4500 * SCALE_FACTOR)
		return true;
	//Đoạn cầu thang cuối cùng phía trên
	if (posX >= 3819 * SCALE_FACTOR && posX <= 3977 * SCALE_FACTOR)
		return true;
	return false;
}

bool AladdinBehaviorComponent::fallthroughtLandRange()
{
	float posX = _obj->getPhysicsComponent()->getPositionX();
	if (posX >= 2243 * SCALE_FACTOR && posX <= 2700 * SCALE_FACTOR)
		return true;
	return false;
}




void AladdinBehaviorComponent::updateZindex()
{
	if (_status != eStatus::NORMAL && _status != eStatus::RUNNING)
		return;

	GVector2 pos = _obj->getPhysicsComponent()->getPosition();
	RECT switch1;
	switch1.left = 2645 * SCALE_FACTOR;
	switch1.right = (2645 + 116) * SCALE_FACTOR;
	switch1.top = 63 * SCALE_FACTOR;
	switch1.bottom = 0;
	if (isContain(switch1, pos))
	{
		_obj->getPhysicsComponent()->setZIndex(2);
		return;
	}
	switch1.left = 2246 * SCALE_FACTOR;
	switch1.right = 2279 * SCALE_FACTOR;
	switch1.top = 140 * SCALE_FACTOR;
	switch1.bottom = 110 * SCALE_FACTOR;
	if (isContain(switch1, pos))
	{
		_obj->getPhysicsComponent()->setZIndex(1);
		return;
	}
}

bool ObjectHelper::isLandBelow(GameObject * myObject, GameObject * otherObject)
{
	if (otherObject->getID() == eObjectID::LAND)
	{
		if (myObject->getPhysicsComponent()->getPositionY() >= otherObject->getPhysicsComponent()->getBounding().bottom)
			return true;
	}
	return false;
}
