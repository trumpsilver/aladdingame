﻿#ifndef __OBJECT_FACTORY_H__
#define __OBJECT_FACTORY_H__
#include "../Framework/define.h"
#include "../Framework/GameObject.h"
#include "Player\Aladdin.h"
#include "Object\Apple.h"
#include "Object\Land.h"
#include "Object\Rope.h"
#include "Enemy\Hakim.h"
#include "Enemy\Falza.h"
#include "Enemy\Nahbi.h"
#include "Enemy\Thrower.h"
#include "Enemy\Jar.h"
#include "Enemy\Jugger.h"
#include "Enemy\Jafar.h"
#include "Object\Sword.h"
#include "Object\Flame.h"
#include "Object\ExplosionPot.h"
#include "Object\Explosions.h"
#include "Object\Dagger.h"
#include "Object\Wall.h"
#include "Object\Camel.h"
#include "Object\Item.h"
#include "Object\Abu.h"
#include "Object\Cutscene.h"
#include "Object\Resilience.h"
#include "Object\SnakeFlame.h"
#include "Object\Star.h"
#include "../../pugixml/src/pugixml.hpp"
#include <functional>
using namespace pugi;

class ObjectFactory
{
public:
	ObjectFactory();
	~ObjectFactory();
	static map<string, GameObject*>* getMapObjectFromFile(const string path);
	static map<string, std::function<GameObject * () >>* getMapObjectFunctionFromFile(const string path);
	static GameObject* getAladdin();
	static GameObject* getApple(GVector2 pos, GVector2 velocity);
	static GameObject* getApple(GVector2 pos, GVector2 velocity, GVector2 gravity);
	//make apple move from start to end with gravity in the time.
	static GameObject* getApple(GVector2 start, GVector2 end, GVector2 gravity, float time);
	static GameObject* getDagger(GVector2 pos, GVector2 velocity);
	static GameObject* getDagger(GVector2 pos, GVector2 velocity, GVector2 gravity);
	static GameObject* getDagger(GVector2 start, GVector2 end, GVector2 gravity, float time);
	static GameObject* getSword(GVector2 pos, float width, float height, bool canSlashEnemy);
	static GameObject* getLand(xml_node node);
	static GameObject* getRope(xml_node node);
	static GameObject* getHakim(GVector2 pos, float rangeXStart, float rangeXEnd);
	static GameObject* getHakim(xml_node node);
	static GameObject* getFalza(GVector2 pos, float rangeXStart, float rangeXEnd);
	static GameObject* getFalza(xml_node node);
	static GameObject* getNahbi(GVector2 pos, float rangeXStart, float rangeXEnd);
	static GameObject* getNahbi(xml_node node);
	static GameObject* getJafar(GVector2 pos);
	static GameObject* getJar(GVector2 pos, float rangeXStart, float rangeXEnd);
	static GameObject* getJar(xml_node node);
	static GameObject* getJugger(GVector2 pos, eDirection side);
	static GameObject* getJugger(xml_node node);
	static GameObject* getFlame(GVector2 pos);
	static GameObject* getSnakeFlame(GVector2 pos, eDirection side);
	static GameObject* getCamel(GVector2 pos);
	static GameObject* getCamel(xml_node node);
	static GameObject* getExlplosionPot(GVector2 pos, GVector2 velocity);
	static GameObject* getThrower(GVector2 pos);
	static GameObject* getThrower(xml_node node);
	static GameObject* getWall(xml_node node);
	static GameObject* getExplosion(GVector2 pos, eExplostion type);
	static GameObject* getItem(GVector2 pos, eItem type);
	static GameObject* getItem(xml_node node);
	static GameObject* getAbu(GVector2 pos, eStatus type);
	static GameObject* getCutscene(GVector2 pos, eCutscene type);
	static GameObject* getResilience(GVector2 pos);
	static GameObject* getResilience(xml_node node);
	static GameObject* getStar(GVector2 pos, GVector2 velocity, eDirection side, GameObject * trackerObject);
private:
	static map<string, string> ObjectFactory::getObjectProperties(xml_node node);
	static GameObject* getObjectById(xml_node node, eObjectID id);
	static std::function<GameObject * () > getFunctionById(xml_node node, eObjectID id);
};
#endif // !__OBJECT_FACTORY_H__