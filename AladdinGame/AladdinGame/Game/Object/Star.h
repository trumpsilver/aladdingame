﻿#ifndef __STAR_H__
#define __STAR_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/NullComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"

#define STAR_LIVING_TIME 2000
#define STAR_VACUM_POWER 30
#define STAR_VELOCITY 1000.f
#define STAR_TRACKER_INTERVAL 50
LINK_FRAMEWORK

class StarPhysicsComponent : public PhysicsComponent
{
public:
	void init() override;
	RECT getBounding() override;
	//overidde bounding
};

class StarAnimationComponent : public AnimationComponent
{
public:
	void init() override;
	RECT getBounding();
protected:
	RECT _boundRect;
};

class StarBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
	void setStatus(int status) override;
	void setTrackerObj(GameObject * trackerObj);
	void setPullDirection(eDirection direction);
	void faceLeft();
	void faceRight();
protected:
	void updateAnimation();
	void checkCollision(float deltatime);
	GameObject * _trackerObj;
	eDirection _pullDirection;
	float _timer;
	float _trackerTimer;
	float _width;
	bool _hasCollapsed;
};


#endif

