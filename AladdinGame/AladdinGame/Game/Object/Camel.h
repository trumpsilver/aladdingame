#ifndef __CAMEL_H__
#define __CAMEL_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../../Framework/Component/PlayerComponent.h"
#include "../ObjectFactory.h"

#define CAMEL_PUFF_OFFSETX 131 * SCALE_FACTOR
#define CAMEL_PUFF_OFFSETY 24 * SCALE_FACTOR

class CamelPhysicsComponent : public PhysicsComponent
{
public:
	void init() override;
	RECT getBounding();
protected:
};

class CamelAnimationComponent : public AnimationComponent
{
public:
	void init() override;
};

class CamelBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
	void setStatus(int status) override;
protected:
	void updateAnimation();
	void checkCollision(float deltatime);
	void puffApple();
	float _timer;
};


#endif


