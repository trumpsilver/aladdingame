#include "StarCreator.h"
#include <cmath>

sigcxx::Signal<GameObject * > StarCreator::addToScene;

StarCreator::StarCreator()
{
	_timer = 0;
	_numberStar = 0;
}


StarCreator::~StarCreator()
{
}

void StarCreator::update(float deltatime)
{
	_timer += deltatime;
	if (_timer >= STAR_CREATOR_INTERVAL)
	{
		_timer -= STAR_CREATOR_INTERVAL;
	}
	else
	{
		return;
	}

	if (_trackerObj == nullptr)
		return;
	//Th�m ng�i sao v�o m�n h�nh
	RECT trackerBound = _trackerObj->getPhysicsComponent()->getBounding();


	GVector2 velocity;
	velocity.x = (trackerBound.right + trackerBound.left) / 2 - _pos.x;
	velocity.y =  (trackerBound.top + trackerBound.bottom) / 2 - _pos.y;
	// tinh ti le
	float factor = STAR_VELOCITY / sqrt(velocity.x * velocity.x + velocity.y + velocity.y);
	velocity = velocity * factor;

	eDirection pullDirection = eDirection::LEFT;
	if ((trackerBound.right + trackerBound.left) / 2 < _pullDest.x)
	{
		pullDirection = eDirection::RIGHT;
	}

	auto star = ObjectFactory::getStar(_pos, velocity, pullDirection, _trackerObj);
	addToScene.Emit(star);
}

void StarCreator::setPostion(GVector2 start)
{
	_pos = start;
}

void StarCreator::setTrackerObj(GameObject * obj)
{
	_trackerObj = obj;
}

void StarCreator::setPullDest(GVector2 destination)
{
	_pullDest = destination;
}

