#include "Resilience.h"

void ResiliencePhysicsComponent::init()
{

}


RECT ResiliencePhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void ResilienceAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::RESILIENCE);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::RESILIENCE, "normal"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR / 2);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::RESILIENCE, "twinkle_style1_1", "twinkle_style1_2", "twinkle_style1_3", "twinkle_style1_4", "twinkle_style1_5","twinkle_style1_6","twinkle_style1_7", "twinkle_style1_8", NULL);
	
	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::RESILIENCE, "resilience_normal_1", "resilience_normal_2", "resilience_normal_3", "resilience_normal_4", "resilience_normal_5", "resilience_normal_6", "resilience_normal_7", "resilience_normal_8", NULL);

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void ResilienceBehaviorComponent::init()
{
	setStatus(eStatus::NORMAL);

	_collisionComponent = new CollisionComponent(eDirection::TOP);
	_collisionComponent->setTargerGameObject(_obj);
}

void ResilienceBehaviorComponent::update(float deltatime)
{
		checkCollision(deltatime);
}


void ResilienceBehaviorComponent::checkCollision(float deltatime)
{
	GVector2 pos = _obj->getPhysicsComponent()->getPosition();
	RECT myBound = _obj->getAnimationComponent()->getBounding();
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case ALADDIN:
			if (_collisionComponent->checkCollision(obj, myBound, deltatime, false))
			{
				if (obj->getPhysicsComponent()->getPositionY() > myBound.bottom && obj->getBehaviorComponent()->getStatus() == eStatus::FALLING)
				{
					SoundManager::getInstance()->Play(eSoundId::sFLAGPOLE);
					((AladdinBehaviorComponent*)obj->getBehaviorComponent())->jump(ALADDIN_JUMP_VEL * 1.3f);
					obj->getAnimationComponent()->setAnimation(eStatus::CLIMB_HORIZON | eStatus::JUMPING);
					_obj->getAnimationComponent()->setTempAnimation(eStatus::RUNNING, 1);
				}
			}
			break;
		}
	}
}