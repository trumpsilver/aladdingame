
#ifndef __ITEM_H__
#define __ITEM_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"
#include "../Player/Aladdin.h"

class ItemPhysicsComponent : public PhysicsComponent
{
public:
	void init();
	RECT getBounding() override;
protected:
};

class ItemAnimationComponent : public AnimationComponent
{
public:
	virtual void init();
};

class AppleItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
	void update(float deltatime) override;
	void draw(LPD3DXSPRITE spriteHandle, Viewport * viewport) override;
	RECT getBounding() override;
};


class GenieItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
};

class AbuItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
};


class MoneyItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
};

class HeartItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
};

class CheckpointItemAnimationComponent : public ItemAnimationComponent
{
public:
	void init();
};

class LifeupItemAnimation : public ItemAnimationComponent
{
public:
	void init();
};

class ItemBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float detatime);
	void setType(eItem type);
	void reset() override;
	void respawn() override;
	eItem getType();
protected:
	void checkCollision(float deltatime);
	eItem _type;
};

class Item : public GameObject
{
public:
	Item();
	~Item();
	void init(eItem _type);

};

#endif

