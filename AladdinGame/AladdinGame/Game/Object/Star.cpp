﻿#include "Star.h"
#include "Land.h"
void StarAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::JAFAR);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1"));
	_sprite->setZIndex(0.0f);

	_sprite->setOrigin(GVector2(0.0f, 0.0f));
	_sprite->setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::JAFAR, "star_1", "star_2", "star_3", NULL);
	_animations[eStatus::NORMAL]->setLoop(true);

	_animations[eStatus::RUNNING] = new Animation(_sprite, 0.07f);
	_animations[eStatus::RUNNING]->addFrameRect(eObjectID::JAFAR, "star_collapse_1", "star_collapse_2", "star_collapse_3", "star_collapse_4", NULL);

	_boundRect.left = 0;
	_boundRect.right = 5;
	_boundRect.bottom = 0;
	_boundRect.top = 5;


}

RECT StarAnimationComponent::getBounding()
{
	_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());

	RECT oldRect = _sprite->getFrameRect();
	_sprite->setFrameRect(_boundRect);
	RECT bound = _sprite->getBounding();
	_sprite->setFrameRect(oldRect);

	return bound;
}


void StarBehaviorComponent::init()
{
	_hasCollapsed = false;
	_timer = 0;
	_trackerTimer = 0;
	RECT r = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_width = r.right - r.left;
	setStatus(eStatus::NORMAL);
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void StarBehaviorComponent::update(float deltatime)
{
	_timer += deltatime;
	RECT trackerBound;
	GVector2 pos;
	GVector2 velocity;
	float factor;
	float distance, angleToPoint, distanceFactor, angleCorrection;
	Movement * move;
	if (_timer >= STAR_LIVING_TIME)
	{
		setStatus(eStatus::DESTROY);
		return;
	}
	switch (_status)
	{
	case NORMAL:
		checkCollision(deltatime);
		_trackerTimer += deltatime;
		if (_trackerTimer >= STAR_TRACKER_INTERVAL)
		{
			_trackerTimer -= STAR_TRACKER_INTERVAL;
		}
		else
		{
			break;
		}
		trackerBound = _trackerObj->getPhysicsComponent()->getBounding();
		pos = _obj->getPhysicsComponent()->getPosition();

		
		//velocity.x = (trackerBound.right + trackerBound.left) / 2 - pos.x;
		//velocity.y = (trackerBound.top + trackerBound.bottom) / 2 - pos.y;
		//if (abs(velocity.x) >= abs(velocity.y))
		//{
		//	if(velocity.x >= 0)
		//		velocity.x = abs(velocity.y);
		//	else
		//		velocity.x = -abs(velocity.y);
		//}
		//// tinh ti le
		//factor = STAR_VELOCITY / sqrt(velocity.x * velocity.x + velocity.y + velocity.y);
		//velocity = velocity * factor;
		

		distance = (trackerBound.right + trackerBound.left) / 2 - pos.x;
		angleToPoint = atan2((trackerBound.top + trackerBound.bottom) / 2 - pos.y, (trackerBound.right + trackerBound.left) / 2 - pos.x);
		distanceFactor = 1 / 1000;
		angleCorrection = (PI*0.18) * (distance * distanceFactor);
		velocity.x = cos(angleToPoint + angleCorrection) * STAR_VELOCITY;
		velocity.y = sin(angleToPoint + angleCorrection) * STAR_VELOCITY;

		//Đã chạm thì không cập nhật y nữa
		move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
		if (_hasCollapsed)
			velocity.y = move->getVelocity().y;
		move->setVelocity(velocity);
		//bám theo object
		
		break;
	case RUNNING:
		if(_obj->getAnimationComponent()->getCurrentAnimation()->isLastFrame())
			setStatus(eStatus::DESTROY);
		break;
	default:
		break;
	}
}

void StarBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);

	updateAnimation();
}

void StarBehaviorComponent::setTrackerObj(GameObject * trackerObj)
{
	_trackerObj = trackerObj;
}

void StarBehaviorComponent::setPullDirection(eDirection direction)
{
	_pullDirection = direction;
}

void StarBehaviorComponent::faceLeft()
{
	if (_obj->getAnimationComponent()->getScale().x > 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + _width);
	}
	setFacingDirection(eDirection::LEFT);
}

void StarBehaviorComponent::faceRight()
{
	if (_obj->getAnimationComponent()->getScale().x < 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - _width);
	}
	setFacingDirection(eDirection::RIGHT);
}


void StarBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case NORMAL:
		_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
		break;
	case RUNNING:
		_obj->getAnimationComponent()->setAnimation(eStatus::RUNNING);
		break;
	default:
		break;
	}
}

void StarBehaviorComponent::checkCollision(float deltatime)
{
	//check for aladdin and lannd
	float moveX, moveY;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	eDirection colliSide;
	_collisionComponent->reset();
	for (auto obj : active_object)
	{

		eObjectID id = obj->getID();
		switch (id)
		{
		case ALADDIN:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				//Càng gần kéo càng mạnh
				float power = (1 - sqrt(_timer / STAR_LIVING_TIME) ) * STAR_VACUM_POWER;
				if (_pullDirection == eDirection::LEFT)
				{
					power = -power;
				}
				auto move = (Movement*)obj->getPhysicsComponent()->getComponent("Movement");
				move->setAddPos(GVector2(power, 0));
				setStatus(eStatus::RUNNING);
			}
			break;
		case LAND:
			if (((Land*)obj)->getLandType() != eLandType::lPLATFORM)
				break;
			//Chạm đất thì nảy ngược lại
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				if (_obj->getPhysicsComponent()->getPositionY() >= obj->getPhysicsComponent()->getBounding().bottom)
				{
					auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
					auto myVelo = move->getVelocity();
					move->setVelocity(GVector2(myVelo.x,  -myVelo.y / 2));
					_hasCollapsed = true;
				}
			}
			break;
		default:
			break;
		}
	}
}

void StarPhysicsComponent::init()
{
	_movingSpeed = 100;
	auto movement = new Movement(GVector2(10, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
}

RECT StarPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}
