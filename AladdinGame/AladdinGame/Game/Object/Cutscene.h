#ifndef __CUTSCENE_H__
#define __CUTSCENE_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/NullComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"


LINK_FRAMEWORK

class CutscenePhysicsComponent : public PhysicsComponent
{
public:
	void init() override;
	void setAnimationComponent(AnimationComponent * animationComponent);
	RECT getBounding();
protected:
	AnimationComponent * _animationComponent;
};

class CutsceneAnimationComponent : public AnimationComponent
{
public:
	void init() override;
};

class CutsceneBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
protected:
};


#endif


#pragma once
