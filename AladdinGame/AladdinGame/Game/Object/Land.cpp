#include "Land.h"
#include "../Scene/SceneManager.h"
void Land::init(int x, int y, int width, int height, eDirection physicBodyDirection, eLandType type)
{
	GameObject::init();
	_id = eObjectID::LAND;
	// X l� left. Y l� top
	RECT bounding;
	bounding.top = y;
	bounding.left = x;
	bounding.bottom = y - height;
	bounding.right = x + width;
	_physicsComponent->setBounding(bounding);
	_physicsComponent->setPosition(GVector2(x, y));
	_behaviorComponent->getCollisionComponent()->setPhysicsSide(physicBodyDirection);
	setLandType(type);
}

void Land::init()
{
	_id = eObjectID::LAND;
}

void Land::setLandType(eLandType landType)
{
	((LandBehaviorComponent *)_behaviorComponent)->setLandType(landType);
}

eLandType Land::getLandType()
{
	return ((LandBehaviorComponent *)_behaviorComponent)->getLandType();
}

void LandPhysiscsComponent::init()
{
}

void LandBehaviorComponent::setLandType(eLandType type)
{
	_landType = type;
}

void LandBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

void LandBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);

	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(0, 0));

	auto gravity = (Gravity*)_obj->getPhysicsComponent()->getComponent("Gravity");
	gravity->setStatus(eGravityStatus::LANDED);

	_timer = 0;
	_holdTimer = 0;
}

eLandType LandBehaviorComponent::getLandType()
{
	return _landType;
}

void LandBehaviorComponent::checkCollision(float deltatime)
{
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case ALADDIN:
			_collisionComponent->checkCollision(obj, deltatime, false);
			break;
		}
	}
}

void LandBehaviorComponent::updateFlameLand(float deltatime)
{
	checkCollision(deltatime);
	if (_timer != 0)
	{
		_timer += deltatime;
		if (_timer >= FLAME_INTEVAL)
		{
			SoundManager::getInstance()->Play(eSoundId::sFIRE_FROM_COAL);
			addToScene.Emit(ObjectFactory::getFlame(_flamePos));
			_timer = 0;
		}
		else
		{
			return;
		}
	}

	GameObject * aladdin = _collisionComponent->isColliding(eObjectID::ALADDIN);

	if (aladdin != nullptr && _timer == 0)
	{
		RECT landBound = _obj->getPhysicsComponent()->getBounding();
		_width = landBound.right - landBound.left;
		float landX = _obj->getPhysicsComponent()->getPositionX();
		float landY = _obj->getPhysicsComponent()->getPositionY();
		RECT aladdinBound = aladdin->getPhysicsComponent()->getBounding();
		
		float aladdinPosX = aladdinBound.left;
		float width = (aladdinBound.right - aladdinBound.left);
		if (aladdinPosX + width / 2 > landX && aladdinPosX + width / 2 < landX + _width && aladdinBound.bottom >= landY)
		{
			_timer += deltatime;
			_flamePos = GVector2(aladdinPosX + width / 2, _obj->getPhysicsComponent()->getPositionY());
		}
	}
}

void LandBehaviorComponent::updateFallingLand(float deltatime)
{
	checkIfOutOfScreen(deltatime);
	checkCollision(deltatime);
	_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);

	if (getStatus() == eStatus::FALLING)
	{
		_timer += deltatime;
		if (_timer >= FALLING_DESTROYED_TIME)
		{
			//setStatus(eStatus::DESTROY);
			//move out of the game instead of destroy
			_obj->getPhysicsComponent()->setPosition(GVector2(-50, 0));
			return;
		}
	}

	if (_holdTimer != 0)
	{
		_holdTimer += deltatime;
		if (_holdTimer >= FALLING_LAND_HOLD_TIME)
		{
			setStatus(eStatus::FALLING);
			auto g = (Gravity*)this->_obj->getPhysicsComponent()->getComponent("Gravity");
			g->setStatus(eGravityStatus::FALLING__DOWN);
		}
	}

	GameObject * aladdin = _collisionComponent->isColliding(eObjectID::ALADDIN);
	if (aladdin != nullptr && aladdin->getPhysicsComponent()->getPositionY() >= _obj->getAnimationComponent()->getBounding().bottom && _holdTimer == 0)
	{
		_holdTimer += deltatime;
	}

}

void LandBehaviorComponent::checkIfOutOfScreen(float deltatime)
{
	auto viewport = SceneManager::getInstance()->getCurrentScene()->getViewport();
	RECT screenBound = viewport->getBounding();
	RECT thisBound = _obj->getAnimationComponent()->getBounding();
	GVector2 position = _obj->getPhysicsComponent()->getPosition();
	float offset = 10;
	GVector2 viewportposition = viewport->getPositionWorld();
	if (thisBound.right < screenBound.left + offset || thisBound.top < viewportposition.y - WINDOW_HEIGHT + offset
		|| thisBound.left > screenBound.right - offset || thisBound.bottom > viewportposition.y - offset)
	{
		respawn();
	}
}


void LandBehaviorComponent::init()
{
	_timer = 0;
	_holdTimer = 0;
	_collisionComponent = new CollisionComponent();
	_collisionComponent->setTargerGameObject(_obj);
}

void LandBehaviorComponent::update(float deltatime)
{
	switch (_landType)
	{
	case lNORMAL:
		break;
	case lFLAME:
		updateFlameLand(deltatime);
		break;
	case lFALLING:
		updateFallingLand(deltatime);
		break;
	default:
		break;
	}
}

void FallingLandPhysiscsComponent::init()
{
	LandPhysiscsComponent::init();
	auto movement = new Movement(GVector2(0, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	auto gravity = new Gravity(GVector2(0, -GRAVITY), movement);
	gravity->setStatus(eGravityStatus::LANDED);
	_componentList["Gravity"] = gravity;

}

RECT FallingLandPhysiscsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void FallingLandAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::MISCITEMS);
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 100.f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::MISCITEMS, "falling_land", NULL);
	_animations[eStatus::NORMAL]->canAnimate(false);

	_index = eStatus::NORMAL;

	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}


}

