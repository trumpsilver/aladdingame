﻿#include "Camel.h"

void CamelPhysicsComponent::init()
{

}


RECT CamelPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void CamelAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CAMEL);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CAMEL, "camel_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.07f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::CAMEL, "camel_1", "camel_2", "camel_3", "camel_4", "camel_5","camel_6","camel_7", "camel_4", "camel_3", "camel_2","camel_1", NULL);
	_animations[eStatus::NORMAL]->setLoop(false);
	for (auto animate : _animations)
	{
		animate.second->setColorFlash(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void CamelBehaviorComponent::init()
{
	_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
	_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
	setStatus(eStatus::NORMAL);
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void CamelBehaviorComponent::update(float deltatime)
{
	switch (_status)
	{
	case NORMAL:
		checkCollision(deltatime);
		break;
	case THROW:
		if (_obj->getAnimationComponent()->getCurrentAnimation()->getIndex() == 4)
		{
			if (_weapon != eStatus::THROW)
			{
				puffApple();
				break;
			}
		}
		if (_obj->getAnimationComponent()->getCurrentAnimation()->isLastFrame())
		{
			setWeapon(eStatus::NORMAL);
			setStatus(eStatus::NORMAL);
			break;
		}
		break;
	default:
		break;
	}
}

void CamelBehaviorComponent::setStatus(int status)
{
	BehaviorComponent::setStatus(status);
	updateAnimation();
}

void CamelBehaviorComponent::puffApple()
{
	setWeapon(eStatus::THROW);
	SoundManager::getInstance()->Play(eSoundId::sCAMELSPIT);
	auto pos = _obj->getPhysicsComponent()->getPosition();
	pos.x += CAMEL_PUFF_OFFSETX;
	pos.y += CAMEL_PUFF_OFFSETY;
	GVector2 velocity(600, 0);
	GVector2 gravity(0, 0);
	auto apple = ObjectFactory::getApple(pos, velocity, gravity);
	addToScene.Emit(apple);
}

void CamelBehaviorComponent::updateAnimation()
{
	switch (_status)
	{
	case NORMAL:
		_obj->getAnimationComponent()->getCurrentAnimation()->restart();
		_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(false);
		break;
	case THROW:
		_obj->getAnimationComponent()->getCurrentAnimation()->canAnimate(true);
		break;
	default:
		break;
	}
}

void CamelBehaviorComponent::checkCollision(float deltatime)
{
	GVector2 pos = _obj->getPhysicsComponent()->getPosition();
	RECT myBound;
	myBound.left = pos.x + 26 * SCALE_FACTOR;
	myBound.bottom = pos.y + 34 * SCALE_FACTOR;
	myBound.right = myBound.left + 7 * SCALE_FACTOR;
	myBound.top = myBound.bottom + 5 * SCALE_FACTOR;
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
		case ALADDIN:
			if (_collisionComponent->checkCollision(obj, myBound, deltatime, false))
			{
				if (obj->getPhysicsComponent()->getPositionY() > myBound.bottom && obj->getBehaviorComponent()->getStatus() == eStatus::FALLING)//frame dầu tiên mới chạm nhảy
				{
					((AladdinBehaviorComponent*)obj->getBehaviorComponent())->jump(ALADDIN_JUMP_VEL * 0.7f);
					setStatus(eStatus::THROW);
				}
			}
			break;
		}
	}
}