﻿#include "Star.h"

void StarAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::JAFAR);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1"));
	_sprite->setZIndex(0.0f);

	_sprite->setOrigin(GVector2(0.0f, 0.0f));
	_sprite->setScale(SCALE_FACTOR);

	_frames["star_1"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_frames["star_2"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_frames["star_3"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");

	_frames["star_collapse_1"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_frames["star_collapse_1"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_frames["star_collapse_1"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_frames["star_collapse_1"] = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");


	RECT starRect = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "star_1");
	_starWidth = starRect.right - starRect.left;
}

void StarAnimationComponent::update(float deltatime)
{
	StarBehaviorComponent * starBehav = (StarBehaviorComponent *)_obj->getBehaviorComponent();
	_pos = starBehav->getPosition();
	_dest = starBehav->getDestination();
}

void StarAnimationComponent::draw(LPD3DXSPRITE spriteHander, Viewport * viewport)
{

	int step = (_dest.x - _pos.x) / _starWidth;
	__debugoutput(_dest.x);
	__debugoutput(_pos.x);
	__debugoutput(step);
	for (int i = 0; i < step; i++)
	{
		_sprite->setPosition(GVector2(_pos.x + i * _starWidth, _pos.y));
		_sprite->render(spriteHander, viewport);
	}
}

void StarBehaviorComponent::init()
{
	_timer = 0;
}

void StarBehaviorComponent::update(float deltatime)
{
	_timer += deltatime;
	if (_timer >= STAR_VACUM_INTERVAL)
	{
		_timer -= STAR_VACUM_INTERVAL;
	}
	else
	{
		return;
	}
	GameObject * aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	//Không có aladdin thì ko hút đc
	if (aladdin == nullptr)
		return;
	
	//Giả sử aladdin bên trái
	//Tìm tọa độ aladdin
	RECT aladdinBound = aladdin->getAnimationComponent()->getBounding();
	float aladdinPosX;
	//Tìm hướng của aladdin
	if (aladdin->getPhysicsComponent()->getPositionX() >= _pos.x)
	{
		//aladdin bên phải lấy biên phải
		aladdinPosX = aladdinBound.right;
	}
	else
	{
		//aladdin bên trái lấy biên trái
		aladdinPosX = aladdinBound.left;
	}
	_dest.x = aladdinPosX;
	float diffirent = aladdinPosX - _pos.x;
	if (diffirent > 0 && diffirent <= STAR_MAX_VACUM_DISTANCE)
	{
		//hút aladdin về
		//càng gần thì hút càng mạnh
		//Cách 1 set vị trí
		float power = (1 - (diffirent / STAR_MAX_VACUM_DISTANCE)) * STAR_VACUM_POWER;
		auto move = (Movement*)aladdin->getPhysicsComponent()->getComponent("Movement");
		move->setAddPos(GVector2(power, 0));
		//Cách 2 giảm vận tốc
	}
	else if (diffirent < 0 && diffirent >= -STAR_MAX_VACUM_DISTANCE)
	{
		float power = - (1 - (-diffirent / STAR_MAX_VACUM_DISTANCE)) * STAR_VACUM_POWER;
		auto move = (Movement*)aladdin->getPhysicsComponent()->getComponent("Movement");
		move->setAddPos(GVector2(power, 0));
	}
}

GVector2 StarBehaviorComponent::getPosition()
{
	return _pos;

}

void StarBehaviorComponent::setPosition(GVector2 pos)
{
	_pos = pos;
}

GVector2 StarBehaviorComponent::getDestination()
{
	return _dest;
}

void StarBehaviorComponent::checkCollision(float deltatime)
{
	
}
