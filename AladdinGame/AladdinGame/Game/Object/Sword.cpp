﻿#include "Sword.h"
//TODO : add delay time before check collision
void SwordPhysicsComponent::init()
{

}

void SwordBehaviorComponent::init()
{
	_canSlashEnemy = false;
	_livingTime = 0;
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void SwordBehaviorComponent::update(float detatime)
{
	_livingTime += detatime;
	if (_livingTime <= SWORD_DELAY_TIME)
	{
		return;
	}
	else if (_livingTime >= SWORD_DELAY_TIME + LIVING_TIME)
	{
		setStatus(eStatus::DESTROY);
		return;
	}
	checkCollision(detatime);
}

void SwordBehaviorComponent::canSlashEnemy(bool result)
{
	_canSlashEnemy = result;
}

void SwordBehaviorComponent::checkCollision(float deltatime)
{
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		if (_canSlashEnemy)
		{
			switch (id)
			{
			case HAKIM: case NAHBI: case FALZA: case JUGGER: case JAR:
				if (_collisionComponent->checkCollision(obj, deltatime, false))
				{
					auto it = std::find(_slashObject.begin(), _slashObject.end(), obj);
					if (it == _slashObject.end() || it._Ptr == nullptr)
					{
						((EnemyBehaviorComponent*)obj->getBehaviorComponent())->dropHitpoint(100);
						_slashObject.push_back(obj);
					}
					break;
				}
			case DAGGER:
				//trúng thì vận tốc y ngược lại
				if (_collisionComponent->checkCollision(obj, deltatime, false))
				{
					auto move = (Movement*)obj->getPhysicsComponent()->getComponent("Movement");
					auto velocity = move->getVelocity();
					velocity.x = -velocity.x;
					move->setVelocity(velocity);
				}
			}
		}
		else
		{
			switch (id)
			{
			case ALADDIN:
				if (_collisionComponent->checkCollision(obj, deltatime, false))
				{
					auto it = std::find(_slashObject.begin(), _slashObject.end(), obj);
					if (it == _slashObject.end() || it._Ptr == nullptr)
					{
						((PlayerBehaviorComponent*)obj->getBehaviorComponent())->dropHitpoint(100);
						_slashObject.push_back(obj);
					}
					break;
				}
			}
		}
	}
}


void Sword::init(int x, int y, int width, int height, eDirection side, bool canSlashEnemy)
{
	GameObject::init();
	_id = eObjectID::SWORD;
	// X là left. Y là top
	RECT bounding;
	bounding.top = y;
	bounding.left = x;
	bounding.bottom = y - height;
	bounding.right = x + width;
	_physicsComponent->setBounding(bounding);
	((SwordBehaviorComponent*)(_behaviorComponent))->canSlashEnemy(canSlashEnemy);
}

Sword::Sword()
{
}

Sword::~Sword()
{
}
