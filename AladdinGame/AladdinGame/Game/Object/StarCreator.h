#ifndef __STARCREARTOR_H__
#define __STARCREARTOR_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/AnimationComponent.h"
#include "../../Framework/Component/BehaviorComponent.h"
#include "../../Framework/Component/PhysicsComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/EnemyComponent.h"
#include "../Share/ShareBehavior.h"
#include "../ObjectFactory.h"
#include "../Scene/SceneManager.h"

#define STAR_CREATOR_INTERVAL 100 // th?i gian t?o m?i sao



class StarCreator
{
public:
	StarCreator();
	~StarCreator();
	void update(float deltatime);
	void setPostion(GVector2 start);
	void setTrackerObj(GameObject * obj);
	void setPullDest(GVector2 destination);
	static sigcxx::Signal<GameObject * > addToScene;
protected:
	GVector2 _pos;
	GVector2 _pullDest;
	GameObject * _trackerObj;
	float _timer;
	int _numberStar;
};

#endif

