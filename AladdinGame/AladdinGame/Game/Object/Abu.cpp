#include "Abu.h"

void AbuPhysicsComponent::init()
{

}

void AbuPhysicsComponent::setAnimationComponent(AnimationComponent * animationComponent)
{
	_animationComponent = animationComponent;
}

RECT AbuPhysicsComponent::getBounding()
{
	return _animationComponent->getBounding();
}

void AbuAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::ABU);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ABU, "with_aladdin_death_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.5f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::DIE] = new Animation(_sprite, 0.07f);
	_animations[eStatus::DIE]->addFrameRect(eObjectID::ABU, "with_aladdin_death_1", "with_aladdin_death_2", "with_aladdin_death_3", "with_aladdin_death_4", "with_aladdin_death_5", "with_aladdin_death_6", NULL);

	_animations[eStatus::WIN] = new Animation(_sprite, 0.07f);
	_animations[eStatus::WIN]->addFrameRect(eObjectID::ABU, "with_aladdin_victory_1", "with_aladdin_victory_2", "with_aladdin_victory_3", "with_aladdin_victory_4", "with_aladdin_victory_5", "with_aladdin_victory_6", "with_aladdin_victory_7", "with_aladdin_victory_8", NULL);

}

void AbuBehaviorComponent::init()
{
	switch (_status)
	{
	case eStatus::DIE:
		_obj->getAnimationComponent()->setAnimation(eStatus::DIE);
		break;
	case eStatus::WIN:
		_obj->getAnimationComponent()->setAnimation(eStatus::WIN);
		break;
	default:
		break;
	}
}

void AbuBehaviorComponent::update(float deltatime)
{
	if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
	{
		setStatus(eStatus::DESTROY);
		return;
	}
}
