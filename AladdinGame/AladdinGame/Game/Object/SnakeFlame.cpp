#include "SnakeFlame.h"
#include "../Object/Land.h"
void SnakeFlamePhysicsComponent::init()
{
	auto movement = new Movement(GVector2(0, 0), GVector2(0, 0), this);
	_componentList["Movement"] = movement;
	_componentList["Gravity"] = new Gravity(GVector2(0, -SNAKE_FLAME_GRAVITY), movement);
}


RECT SnakeFlamePhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void SnakeFlameAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::JAFAR);
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::JAFAR, "snake_flame_1", "snake_flame_2", "snake_flame_3", "snake_flame_4", "snake_flame_5", "snake_flame_6", "snake_flame_7", "snake_flame_8", NULL);

	_index = eStatus::NORMAL;

	_boundRect.left = 0;
	_boundRect.bottom = 0;
	_boundRect.right = SNAKE_FLAME_WIDTH;
	_boundRect.top = SNAKE_FLAME_HEIGHT;
}

RECT SnakeFlameAnimationComponent::getBounding()
{
	_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());

	RECT oldRect = _sprite->getFrameRect();
	_sprite->setFrameRect(_boundRect);
	RECT bound = _sprite->getBounding();
	_sprite->setFrameRect(oldRect);

	return bound;
}

void SnakeFlameBehaviorComponent::init()
{
	RECT r = SpriteResource::getInstance()->getSourceRect(eObjectID::JAFAR, "snake_flame_1");
	_width = r.right - r.left;
	_obj->getAnimationComponent()->setAnimation(eStatus::NORMAL);
	setStatus(eStatus::NORMAL);
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
}

void SnakeFlameBehaviorComponent::update(float deltatime)
{
	checkCollision(deltatime);
}

void SnakeFlameBehaviorComponent::faceLeft()
{
	if (_obj->getAnimationComponent()->getScale().x > 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		//_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() + _width);
	}
	setFacingDirection(eDirection::LEFT);
}

void SnakeFlameBehaviorComponent::faceRight()
{
	if (_obj->getAnimationComponent()->getScale().x < 0)
	{
		_obj->getAnimationComponent()->setScaleX(_obj->getAnimationComponent()->getScale().x * (-1));
		//_obj->getPhysicsComponent()->setPositionX(_obj->getPhysicsComponent()->getPositionX() - _width);
	}
	setFacingDirection(eDirection::RIGHT);
}
void SnakeFlameBehaviorComponent::standing()
{
	
	auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
	move->setVelocity(GVector2(move->getVelocity().x, 0));

	auto gravity = (Gravity*)_obj->getPhysicsComponent()->getComponent("Gravity");
	gravity->setStatus(eGravityStatus::LANDED);
}

void SnakeFlameBehaviorComponent::checkCollision(float deltatime)
{
	auto active_object = SceneManager::getInstance()->getCurrentScene()->getActiveObject();
	_collisionComponent->reset();
	for (auto obj : active_object)
	{
		eObjectID id = obj->getID();
		switch (id)
		{
			case ALADDIN:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				((PlayerBehaviorComponent*)obj->getBehaviorComponent())->dropHitpoint(100);
			}
			break;
			case WALL:
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				setStatus(eStatus::DESTROY);
			}
			break;
		case LAND:
			if (((Land*)obj)->getLandType() != eLandType::lPLATFORM)
				break;
			if (_collisionComponent->checkCollision(obj, deltatime, false))
			{
				if (_obj->getPhysicsComponent()->getPositionY() > obj->getPhysicsComponent()->getBounding().bottom)
				{
					standing();
					auto move = (Movement*)_obj->getPhysicsComponent()->getComponent("Movement");
					if(move->getVelocity().x > 0)
						move->setVelocity(GVector2(SNAKE_FLAME_VELOCITY * 5, 0));
					else
						move->setVelocity(GVector2(-SNAKE_FLAME_VELOCITY * 5, 0));
				}
			}
			break;
		}
	}
}
