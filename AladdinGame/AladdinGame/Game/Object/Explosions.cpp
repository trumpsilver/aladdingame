#include "Explosions.h"

void ExplosionsPhysicsComponent::init()
{

}

RECT ExplosionsPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void ExplosionsAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::EXPLOSIONS);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::EXPLOSIONS, "enemy_explode_1"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.5f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eExplostion::EX1] = new Animation(_sprite, 0.07f);
	_animations[eExplostion::EX1]->addFrameRect(eObjectID::EXPLOSIONS, "enemy_explode_1", "enemy_explode_2", "enemy_explode_3", "enemy_explode_4", "enemy_explode_5", "enemy_explode_6", "enemy_explode_7", "enemy_explode_8", "enemy_explode_9", NULL);

	_animations[eExplostion::EX2] = new Animation(_sprite, 0.07f);
	_animations[eExplostion::EX2]->addFrameRect(eObjectID::EXPLOSIONS, "GenieFace_explode_1", "GenieFace_explode_2", "GenieFace_explode_3", "GenieFace_explode_4", "GenieFace_explode_5", "GenieFace_explode_6", "GenieFace_explode_7", "GenieFace_explode_8", "GenieFace_explode_9", "GenieFace_explode_10", 
		"GenieFace_explode_11", "GenieFace_explode_12", "GenieFace_explode_13", "GenieFace_explode_14", "GenieFace_explode_15", "GenieFace_explode_16", "GenieFace_explode_17", NULL);

	_animations[eExplostion::EX3] = new Animation(_sprite, 0.07f);
	_animations[eExplostion::EX3]->addFrameRect(eObjectID::EXPLOSIONS, "apple_disappear_1", "apple_disappear_2", "apple_disappear_3", "apple_disappear_4", "apple_disappear_5", "apple_disappear_6", "apple_disappear_7", "apple_disappear_8", "apple_disappear_9", NULL);

	_animations[eExplostion::EX4] = new Animation(_sprite, 0.07f);
	_animations[eExplostion::EX4]->addFrameRect(eObjectID::EXPLOSIONS, "knife_explode_1", "knife_explode_2", "knife_explode_3", "knife_explode_4", NULL);

	_animations[eExplostion::EX5] = new Animation(_sprite, 0.07f);
	_animations[eExplostion::EX5]->addFrameRect(eObjectID::EXPLOSIONS, "firekworks_1", "fireworks_2", "fireworks_3", "fireworks_4", "fireworks_5", NULL);
}

void ExplosionsAnimationComponent::draw(LPD3DXSPRITE spriteHander, Viewport * viewport)
{
	switch (_index)
	{
	case EX3:
		setScale(SCALE_FACTOR * 1.5);
		break;
	default:
		break;
	}
	AnimationComponent::draw(spriteHander, viewport);
}

void ExplosionsBehaviorComponent::init()
{
	/*switch (_status)
	{
	case eExplostion::EX1:
		_obj->getAnimationComponent()->setAnimation(eExplostion::EX1);
		break;
	case eExplostion::EX2:
		_obj->getAnimationComponent()->setAnimation(eExplostion::EX2);
		break;
	case eExplostion::EX3:
		_obj->getAnimationComponent()->setAnimation(eExplostion::EX3);
		break;
	case eExplostion::EX4:
		_obj->getAnimationComponent()->setAnimation(eExplostion::EX4);
		break;
	case eExplostion::EX5:
		_obj->getAnimationComponent()->setAnimation(eExplostion::EX5);
		break;
	default:
		break;
	}*/
}

void ExplosionsBehaviorComponent::update(float deltatime)
{
	if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
	{
		setStatus(eStatus::DESTROY);
		return;
	}
}
