﻿#include "Item.h"

void ItemPhysicsComponent::init()
{
}



RECT ItemPhysicsComponent::getBounding()
{
	return _obj->getAnimationComponent()->getBounding();
}

void ItemAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::ITEM);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ITEM, "apple"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.0f, 0.0f));
	setScale(SCALE_FACTOR);
}


void ItemBehaviorComponent::init()
{
	_collisionComponent = new CollisionComponent(eDirection::ALL);
	_collisionComponent->setTargerGameObject(_obj);
	setStatus(eStatus::NORMAL);
}

void ItemBehaviorComponent::update(float detatime)
{
	//checkCollision(detatime);
	// if type = checkpoint and out of screen remove it
}

void ItemBehaviorComponent::setType(eItem type)
{
	_type = type;
}

void ItemBehaviorComponent::reset()
{
	setStatus(eStatus::NORMAL);
}

void ItemBehaviorComponent::respawn()
{
	reset();
	BehaviorComponent::respawn();
}

eItem ItemBehaviorComponent::getType()
{
	return _type;
}

void ItemBehaviorComponent::checkCollision(float deltatime)
{
	auto aladdin = SceneManager::getInstance()->getCurrentScene()->getObject(eObjectID::ALADDIN);
	auto aladdinBehavior = (AladdinBehaviorComponent *)aladdin->getBehaviorComponent();

	if (_collisionComponent->checkCollision(aladdin, deltatime, false))
	{
		switch (_type)
		{
		case iEXTRA_HEART:
			break;
		case iAPPLE:
			OutputDebugStringW(L"called : ");
			aladdinBehavior->setNumApple(aladdinBehavior->getNumApple() + 1);
			__debugoutput(aladdinBehavior->getNumApple());
			setStatus(eStatus::DESTROY);
			break;
		case iMONEY:
			break;
		case iRESTART_POINT:
			break;
		case i1UP:
			break;
		default:
			break;
		}
	}
}

Item::Item()
{
}


Item::~Item()
{
}

void Item::init(eItem type)
{
	GameObject::init();
	_id = eObjectID::ITEM;
	((ItemBehaviorComponent*)_behaviorComponent)->setType(type);
}

void AppleItemAnimationComponent::init()
{
	ItemAnimationComponent::init();
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::ITEM, "apple"));
}

void AppleItemAnimationComponent::update(float deltatime)
{
	//do nothing
	_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());
}

void AppleItemAnimationComponent::draw(LPD3DXSPRITE spriteHandle, Viewport* viewport)
{
	_sprite->render(spriteHandle, viewport);
}

RECT AppleItemAnimationComponent::getBounding()
{
	//_sprite->setPosition(_obj->getPhysicsComponent()->getPosition());
	return _sprite->getBounding();
}

void MoneyItemAnimationComponent::init()
{

	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.2f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "money_1", "money_2", "money_3", "money_4", "money_5", "money_6", "money_7", "money_8", "money_9", NULL);
	_animations[eStatus::NORMAL]->canAnimate(true);

	_index = eStatus::NORMAL;
}

void GenieItemAnimationComponent::init()
{
	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.2f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "genie_face_1", "genie_face_2", "genie_face_3", "genie_face_4", NULL);
	_animations[eStatus::NORMAL]->canAnimate(true);

	_index = eStatus::NORMAL;
}

void HeartItemAnimationComponent::init()
{
	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.2f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "blue_heart_1", "blue_heart_2", "blue_heart_3", "blue_heart_4", "blue_heart_5", "blue_heart_6", "blue_heart_7", "blue_heart_8", NULL);
	_animations[eStatus::NORMAL]->canAnimate(true);
	
	_index = eStatus::NORMAL;

}

void CheckpointItemAnimationComponent::init()
{
	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.1f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "vase_1", "vase_2", "vase_3", "vase_4", "vase_5", "vase_6", "vase_7", "vase_8", "vase_9", "vase_10", NULL);
	_animations[eStatus::NORMAL]->setLoop(false);
	_animations[eStatus::NORMAL]->canAnimate(false);

	_index = eStatus::NORMAL;
}

void LifeupItemAnimation::init()
{
	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.2f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "aladdinFace_1", "aladdinFace_2", "aladdinFace_3", "aladdinFace_4", "aladdinFace_5", "aladdinFace_6", "aladdinFace_7", "aladdinFace_8", NULL);
	_animations[eStatus::NORMAL]->canAnimate(false);

	_index = eStatus::NORMAL;
}

void AbuItemAnimationComponent::init()
{
	ItemAnimationComponent::init();

	_animations[eStatus::NORMAL] = new Animation(_sprite, 0.2f);
	_animations[eStatus::NORMAL]->addFrameRect(eObjectID::ITEM, "abu_face_1", "abu_face_2", "abu_face_3", "abu_face_4", "abu_face_5", "abu_face_6", "abu_face_7", "abu_face_8", "abu_face_9", "abu_face_10", NULL);
	_animations[eStatus::NORMAL]->canAnimate(false);

	_index = eStatus::NORMAL;
}
