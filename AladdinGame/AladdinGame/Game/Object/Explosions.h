#ifndef __EXPLOSIONS_H__
#define __EXPLOSIONS_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/NullComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"


LINK_FRAMEWORK

class ExplosionsPhysicsComponent : public PhysicsComponent
{
public:
	void init() override;
	RECT getBounding();
protected:
};

class ExplosionsAnimationComponent : public AnimationComponent
{
public:
	void init() override;
	void draw(LPD3DXSPRITE spriteHander, Viewport* viewport);
};

class ExplosionsBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
protected:
};


#endif


