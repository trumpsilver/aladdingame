﻿#ifndef __STAR_H__
#define __STAR_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/NullComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"

#define STAR_MAX_VACUM_DISTANCE 150 * SCALE_FACTOR
#define STAR_VACUM_POWER 1
#define STAR_VACUM_INTERVAL 10 
LINK_FRAMEWORK


class StarAnimationComponent : public AnimationComponent
{
public:
	void init() override;
	void update(float deltatime) override;
	void draw(LPD3DXSPRITE spriteHander, Viewport* viewport) override;
protected:
	float _starWidth;
	GVector2 _pos;
	GVector2 _dest;
	map<string, RECT> _frames;
};

class StarBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
	GVector2 getPosition();
	void setPosition(GVector2 pos);
	GVector2 getDestination();
protected:
	void checkCollision(float deltatime);
	GVector2 _pos;
	GVector2 _dest;
	float _timer;
	
};


#endif

