﻿#ifndef __SNAKEFLAME_H__
#define __SNAKEFLAME_H__
#include "../../Framework/define.h"
#include "../../Framework/GameObject.h"
#include "../../Framework/Component/NullComponent.h"
#include "../../Framework/Component/CollisionComponent.h"
#include "../../Framework/Component/PlayerComponent.h"

#define SNAKE_FLAME_GRAVITY 200
#define SNAKE_FLAME_VELOCITY 50
#define SNAKE_FLAME_WIDTH 54
#define SNAKE_FLAME_HEIGHT 27
LINK_FRAMEWORK

class SnakeFlamePhysicsComponent : public PhysicsComponent
{
public:
	void init() override;
	RECT getBounding();
protected:
};

class SnakeFlameAnimationComponent : public AnimationComponent
{
public:
	void init() override;
	RECT getBounding() override;
protected:
	RECT _boundRect;
};

class SnakeFlameBehaviorComponent : public BehaviorComponent
{
public:
	void init();
	void update(float deltatime) override;
	void faceRight();
	void faceLeft();
protected:
	void standing();
	void checkCollision(float deltatime);
	float _width;
};


#endif

