#include "Cutscene.h"

void CutscenePhysicsComponent::init()
{
}

void CutscenePhysicsComponent::setAnimationComponent(AnimationComponent * animationComponent)
{
	_animationComponent = animationComponent;
}

RECT CutscenePhysicsComponent::getBounding()
{
	return _animationComponent->getBounding();
}

void CutsceneAnimationComponent::init()
{
	_sprite = SpriteResource::getInstance()->getSprite(eObjectID::CUTSCENES);
	_sprite->setFrameRect(SpriteResource::getInstance()->getSourceRect(eObjectID::CUTSCENES, "level_complete"));
	_sprite->setZIndex(0.0f);

	setOrigin(GVector2(0.5f, 0.0f));
	setScale(SCALE_FACTOR);

	_animations[eCutscene::cLEVEL_COMPLETE] = new Animation(_sprite, 0.07f);
	_animations[eCutscene::cLEVEL_COMPLETE]->addFrameRect(eObjectID::CUTSCENES, "level_complete", NULL);

	_animations[eCutscene::cMENU] = new Animation(_sprite, 0.07f);
	_animations[eCutscene::cMENU]->addFrameRect(eObjectID::CUTSCENES, "scene_menu", NULL);

	_animations[eCutscene::cSWORD] = new Animation(_sprite, 0.07f);
	_animations[eCutscene::cSWORD]->addFrameRect(eObjectID::CUTSCENES, "sword", "sword", "sword", "sword", "sword", "sword", "sword", NULL);

	_animations[eCutscene::cAGRABAH_MARKET] = new Animation(_sprite, 0.123f);
	_animations[eCutscene::cAGRABAH_MARKET]->addFrameRect(eObjectID::CUTSCENES, "AgrabahMarket_1", "AgrabahMarket_2", "AgrabahMarket_3", "AgrabahMarket_4", "AgrabahMarket_5", "AgrabahMarket_6", NULL);

	_animations[eCutscene::cJAFAR_PALACE] = new Animation(_sprite, 0.123f);
	_animations[eCutscene::cJAFAR_PALACE]->addFrameRect(eObjectID::CUTSCENES, "JafarPalace_1", "JafarPalace_2", "JafarPalace_3", "JafarPalace_4", "JafarPalace_5", "JafarPalace_6", NULL);
}

void CutsceneBehaviorComponent::init()
{
	switch (_status)
	{
	case eCutscene::cLEVEL_COMPLETE:
		_obj->getAnimationComponent()->setAnimation(eCutscene::cLEVEL_COMPLETE);
		break;
	case eCutscene::cMENU:
		_obj->getAnimationComponent()->setAnimation(eCutscene::cMENU);
		break;
	case eCutscene::cSWORD:
		_obj->getAnimationComponent()->setAnimation(eCutscene::cSWORD);
		break;
	case eCutscene::cAGRABAH_MARKET:
		_obj->getAnimationComponent()->setAnimation(eCutscene::cAGRABAH_MARKET);
		break;
	default:
		break;
	}
}

void CutsceneBehaviorComponent::update(float deltatime)
{
	if (_obj->getAnimationComponent()->getCurrentAnimation()->getCount() >= 1)
	{
		setStatus(eStatus::DESTROY);
		return;
	}
}
