﻿#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "../pugixml/src/pugixml.hpp"
using namespace std;
using namespace pugi;

#define MAP_HEIGHT 689;
#define OFFSETX 66;
#define OFFSETY 46;

void loadSpriteInfo(const char* fileInfoPath)
{
	FILE* file;
	file = fopen(fileInfoPath, "r");

	ofstream myfile("result.txt");
	int left, top, width, height, transitionx;
	if (file)
	{
		while (!feof(file))
		{

			char name[100];
			fscanf(file, "%s %d %d %d %d %d", &name, &left, &top, &width, &height, &transitionx);
			myfile << name << "\t" << left << "\t" << top << "\t" << left + width << "\t" << top + height << "\t" << -transitionx << "\t0" <<endl;
		}
	}
	myfile.close();
	fclose(file);
}

void fixXML(const string path)
{
	pugi::xml_document readDoc;
	pugi::xml_document writeDoc;

	// Mở file và đọc
	xml_parse_result result = readDoc.load_file(path.data(), pugi::parse_default | pugi::parse_pi);
	if (result == false)
	{
		return;
	}

	int i = 0;

	// Lấy id từ file xml. so sánh với eID, tuỳ theo eID nào mà gọi đến đúng hàm load cho riêng object đó.
	for (auto item : readDoc.children())
	{
		i++;
		string s = std::to_string(i);
		int id = item.attribute("id").as_int();
		int x = item.attribute("x").as_int();
		int y = item.attribute("y").as_int();
		int width = item.attribute("width").as_int();
		int height = item.attribute("height").as_int();

		xml_node node = writeDoc.append_child("Item");
		node.append_attribute("Id") = 1; 
		node.append_attribute("Name") = ("land_" + std::to_string(i)).c_str();
		node.append_attribute("X") = x  + OFFSETX;
		node.append_attribute("Y") = 538 - y - 5  - OFFSETY;
		node.append_attribute("Width") = width;
		node.append_attribute("Height") = height;

		xml_node bnode = node.append_child("Elem");
		bnode.append_attribute("Key") = "type";
		bnode.append_attribute("Value") = 0;

		xml_node cnode = node.append_child("Elem");
		cnode.append_attribute("Key") = "physicBodyDirection";
		cnode.append_attribute("Value") = 1;

		//xml_node dnode = node.append_child("Elem");
		//dnode.append_attribute("Key") = "zIndex";
		//dnode.append_attribute("Value") = 2;
		
	}

	bool saveSucceeded = writeDoc.save_file("result.xml", PUGIXML_TEXT("  "));
}

void fixXMLwithRange(const string path)
{
	pugi::xml_document readDoc;
	pugi::xml_document writeDoc;
	vector<xml_node> nodeList;

	// Mở file và đọc
	xml_parse_result result = readDoc.load_file(path.data(), pugi::parse_default | pugi::parse_pi);
	if (result == false)
	{
		return;
	}

	int i = 0;

	// Lấy id từ file xml. so sánh với eID, tuỳ theo eID nào mà gọi đến đúng hàm load cho riêng object đó.

	for (auto item : readDoc.child("object").children())
	{
		i++;
		string s = std::to_string(i);
		int id = item.attribute("id").as_int();
		int x = item.attribute("x").as_int();
		int y = item.attribute("y").as_int();
		int width = item.attribute("width").as_int();
		int height = item.attribute("height").as_int();

		xml_node node = writeDoc.append_child("Item");
		node.append_attribute("Id") = 11;
		node.append_attribute("Name") = ("jar_" + std::to_string(i)).c_str();
		node.append_attribute("X") = x + OFFSETX;
		node.append_attribute("Y") = 689 - y + OFFSETY;
		node.append_attribute("Width") = width;
		node.append_attribute("Height") = height;

	}

	auto rNode = writeDoc.child("Item");
	for (auto item : readDoc.child("objectarea").children())
	{
		int x = item.attribute("x").as_int();
		int y = item.attribute("y").as_int();
		int width = item.attribute("width").as_int();
		int height = item.attribute("height").as_int();

		xml_node node = rNode.append_child("Bound");
		node.append_attribute("Left") = x + OFFSETX;
		node.append_attribute("Bottom") = 689 - y + OFFSETY;
		node.append_attribute("Right") = x + width + OFFSETX;
		node.append_attribute("Top") = 689 - y + height + OFFSETY;
		rNode = rNode.next_sibling();
	}


	bool saveSucceeded = writeDoc.save_file("result.xml", PUGIXML_TEXT("  "));
}

void generaladder(int sx, int sy, int ex, int ey, int zIndex, int numberofstep)
{
	pugi::xml_document writeDoc;

	float width = 1.0f * (ex - sx) / numberofstep;
	float height = 1.0f * (ey - sy) / numberofstep;
	for (int i = 0; i <= numberofstep; i++)
	{
		int x = sx + width * i;
		int y = sy + height * i;

		xml_node node = writeDoc.append_child("Item");
		node.append_attribute("Id") = 1;
		node.append_attribute("Name") = ("pass_throught_ladder_" + std::to_string(31 + i)).c_str();
		node.append_attribute("X") = x;
		node.append_attribute("Y") = y;
		node.append_attribute("Width") = abs((int)width);
		node.append_attribute("Height") = abs((int)height);

		xml_node bnode = node.append_child("Elem");
		bnode.append_attribute("Key") = "zIndex";
		bnode.append_attribute("Value") = zIndex;
	}

	bool saveSucceeded = writeDoc.save_file("result.xml", PUGIXML_TEXT("  "));
}
int main()
{
	cout << "Fuck you !";
	loadSpriteInfo("text.txt");
	//fixXML("save.xml");
	//fixXMLwithRange("save.xml");
	//generaladder(2282, 114, 2556, 173, 2, 30);
}